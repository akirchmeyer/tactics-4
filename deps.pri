
######CONFIG#####
CONFIG -= qt
CONFIG += console

QMAKE_CXXFLAGS += -std=c++1y# -pipe #-DNDEBUG
QMAKE_CXXFLAGS += -Wall -Wextra -Wnonnull -Wuninitialized -Wsuggest-attribute=const -Wsuggest-attribute=pure -Wparentheses -Wsequence-point -Wmissing-braces -Winit-self -O0
# -Os -Og #--O3 -ffast-math
QMAKE_CXXFLAGS += -Wunsafe-loop-optimizations -Wsign-compare -Wpedantic
# -Wpedantic
QMAKE_LFLAGS += -static-libgcc -static-libstdc++

QMAKE_CXXFLAGS += -D_WIN32_WINNT=0x600 -march=native -Wall -Wextra -Wshadow -pedantic -pthread -Wl,--no-as-needed

######DIRECTORIES#####
BASE_DIR =  "/Projects/tactics-2"#"/prog/tactics-2"#
#!exists(BASE_DIR){
#    BASE_DIR = "F:/prog/tactics-2"
#    !exists(BASE_DIR){
#        BASE_DIR = "../../.."
#    }
#}

DEPS_INC = $$BASE_DIR/deps/include
DEPS_LIB = $$BASE_DIR/deps/lib

PROJ_DIR = $$BASE_DIR/src
DEST_DIR = $$BASE_DIR/build

BIN_DIR =  $$DEST_DIR/bin
TMP_DIR =  $$DEST_DIR/tmp
PWD_DIR =  $$DEST_DIR/pwd

PWD = $$PWD_DIR
DESTDIR  = $$BIN_DIR
OBJECTS_DIR = $$TMP_DIR
MOC_DIR = $$TMP_DIR
UI_DIR = $$TMP_DIR

######LIBRARIES#####

NAME_DNX = dnx-0.4
NAME_ML  = ml-0.4
NAME_GAME = tactics-0.4

INCLUDEPATH += $$DEPS_INC

CONFIG(BUILD_DNX){
    INCLUDEPATH += $$PROJ_DIR/dynamics/src
    LIBS += -L$$BIN_DIR -l$$NAME_DNX
}

CONFIG(BUILD_ML){
    INCLUDEPATH += $$PROJ_DIR/ml/src
    LIBS += -L$$BIN_DIR -l$$NAME_ML
}

CONFIG(BUILD_GLFW){
    #DEFINES += GLFW_DLL
    LIBS += -L$$DEPS_LIB -lglfw3 -lgdi32 -lopengl32
}

CONFIG(BUILD_STBI){
    #INCLUDEPATH += $$DEPS_INC
    LIBS += -L$$DEPS_LIB -lstbi
}

CONFIG(BUILD_GLEW){
    #INCLUDEPATH += $$DEPS_INC
    LIBS += -lopengl32 -lglu32 
    LIBS += -L$$DEPS_LIB -lglew32
}

CONFIG(BUILD_TBB){
    #INCLUDEPATH += $$DEPS_INC
    LIBS += -L$$DEPS_LIB -ltbb_debug
}

#CONFIG(BUILD_BOOST){
#    INCLUDEPATH += $$BASE_DIR/../Dependencies/boost_1_57_0
#    LIBS += -L $$BASE_DIR/../Dependencies/boost_1_57_0/lib -lboost_system-mgw49-mt-d-1_57 -lboost_log-mgw49-mt-d-1_57 -lboost_log_setup-mgw49-mt-d-1_57
#}


