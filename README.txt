README:
Tactics v0.1
 

* Map - height
* Update collision detection
* Rotation



















OLD:

TODO:
* Steering/Flocking
* Platoons
* Terrain Heightmap
* Fog of War

PHYSICS:
 * Add Integrator - Sympletic -> look at pdf
 * Use AABB for Broadphase collision -> pairs - sweep and prune + spatial hash? -> "Tus+ Core Engine"
 * Use SAT (or GJK) for narrow-phase collision? -> "N tutorial A" or use MPR

entity          -> manages body + position   +   calls the following:

 - intelligence -> manages AI -> stocking + flocking + specific behaviors
 - hull         -> manages damage received + life + shield + reinforcements
 - engine       -> manages movement done
 - weapon       -> manages damage done
 - vision?      -> manages vision + entities found
