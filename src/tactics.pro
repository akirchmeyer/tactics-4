TEMPLATE = subdirs
CONFIG += ordered

include(../deps.pri)

OTHER_FILES += $$BASE_DIR/README.txt \
               $$BASE_DIR/data/data.svg

SUBDIRS += \
    ml \
    dynamics \
    game

#dynamics.depends += ml
#game.depends += dynamics
