#ifndef IO_H
#define IO_H

#if 0
#include <exception>
#include <string>
#include <ml/common.h>
#include <cassert>

enum code{
    deb = -11,
    ver = -10,
    act = -7,
    prog= -5,
    gen = -1,
    war = 1,
    err = 5,
    fat = 9
};

namespace ml{
class exception : public std::exception{
public:
    exception(const std::string &desc, const std::string &file, const int32 line, const std::string &func, const int32 level = code::fat, const int32 ec = 0);
    virtual ~exception() throw();

    virtual const char* what() const throw();
    _inline void setlevel(const int32 l){ m_level = l; }

    static void open(const std::string &filename, const int32 state);
    static void close();
    static void log(const exception &e);
    static void log(const std::string &desc, const std::string &file, const int32 line, const std::string &func, const int32 level = 9, const int32 ec = 0);
    static void push(const exception &e);
    static void push(const std::string &desc, const std::string &file, const int32 line, const std::string &func, const int32 level = 9, const int32 ec = 0);
    static void pop();
    static void left();

//private:
    std::string m_desc, m_file, m_func;
    int32 m_line, m_error, m_level;
};
}

typedef ml::exception dbg;

//#define essert(b, d) if(!d) throw ml::exception(d, _FILE, _LINE, _FUNC, code::fat, 0);
//#define CATCH() catch(std::exception const& e) { dbg::print(e.what()); }
//#define THROW(b, e) if(b){ throw e; } else{dbg::lowlog(e);}
#endif
#include <cassert>
/*#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/trivial.hpp>


using namespace boost;

BOOST_LOG_INLINE_GLOBAL_LOGGER_DEFAULT(this_logger, log::sources::logger_mt)*/

#include "spdlog/spdlog.h"
namespace io = spdlog;

#define LOC LOC2(__FILE__,__LINE__,_FUNC)
#define LOC2(file, line, func) "[" << file << "@" << line << ": " << func << "] "

/*template<typename T> void __critical(T const& b, const char* e, const char* d, const char* file, int line, const char* func){
    bool _b = !(!b);
    if(!_b) io::get("this")->critical() << LOC2(file, line, func) << "Check " << e << " failed: " << d << "\n";
}

#define _critical(b, d) __critical(b, b, d)*/

/*namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;*/

#endif // IO_H
