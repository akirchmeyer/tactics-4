#ifndef CONSOLE_H
#define CONSOLE_H

#include "io.h"
#include "ml/types.h"

template<typename T>constexpr std::ostream& operator<< (std::ostream& a, const ml::vec2<T> &b){
    return a << b.x << ' ' << b.y;
}

template<typename T>constexpr std::ostream& operator<< (std::ostream& a, const ml::vec3<T> &b){
    return a << b.x << ' ' << b.y << ' ' << b.z;
}

template<typename T>constexpr std::ostream& operator<< (std::ostream& a, const ml::vec4<T> &b){
    return a << b.x << ' ' << b.y << ' ' << b.z << ' ' << b.w;
}

template<typename T>constexpr std::ostream& operator<< (std::ostream& a, const ml::mat2<T> &b){
    return a << b[0] << '\n' << b[1];
}

template<typename T>constexpr std::ostream& operator<< (std::ostream& a, const ml::mat4<T> &b){
    return a << b[0] << '\n' << b[1] << '\n' << b[2] << '\n' << b[3];
}
#endif // CONSOLE_H
