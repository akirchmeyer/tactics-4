#ifndef UL_COMMON_H
#define UL_COMMON_H

#include "ml/common.h"
#include "ml/ml.h"

#include "io.h"

#define _FUNC __func__
#define _LINE __LINE__
#define _FILE __FILE__

#undef NULL
#define NULL nullptr

#include <memory>


#define uptr std::unique_ptr

class del{
public:
    constexpr del() {}
    virtual ~del() {}
};

namespace gc{
    /*void add(del *p);
    void remove(del *p);
    void release();*/
    del* create(del* p);
    void releaseAll();
}

#endif // COMMON_H
