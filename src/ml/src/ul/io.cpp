#include "io.h"

#if 0
#include "ml/timer.h"
#include <sstream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include "io.h"

#include <vector>

//std::ofstream stream;

std::ofstream stream;
ml::tpoint uptime;
uint32 __level = 0;
int32 max_level = -1;

namespace ml{

exception::exception(const std::string &desc, const std::string &file, const int32 line, const std::string &func, const int32 level, const int32 ec)
    : m_desc(desc), m_file(file), m_func(func), m_line(line), m_error(ec), m_level(level) {
}

exception::~exception() throw() {
}

void exception::push(const exception &e){
    log(e);
    __level+=1;
}

void exception::push(const std::string &desc, const std::string &file, const int32 line, const std::string &func, const int32 level, const int32 ec){
    push(exception(desc, file, line, func, level, ec));
}

void exception::pop(){
    __level-=1;
}

std::string extract_time(ml::tdur d){
    uint32 h, m, s;
    h = ml::dur<uint32, ml::thour>(d); d -= std::chrono::hours(h);
    m = ml::dur<uint32, ml::tmin>(d); d -= std::chrono::minutes(m);
    s = ml::dur<uint32, ml::tsec>(d);
    std::ostringstream ss;
    ss << std::setfill('0') << std::setw(2) << h << ":" << std::setw(2) << m << ":" << std::setw(2) << s;
    return ss.str();
}

std::string func(const exception *e){
    std::ostringstream s;
    s << e->m_func << "()";
    return s.str();
}

std::string file_line(const exception *e){
    std::ostringstream s;
    s << e->m_file << ":" << e->m_line;
    return s.str();
}

std::string desc(const exception *e){
    std::ostringstream s;
    s << e->m_desc;
    return s.str();
}

std::string up(){
    return extract_time(ml::now() - uptime);
}

std::string error(const exception *e){
    std::ostringstream s;
    if(e->m_level > code::ver)
        s << "W:" << (e->m_level-code::ver);
    else if(e->m_level > 0){
        s << "ERROR:" << ((e->m_error != 0) ? e->m_error : -e->m_level);
    }
    return s.str();
}

std::string space(){
    std::ostringstream s;
    for(uint32 i = 0; i < __level; ++i){
        s << "|  ";
    }
    s << "[";
    return s.str();
}

void exception::left(){
    stream << '\n'; //print("\n");
    __level = 0;
}

const char* exception::what() const throw(){
    std::ostringstream s;
    s << std::left/* << up()*/;
    s << space();
    std::ostringstream o;
    o << func(this) << ":  " << desc(this);
    s << std::setw(70-(__level*3+1)) << o.str();
    s << std::setw(15) << error(this);


    //s << std::setw(50)
    s << std::setw(20) << file_line(this);
    s << "\n";
    return s.str().c_str();
}

/*const char* exception::what() const throw(){
    std::ostringstream s, o;
    o << m_file << ":" << m_line;
    s << std::left << std::setw(25-__level) << m_func+"() "+o.str();  o.str("");

    o << std::setw(2) << m_level << ":" << m_error << ": ";

    s << std::setw(20) << o.str(); o.str("");

    s << m_desc;

    s << "\n";
    return s.str().c_str();
}*/

void exception::open(const std::string &filename, const int32 state){
    max_level = state;
    stream.open(filename);
    uptime = ml::now();
    std::time_t time = std::chrono::system_clock::to_time_t(uptime);
    std::cout << stream  << "Tactics v0.1 - " << std::ctime(&time) << "\n";
}

void exception::log(const exception &e){
    if(e.m_level > max_level){
        std::cout << stream << e.what();
    }
}

void exception::log(const std::string &desc, const std::string &file, const int32 line, const std::string &func, const int32 level , const int32 ec){
    log(exception(desc, file, line, func, level, ec));
}

void exception::close(){
    std::ostringstream s;
    std::cout << stream << "\nLogs End - " << extract_time(ml::now()-uptime);
    stream.close();
}
}
#endif
