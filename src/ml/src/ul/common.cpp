#include "common.h"
#include <ul/io.h>
#include <vector>

std::vector<uptr<del>> v;

namespace gc{
del *create(del* p){
    v.push_back(uptr<del>(p));
    return (*(v.end()-1)).get();
}

void releaseAll(){
    v.clear();
}
}
