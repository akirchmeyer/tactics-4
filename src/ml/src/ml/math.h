#ifndef MATH_H
#define MATH_H


//#include <vector>

//#include <limits.h>
#include <cmath>
#include <algorithm>

#include "common.h"

#if 0
//#define ONE_DIV_PI  0.31830988618379067154f
#define HALF_PI     1.5707963f
#define FPI          3.1415927f
#define TWO_PI      6.2831853f

//#define SQUARE_PI   1.77245385090551602729f

#define RAD_PI   0.01745329252f  //PI/180
#define INV_RAD  57.29577951f    //180/PI

/*#define SIN_1       1.2732395f//4/pi
#define SIN_2       -0.4052847f//-4/(pi*pi)*/

/*union{int i;float x;} u;
float x1 = 0.f;*/
#endif

namespace ml{
constexpr const float pi      = 3.14159265358979323846f;
constexpr const float rad_pi  = 0.01745329251994329577f;

    template<typename T>constexpr _inline T max(const T x, const T y)  {   return (x > y) ? x : y;     }
    template<typename T>constexpr _inline T min(const T x, const T y)  {   return (x < y) ? x : y;     }
    template<typename T>constexpr _inline T abs(const T x)       {   return (x > 0) ? x : -x;    }

    template<typename T>constexpr _inline void swap(T &a, T &b){ std::swap(a, b); }

    template<typename T>constexpr _inline T sign(const T x)              {   return (x > 0) - (x < 0);                  }
    template<typename T>constexpr _inline T sgn(const T x)              {   return (x < 0) ? -1 : 1;                  }
    template<typename T>constexpr _inline T scl(const T x, const T lo, const T hi)   {   return ( x * (hi - lo) + hi + lo) / T(2);   }
    template<typename T>constexpr _inline T clamp(const T x, const T min, const T max){  return ml::max(ml::min(x, max), min);       }


    constexpr _inline float    radians(const float angle)    {   return angle * rad_pi;  }
    constexpr _inline float    floor(const float x)          {   return std::floor(x);   }
    constexpr _inline float    ceil(const float x)           {   return std::ceil(x);    }
    constexpr _inline float    pow(const float x,const float y)   { return std::pow(x, y);    }
    constexpr _inline float    round(const float x)          { return std::roundf(x);    }

    constexpr _inline float    mod(const float x,const float y){ return std::fmod(x, y); }
    _inline float    ipart(const float x){ float i = 0.f; std::modf(x, &i); return i; }
    _inline float    fpart(const float x){ return std::modf(x, nullptr);  }


    constexpr _inline float sin(const float x)   {   return sinf(x);     }
    constexpr _inline float cos(const float x)   {   return cosf(x);     }
    constexpr _inline float tan(const float x)   {   return tanf(x);     } //sin(x)/cos(x)
    constexpr _inline float asin(const float x)  {   return asinf(x);    }
    constexpr _inline float acos(const float x)  {   return acosf(x);    }
    constexpr _inline float atan(const float x)  {   return atanf(x);    }
    constexpr _inline float sqrt(const float x)  {   return sqrtf(x);    }
    constexpr _inline float atan2(const float y, const float x) {   return atan2f(y, x);   }

    constexpr _inline float clamp(const float x, const float min, const float max){ return ml::max(ml::min(x, max), min); }
}

#endif // MATH_H
