#ifndef FUNCTIONS
#define FUNCTIONS


#include "types.h"

namespace ml{
template<typename T>constexpr _inline ml::vec2<T> abs(const ml::vec2<T> &a){
    return ml::vec2<T>(ml::abs(a.x), ml::abs(a.y));
}

template<typename T>constexpr _inline ml::vec3<T> abs(const ml::vec3<T> &a){
    return ml::vec3<T>(ml::abs(a.x), ml::abs(a.y), ml::abs(a.z));
}

template<typename T>constexpr _inline ml::vec4<T> abs(const ml::vec4<T> &a){
    return ml::vec4<T>(ml::abs(a.x), ml::abs(a.y), ml::abs(a.z), ml::abs(a.w));
}

template<typename T>constexpr _inline T dot(const ml::vec2<T> &a, const ml::vec2<T> &b){
    return a.x*b.x + a.y*b.y;
}

template<typename T>constexpr _inline T dot(const ml::vec3<T> &a, const ml::vec3<T> &b){
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

template<typename T>constexpr _inline T dot(const ml::vec4<T> &a, const ml::vec4<T> &b){
    return a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w;
}

template<typename T>constexpr _inline T dot(const ml::vec4<T> &a, const ml::vec3<T> &b){
    return a.x*b.x + a.y*b.y + a.z*b.z + a.w;
}

template<typename T>constexpr _inline float length(const ml::vec2<T> &v){
    return ml::sqrt(lengthSq(v));
}

template<typename T>constexpr _inline float length(const ml::vec3<T> &v){
    return ml::sqrt(lengthSq(v));
}

template<typename T>constexpr _inline float length(const ml::vec4<T> &v){
    return ml::sqrt(lengthSq(v));
}

template<typename T>constexpr _inline float lengthSq(const ml::vec2<T> &v){
    return (v.x*v.x + v.y*v.y);
}

template<typename T>constexpr _inline T lengthSq( ml::vec3<T> const &v){
    return (v.x*v.x + v.y*v.y + v.z*v.z);
}

template<typename T>constexpr _inline T lengthSq( ml::vec4<T> const &v){
    return (v.x*v.x + v.y*v.y + v.z*v.z + v.w*v.w);
}

template<typename T>constexpr _inline float distance(const T &p1,const T &p2){
    return length(p1-p2);
}

template<typename T>constexpr _inline auto distanceSq(const T &p1,const T &p2){
    return lengthSq(p1-p2);
}
template<typename T>constexpr _inline ml::vec2<T> ceil(const ml::vec2f &v){
    return ml::vec2<T>(ml::ceil(v.x),ml::ceil(v.y));
}

template<typename T>constexpr _inline ml::vec3<T> ceil(const ml::vec3f &v){
    return ml::vec3<T>(ml::ceil(v.x),ml::ceil(v.y), ml::ceil(v.z));
}

template<typename T>constexpr _inline ml::vec4<T> ceil(const ml::vec4f &v){
    return ml::vec4<T>(ml::ceil(v.x),ml::ceil(v.y), ml::ceil(v.z), ml::ceil(v.w));
}

template<typename T>constexpr _inline ml::vec2<T> floor(const ml::vec2f &v){
    return ml::vec2<T>(ml::floor(v.x),ml::floor(v.y));
}

template<typename T>constexpr _inline ml::vec3<T> floor(const ml::vec3f &v){
    return ml::vec3i(ml::floor(v.x),ml::floor(v.y), ml::floor(v.z));
}

template<typename T>constexpr _inline ml::vec4<T> floor(const ml::vec4f& v){
    return ml::vec4<T>(ml::floor(v.x),ml::floor(v.y), ml::floor(v.z), ml::floor(v.w));
}


template<typename T>constexpr _inline ml::vec2<T> clamp(const ml::vec2<T> &a, const ml::vec2<T> &min, const ml::vec2<T> &max){
    return ml::vec2<T>(ml::clamp(a.x, min.x, max.x), ml::clamp(a.y, min.y, max.y));
}

template<typename T>constexpr _inline ml::vec3<T> clamp(const ml::vec3<T> &a, const ml::vec3<T> &min, const ml::vec3<T> &max){
    return ml::vec3<T>(ml::clamp(a.x, min.x, max.x),ml::clamp(a.y, min.y, max.y), ml::clamp(a.z, min.z, max.z));
}

template<typename T>constexpr _inline ml::vec4<T> clamp(const ml::vec4<T> &a, const ml::vec4<T> &min, const ml::vec4<T> &max){
    return ml::vec4<T>(ml::clamp(a.x, min.x, max.x),ml::clamp(a.y, min.y, max.y),
                     ml::clamp(a.z, min.z, max.z),ml::clamp(a.w, min.w, max.w));
}

template<typename T>constexpr _inline ml::vec2<T> min(const ml::vec2<T> &a,const ml::vec2<T> &b){
    return ml::vec2<T>(ml::min(a.x, b.x), ml::min(a.y, b.y));
}

template<typename T>constexpr _inline ml::vec3<T> min(const ml::vec3<T> &a,const ml::vec3<T> &b){
    return ml::vec3<T>(ml::min(a.x, b.x), ml::min(a.y, b.y), ml::min(a.z, b.z));
}

template<typename T>constexpr _inline ml::vec4<T> min(const ml::vec4<T> &a,const ml::vec4<T> &b){
    return ml::vec4<T>(ml::min(a.x, b.x), ml::min(a.y, b.y), ml::min(a.z, b.z), ml::min(a.w, b.w));
}

template<typename T>constexpr _inline ml::vec2<T> max(const ml::vec2<T> &a,const ml::vec2<T>& b){
    return ml::vec2<T>(ml::max(a.x, b.x), ml::max(a.y, b.y));
}

template<typename T>constexpr _inline ml::vec3<T> max(const ml::vec3<T> &a,const ml::vec3<T> &b){
    return ml::vec3<T>(ml::max(a.x, b.x), ml::max(a.y, b.y), ml::max(a.z, b.z));
}

template<typename T>constexpr _inline ml::vec4<T> max(const ml::vec4<T> &a,const ml::vec4<T> &b){
    return ml::vec4<T>(ml::max(a.x, b.x), ml::max(a.y, b.y), ml::max(a.z, b.z), ml::max(a.w, b.w));
}

template<typename T>constexpr _inline ml::vec3<T> cross(const ml::vec3<T> &a,const ml::vec3<T> &b){
    return ml::vec3<T>(a.y*b.z - b.y*a.z, b.x*a.z - a.x*b.z, a.x*b.y - a.y*b.x);
}

template<typename T>constexpr _inline T cross(const ml::vec2<T> &a,const ml::vec2<T> &b){
    return (a.x*b.y - a.y*b.x);
}

template<typename T>constexpr _inline vec2<T> cross(const vec2<T>& a, const float s )
{
  return vec2<T>( s * a.y, -s * a.x );
}

template<typename T>constexpr _inline vec2<T> cross(const float s, const vec2<T>& a )
{
  return vec2<T>( -s * a.y, s * a.x );
}

template<typename T>constexpr _inline ml::vec2<T> perp(const ml::vec2<T> &v){
    return ml::vec2<T>(-v.y, v.x);
}

template<typename T>constexpr _inline T normalize(const T v){
    return v*(1.f/ml::length(v));
}


template<typename T>constexpr _inline ml::vec2<T> truncate(const ml::vec2<T> &a, const float b){
    T l = ml::length(a);
    return (l > b) ? (a*(b/l)) : a;
}

template<typename T>_inline ml::vec2<T> truncater(const ml::vec2<T> &a, float &b){
    b = ml::min(ml::length(a), b);
    return ml::normalize(a)*b;
}

template<typename T>_inline void angleBetween2(const ml::vec3<T> &v1, const ml::vec3<T> &v2, ml::vec3<T> &axis, float &angle) {
    // turn vectors into unit vectors
    ml::vec3<T> n1 = ml::normalize(v1);
    ml::vec3<T> n2 = ml::normalize(v2);
    angle = ml::acos(ml::dot(n1,n2));
    // if no noticable rotation is available return zero rotation to avoid cross product artifacts
    axis = (ml::abs(angle) < 0.0001f) ? ml::vec3<T>(0.f,0.f,1.f) : ml::normalize(ml::cross(n1, n2));
}

template<typename T>constexpr _inline ml::mat4<T> translate(const ml::mat4<T> &m, const ml::vec3<T> &v){
    return ml::mat4<T>(m[0], m[1], m[2], m[0] * v[0] + m[1] * v[1] + m[2] * v[2] + m[3]);
}

template<typename T>constexpr _inline ml::mat4<T> scale(const ml::mat4<T> &m, const ml::vec3<T> &v){
    return ml::mat4<T>(m[0] * v[0], m[1] * v[1], m[2] * v[2], m[3]);
}

template<typename T>constexpr _inline ml::mat4<T> lookAt(ml::vec3<T> const &pos,ml::vec3<T> const &target,ml::vec3<T> const &up){
    const ml::vec3<T> forward = ml::normalize(target - pos),
    side    = ml::normalize(ml::cross(forward, up)),
    _up     = ml::cross(side, forward);

    return ml::translate(ml::mat4<T>(side.x, _up.x, -forward.x,0.f,
                                    side.y, _up.y, -forward.y,0.f,
                                    side.z, _up.z, -forward.z,0.f,
                                    0.f,    0.f,   0.f,      1.f), -pos);
}

template<typename T>constexpr _inline ml::mat4<T> perspective(const float fovy,const float aspect,const float zNear,const float zFar){
    const float tanHalfFovy = ml::tan(ml::radians(fovy)*0.5f);
    ml::mat4<T> m = ml::mat4<T>(1.f);

    m[0][0] = 1.f/(tanHalfFovy * aspect);
    m[1][1] = 1.f/tanHalfFovy;
    m[2][2] = -(zFar + zNear) / (zFar - zNear);
    m[2][3] = -1.f;
    m[3][2] = -(2.f * zFar * zNear) / (zFar - zNear);

    return m;
}



template<typename T>constexpr _inline ml::mat4<T> rotate(const ml::mat4<T> &m, const float angle, const ml::vec3<T> &v)
{
    const float c = ml::cos(ml::radians(angle)),oc = 1.f - c, s = ml::sin(ml::radians(angle));    // sine
    const ml::vec3<T> x = v * (v.x * oc), y = ml::vec3<T>(v.y*v.y, v.y*v.z,v.z*v.z) * oc, _v = v * s;

    return m * ml::mat4<T>(x.x + c,      x.y - _v.z, x.z + _v.y, 0.f,
                         x.y + _v.z,   y.x + c,    y.y - _v.x, 0.f,
                         x.z - _v.y,   y.y + _v.x, y.z + c,    0.f,
                         0.f,          0.f,        0.f,        1.f);
}

template<typename T>constexpr _inline ml::mat4<T> rotateM(const float angle, const ml::vec3<T> &v){
    const float c = ml::cos(ml::radians(angle)),oc = 1.f - c, s = ml::sin(ml::radians(angle));    // sine
    const ml::vec3<T> x = v * (v.x * oc), y = ml::vec3<T>(v.y*v.y, v.y*v.z,v.z*v.z) * oc, _v = v * s;

    return ml::mat4<T>(x.x + c,      x.y - _v.z, x.z + _v.y, 0.f,
                     x.y + _v.z,   y.x + c,    y.y - _v.x, 0.f,
                     x.z - _v.y,   y.y + _v.x, y.z + c,    0.f,
                     0.f,          0.f,        0.f,        1.f);
}

template<typename T>constexpr _inline ml::mat4<T> rotateM(const ml::vec3<T> &a) {
    return ((a.x != 0.f) ? rotateM(a.x, ml::vec3<T>(1.f,0.f,0.f)) : ml::mat4<T>(1.f)) *
           ((a.y != 0.f) ? rotateM(a.y, ml::vec3<T>(0.f,1.f,0.f)) : ml::mat4<T>(1.f)) *
           ((a.z != 0.f) ? rotateM(a.z, ml::vec3<T>(0.f,0.f,1.f)) : ml::mat4<T>(1.f));
}


template<typename T>constexpr _inline ml::vec3<T> unproject(const float x,const float y,const float w,const float h,
                                                           const ml::mat4<T> &proj,const ml::mat4<T> &mv,const float depth){

    ml::vec4<T> r = ml::vec4<T>((x/w)*2.f-1.f,//0.f
                            (y/h)*2.f-1.f,//0.f
                            depth*2.f-1.f,
                             1.f);
    ml::vec4<T> rw = ml::inverse(proj * mv) * r;
    return ml::vec3<T>(rw) / rw.w;
}

template<typename T>constexpr _inline ml::mat2<T> inverse(const ml::mat2<T> &m){
    float32 det = ml::cross(m[0], m[1]);
    det = (det != 0.f) ? 1.f/det : 0.f;
    return ml::mat2<T>(m[1].y *   det,  m[1].x * (-det),
                       m[0].y * (-det), m[0].x *   det);
}
}

#endif // FUNCTIONS

