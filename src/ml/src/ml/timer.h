#ifndef TIMER_H
#define TIMER_H

#include "common.h"
#include <chrono>

namespace ml{

    //http://stackoverflow.com/questions/13431237/improving-a-timer-class-based-on-chrono

/*namespace time{
    typedef std::chrono::hours hour;
    typedef std::chrono::minutes min;
    typedef std::chrono::seconds sec;
    typedef std::chrono::milliseconds ms;
    typedef std::chrono::microseconds us;
    typedef std::chrono::nanoseconds ns;
}*/

typedef std::ratio<3600> thour;
typedef std::ratio<60> tmin;
typedef std::ratio<1> tsec;
typedef std::milli tms;
typedef std::micro tus;
typedef std::nano tns;

typedef std::chrono::high_resolution_clock tclock;
typedef tclock::time_point tpoint;
typedef tclock::duration tdur;

    /*class timer{
    public:

        void start(){
            epoch = Clock::now();
        }

        template<typename unit>uint64 dur() const{
            return std::chrono::duration_cast<unit>(Clock::now()-epoch).count();
        }

        template<typename unit>float durf() const{
            std::chrono::duration<float> d = Clock::now()-epoch;
            return std::chrono::duration_cast<unit>(d);
        }

    private:
        Clock::time_point epoch;
    };*/

    _inline tpoint now() noexcept{
        return tclock::now();
    }
    template<typename T, typename U>constexpr T dur(const tpoint &tbig, const tpoint &tsmall) noexcept{
        return (std::chrono::duration_cast<std::chrono::duration<T, U>>(tbig-tsmall)).count();
    }

    template<typename T, typename U>constexpr T dur(const ml::tdur &t) noexcept{
        return (std::chrono::duration_cast<std::chrono::duration<T, U>>(t)).count();
    }
}
#endif // TIMER_H
