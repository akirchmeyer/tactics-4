#include <cstdint>
#include <cstddef>

typedef int_fast8_t      int8;
typedef int_fast16_t     int16;
typedef int_fast32_t     int32;
typedef int_fast64_t     int64;

typedef uint_fast8_t     uint8;
typedef uint_fast16_t    uint16;
typedef uint_fast32_t    uint32;
typedef uint_fast64_t    uint64;

typedef float float32;
typedef double float64;

#ifndef _inline
#define _inline inline
#endif

#define DEFINE_COPY(C, V) \
    constexpr C(const C&) noexcept = V; \
    C& operator=(const C&) noexcept = V;


#define DEFINE_MOVE(C, V) \
    constexpr C(C &&) noexcept = V; \
    C& operator=(C &&) noexcept = V;

#define DEFINE_COPY2(C, V) \
    C(const C&) noexcept = V; \
    C& operator=(const C&) noexcept = V;


#define DEFINE_MOVE2(C, V) \
    C(C &&) noexcept = V; \
    C& operator=(C &&) noexcept = V;
