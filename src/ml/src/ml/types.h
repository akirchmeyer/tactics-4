#ifndef MLTYPES_H
#define MLTYPES_H

#include "math.h"
#include <cassert>

namespace ml{

///VEC2
template<typename T> class vec2
{
public:
    T x, y;

    constexpr vec2() noexcept        : x(0), y(0){}
    constexpr vec2(const T _v) noexcept : x(_v), y(_v){}
    constexpr vec2(const T _x,const T _y) noexcept : x(_x), y(_y){}
    constexpr vec2(const T *_v) : x(_v[0]), y(_v[1]){}

    constexpr vec2(vec2<T> const& a) noexcept : x(a.x), y(a.y){}

    _inline vec2<T> operator= (vec2<T> const& a) noexcept{
        x = a.x; y = a.y;
        return *this;
    }
    _inline vec2<T> operator= (const T &a) noexcept{
        x = a; y = a;
        return *this;
    }
    constexpr _inline vec2<T> operator= (const T *a){
        x = a[0]; y = a[1];
        return *this;
    }

    constexpr _inline T&	operator[](const size_t i ) const noexcept { assert(i <= 2); return (&x)[i];}

    constexpr _inline friend bool operator==(vec2<T> const& a,vec2<T> const& b) noexcept{
        return (a.x == b.x) && (a.y == b.y);
    }
    constexpr _inline friend bool operator!=(vec2<T> const& a,vec2<T> const& b) noexcept{
        return (a.x != b.x) || (a.y != b.y);
    }

    _inline vec2<T> operator+=(vec2<T> const& _v) noexcept{
        x += _v.x;y += _v.y;
        return *this;
    }
    _inline vec2<T> operator-=(vec2<T> const& _v) noexcept{
        x -= _v.x;y -= _v.y;
        return *this;
    }
    _inline vec2<T> operator*=(vec2<T> const& _v) noexcept{
        x *= _v.x;y *= _v.y;
        return *this;
    }
    _inline vec2<T> operator/=(vec2<T> const& _v) noexcept{
        x /= _v.x;y /= _v.y;
        return *this;
    }
    _inline vec2<T> operator*=(const T& _v) noexcept{
        x *= _v;y *= _v;
        return *this;
    }
    _inline vec2<T> operator/=(const T& _v) noexcept{
        x /= _v;y /= _v;
        return *this;
    }
};

template<typename T> class vec3
{

public:
    T x, y, z;

    constexpr vec3() noexcept         : x(0), y(0), z(0){ }
    constexpr vec3(const T _v) : x(_v), y(_v), z(_v){    }
    constexpr vec3(const T _x, const T _y, const T _z) : x(_x), y(_y), z(_z){    }
    constexpr vec3(const T *_v) : x(_v[0]), y(_v[1]), z(_v[2]){    }
    constexpr vec3(const vec2<T> &v, const T _z) : x(v.x), y(v.y), z(_z){}
    constexpr vec3(vec3<T> const& a) : x(a.x), y(a.y), z(a.z){}

    _inline vec3 operator= (const vec3& a){
        x = a.x; y = a.y; z = a.z;
        return *this;
    }
    _inline vec3 operator= (const T a){
        x = a; y = a; z = a;
        return *this;
    }
    _inline vec3 operator= (const T *a){
        x = a[0]; y = a[1]; z = a[2];
        return *this;
    }

    constexpr _inline T&		operator[](const size_t i ) const {return (&x)[i];}

    constexpr friend _inline bool operator==(vec3 const& a,vec3 const& b){
        return (a.x == b.x) && (a.y == b.y) && (a.z == b.z);
    }
    constexpr friend _inline bool operator!=(vec3 const& a,vec3 const& b){
        return (a.x != b.x) || (a.y != b.y) || (a.z != b.z);
    }

    _inline vec3 operator+=(vec3 const& _v){
        x += _v.x;y += _v.y;z += _v.z;
        return *this;
    }
    _inline vec3 operator-=(vec3 const& _v){
        x -= _v.x;y -= _v.y;z -= _v.z;
        return *this;
    }
    _inline vec3 operator*=(vec3 const& _v){
        x *= _v.x;y *= _v.y;z *= _v.z;
        return *this;
    }
    _inline vec3 operator/=(vec3 const& _v){
        x /= _v.x;y /= _v.y;z /= _v.z;
        return *this;
    }

    _inline vec3 operator+=(T const& _v){
        x += _v;y += _v;z += _v;
        return *this;
    }
    _inline vec3 operator-=(T const& _v){
        x -= _v;y -= _v;z -= _v;
        return *this;
    }
    _inline vec3 operator*=(T const& _v){
        x *= _v;y *= _v;z *= _v;
        return *this;
    }
    _inline vec3 operator/=(T const& _v){
        x /= _v;y /= _v;z /= _v;
        return *this;
    }

    constexpr _inline const vec2<T> xy() const{
        return vec2<T>(x, y);
    }
    constexpr _inline const vec2<T> xz() const{
        return vec2<T>(x, z);
    }
    constexpr _inline const vec2<T> yz() const{
        return vec2<T>(y, z);
    }
};

template<typename T>constexpr _inline ml::vec3<T> operator-(vec3<T> const& a){
    return vec3<T>(-a.x,-a.y,-a.z);
}
template<typename T>constexpr _inline ml::vec3<T> operator+(vec3<T> const& a, vec3<T> const& b){
    return vec3<T>(a.x+b.x,a.y+b.y,a.z+b.z);
}
template<typename T>constexpr _inline ml::vec3<T> operator-(vec3<T> const& a,vec3<T> const& b){
    return vec3<T>(a.x-b.x,a.y-b.y,a.z-b.z);
}
template<typename T>constexpr _inline ml::vec3<T> operator*(vec3<T> const& a,vec3<T> const& b){
    return vec3<T>(a.x*b.x,a.y*b.y,a.z*b.z);
}
template<typename T>constexpr _inline ml::vec3<T> operator/(vec3<T> const& a,vec3<T> const& b){
    return vec3<T>(a.x/b.x,a.y/b.y,a.z/b.z);
}

template<typename T>constexpr _inline ml::vec3<T> operator+(vec3<T> const& a,T const& b){
    return vec3<T>(a.x+b,a.y+b,a.z+b);
}
template<typename T>constexpr _inline ml::vec3<T> operator-(vec3<T> const& a,T const& b){
    return vec3<T>(a.x-b,a.y-b,a.z-b);
}
template<typename T>constexpr _inline ml::vec3<T> operator*(vec3<T> const& a,T const& b){
    return vec3<T>(a.x*b,a.y*b,a.z*b);
}
template<typename T>constexpr _inline ml::vec3<T> operator/(vec3<T> const& a,T const& b){
    return vec3<T>(a.x/b,a.y/b,a.z/b);
}

///VEC4

template<typename T>class vec4
{
public:
    T x, y, z, w;

    constexpr vec4()         : x(0), y(0), z(0), w(0){    }
    constexpr vec4(const T v) : x(v), y(v), z(v), w(v){    }
    constexpr vec4(const T _x, const T _y, const T _z, const T _w) : x(_x), y(_y), z(_z), w(_w){    }
    constexpr vec4(const vec3<T> &v, const T _w) : x(v.x), y(v.y), z(v.z), w(_w){    }
    constexpr vec4(T *v) : x(v[0]), y(v[1]), z(v[2]), w(v[3]){    }
    constexpr vec4(vec4<T> const& a) : x(a.x), y(a.y), z(a.z), w(a.w){}

    _inline vec4<T> operator= (vec4<T> const& a){
        x = a.x; y = a.y; z = a.z; w = a.w;
        return *this;
    }
    _inline vec4<T> operator= (T const a){
        x = a; y = a; z = a; w = a;
        return *this;
    }
    _inline vec4<T> operator= (T const *a){
        x = a[0]; y = a[1]; z = a[2]; w = a[3];
        return *this;
    }

    constexpr T&		operator[] ( const size_t i ) const{return (&x)[i];}

    constexpr friend bool operator==(vec4<T> const& a,vec4<T> const& b){
        return (a.x == b.x) && (a.y == b.y) && (a.z == b.z) && (a.w == b.w);
    }
    constexpr friend bool operator!=(vec4<T> const& a,vec4<T> const& b){
        return (a.x != b.x) || (a.y != b.y) || (a.z != b.z) || (a.w != b.w);
    }

    _inline vec4<T>& operator+=(vec4<T> const& _v){
        x += _v.x;y += _v.y;z += _v.z; w += _v.w;
        return *this;
    }
    _inline vec4<T>& operator-=(vec4<T> const& _v){
        x -= _v.x;y -= _v.y;z -= _v.z; w -= _v.w;
        return *this;
    }
    _inline vec4<T>& operator*=(vec4<T> const& _v){
        x *= _v.x;y *= _v.y;z *= _v.z; w *= _v.w;
        return *this;
    }
    _inline vec4<T>& operator/=(vec4<T> const& _v){
        x /= _v.x;y /= _v.y;z /= _v.z; w /= _v.w;
        return *this;
    }

    _inline vec4<T>& operator+=(T const& _v){
        x += _v;y += _v;z += _v; w += _v;
        return *this;
    }
    _inline vec4<T>& operator-=(T const& _v){
        x -= _v;y -= _v;z -= _v; w -= _v;
        return *this;
    }
    _inline vec4<T>& operator*=(T const& _v){
        x *= _v;y *= _v;z *= _v; w *= _v;
        return *this;
    }
    _inline vec4<T>& operator/=(T const& _v){
        x /= _v;y /= _v;z /= _v; w /= _v;
        return *this;
    }

    constexpr _inline const vec3<T> xyz() const{
        return vec3<T>(x, y, z);
    }
};

template<typename T>constexpr _inline ml::vec4<T> operator-(vec4<T> const& a){
    return vec4<T>(-a.x,-a.y,-a.z,-a.w);
}

template<typename T>constexpr _inline ml::vec4<T> operator+(vec4<T> const& a,vec4<T> const& b){
    return vec4<T>(a.x+b.x,a.y+b.y,a.z+b.z,a.w+b.w);
}
template<typename T>constexpr _inline ml::vec4<T> operator-(vec4<T> const& a,vec4<T> const& b){
    return vec4<T>(a.x-b.x,a.y-b.y,a.z-b.z,a.w-b.w);
}
template<typename T>constexpr _inline ml::vec4<T> operator*(vec4<T> const& a,vec4<T> const& b){
    return vec4<T>(a.x*b.x,a.y*b.y,a.z*b.z,a.w*b.w);
}
template<typename T>constexpr _inline ml::vec4<T> operator/(vec4<T> const& a,vec4<T> const& b){
    return vec4<T>(a.x/b.x,a.y/b.y,a.z/b.z,a.w/b.w);
}

template<typename T>constexpr _inline ml::vec4<T> operator+(vec4<T> const& a,T const& b){
    return vec4<T>(a.x+b,a.y+b,a.z+b,a.w+b);
}
template<typename T>constexpr _inline ml::vec4<T> operator-(vec4<T> const& a,T const& b){
    return vec4<T>(a.x-b,a.y-b,a.z-b,a.w-b);
}
template<typename T>constexpr _inline ml::vec4<T> operator*(vec4<T> const& a,T const& b){
    return vec4<T>(a.x*b,a.y*b,a.z*b,a.w*b);
}
template<typename T>constexpr _inline ml::vec4<T> operator/(vec4<T> const& a,T const &b){
    return vec4<T>(a.x/b,a.y/b,a.z/b,a.w/b);
}

template<typename T>class mat2{
public:
    constexpr mat2() : m(vec2<T>(), vec2<T>()){    }
    constexpr mat2 rot(const T rad){
      T c = ml::cos(rad);
      T s = ml::sin(rad);

      m[0] = vec2<T>(c,-s);
      m[1] = vec2<T>(s,c);
    }
    constexpr mat2(mat2<T> const& a) : m(a.m){}

    constexpr mat2(const vec2<T> &r1, const vec2<T> &r2) : m(r1, r2){    }
    constexpr mat2(const vec2<T> *r) : m(r[0], r[1]){    }

    constexpr mat2(const T *v) : m(vec2<T>(v[0], v[1]), vec2<T>(v[2], v[3])){    }

    constexpr mat2(const T a, const T b, const T c, const T d) : m(vec2<T>(a, b), vec2<T>(c, d)){
    }

    constexpr const vec2<T> x() const{ return ml::vec2<T>(m[0].x, m[1].x);    }
    constexpr const vec2<T> y() const{ return ml::vec2<T>(m[0].y, m[1].y);    }

    constexpr const vec2<T>& right() const{ return m[0];    }
    constexpr const vec2<T>& up() const{ return m[1];  }

    constexpr vec2<T>&		operator[](const size_t i )	const			{return m[i];}

    mat2& operator+=(const mat2 &_m){
        m[0] += _m[0]; m[1] += _m[1];
        return *this;
    }

    mat2& operator-=(const mat2 &_m){
        m[0] -= _m[0]; m[1] -= _m[1];
        return *this;
    }

    mat2& operator*= (const T v){
        m[0] *= v; m[1] *= v;
        return *this;
    }

    friend bool operator== (const mat2 &a,const mat2 &b){
        return a[0] == b[0] && a[1] == b[1];
    }

    friend bool operator!= (mat2 const &a, mat2 const &b){
        return a[0] != b[0] || a[1] != b[1];
    }

    constexpr _inline vec2<T> rotwstols(const ml::vec2<T> &b) const{
        return vec2<T>( m[0].x * b.x + m[0].y * b.y, m[1].x * b.x + m[1].y * b.y);
    }

    constexpr _inline vec2<T> rotlstows(const ml::vec2<T> &b) const{
        return (m[0]*b.x + m[1]*b.y);
    }

   constexpr const mat2 operator*( const mat2& rhs ) const{
       return mat2(rhs.m[0]*m[0].x + rhs.m[1]*m[0].y,
                   rhs.m[0]*m[1].x + rhs.m[1]*m[1].y);
   }


private:
    vec2<T> m[2];
};

template<typename T>class mat4{
public:
    constexpr mat4() : m(vec4<T>(),vec4<T>(), vec4<T>(), vec4<T>()){ }
    constexpr mat4(T const d) : m(vec4<T>( d,0,0,0),
                                  vec4<T>( 0,d,0,0),
                                  vec4<T>( 0,0,d,0),
                                  vec4<T>( 0,0,0, d )){
    }
    constexpr mat4(mat4<T> const& a) : m(a.m){}

    constexpr mat4(const vec4<T> &r1, const vec4<T> &r2, const vec4<T> &r3, const vec4<T> &r4) : m(r1, r2, r3, r4){    }
    constexpr mat4(const vec4<T> *r) : m(r[0], r[1], r[2], r[3]){    }
    constexpr mat4(const T *v) :m(vec4<T>(v[0],  v[1],  v[2],  v[3] ), vec4<T>(v[4],  v[5],  v[6],  v[7] ),
                                  vec4<T>(v[8],  v[9],  v[10], v[11]), vec4<T>(v[12], v[13], v[14], v[15])){
    }

    constexpr mat4(const T a, const T b, const T c, const T d, const T e, const T f,const T g, const T h,
         const T i, const T j,const T k, const T l, const T _m, const T n,const T o, const T p)
        : m(vec4<T>(a, b, c, d), vec4<T>(e, f, g, h), vec4<T>(i, j, k, l), vec4<T>(_m, n, o, p)){
    }

    constexpr vec4<T>& front() const{ return m[0]; }
    constexpr vec4<T>& up()    const{ return m[1]; }
    constexpr vec4<T>& right() const{ return m[2]; }
    constexpr vec4<T>& pos()   const{ return m[3]; }

    constexpr vec4<T>&		operator[](const size_t i ) const {return m[i];}

    mat4 operator+=(const mat4 &_m){
        m[0] += _m[0]; m[1] += _m[1]; m[2] += _m[2]; m[3] += _m[3];
        return *this;
    }

    mat4 operator-=(const mat4 &_m){
        m[0] -= _m[0]; m[1] -= _m[1]; m[2] -= _m[2]; m[3] -= _m[3];
        return *this;
    }

    mat4 operator*= (const T v){
        m[0] *= v; m[1] *= v; m[2] *= v; m[3] *= v;
        return *this;
    }

    constexpr mat4 operator*= (const mat4 &m);
    constexpr mat4 operator/= (const mat4 &m);

    friend bool operator== (const mat4 &a,const mat4 &b){
        return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3];
    }

    friend bool operator!= (const mat4 &a,const mat4 &b){
        return a[0] != b[0] || a[1] != b[1] || a[2] != b[2] || a[3] != b[3];
    }

private:
    vec4<T> m[4];
};

//extern ml::mat4 tmp44;

template<typename T>constexpr _inline ml::mat4<T> operator-(mat4<T> const& a){
    return ml::mat4<T>(-a[0], -a[1], -a[2], -a[3]);
}

template<typename T>constexpr _inline ml::mat4<T> operator+(mat4<T> const &a,mat4<T> const& b){
    return (ml::mat4<T>(a) += b);
}
template<typename T>constexpr _inline ml::mat4<T> operator-(mat4<T> const &a,mat4<T> const& b){
    return (ml::mat4<T>(a) -= b);
}

template<typename T>constexpr _inline ml::mat4<T> operator/(mat4<T> const &a,mat4<T> const& b){
    return (ml::mat4<T>(a) /= b);
}

template<typename T>constexpr _inline ml::mat4<T> operator*(ml::mat4<T> const&a, ml::mat4<T> const&b){
    ml::mat4<T> c;

    c[0] = a[0]*b[0].x + a[1]*b[0].y + a[2]*b[0].z + a[3]*b[0].w;
    c[1] = a[0]*b[1].x + a[1]*b[1].y + a[2]*b[1].z + a[3]*b[1].w;
    c[0] = a[0]*b[2].x + a[1]*b[2].y + a[2]*b[2].z + a[3]*b[2].w;
    c[1] = a[0]*b[3].x + a[1]*b[3].y + a[2]*b[3].z + a[3]*b[3].w;

#if 0
    c[0][0] = b[0][0]*a[0][0] + b[0][1]*a[1][0] + b[0][2]*a[2][0] + b[0][3]*a[3][0];
    c[0][1] = b[0][0]*a[0][1] + b[0][1]*a[1][1] + b[0][2]*a[2][1] + b[0][3]*a[3][1];
    c[0][2] = b[0][0]*a[0][2] + b[0][1]*a[1][2] + b[0][2]*a[2][2] + b[0][3]*a[3][2];
    c[0][3] = b[0][0]*a[0][3] + b[0][1]*a[1][3] + b[0][2]*a[2][3] + b[0][3]*a[3][3];

    c[1][0] = b[1][0]*a[0][0] + b[1][1]*a[1][0] + b[1][2]*a[2][0] + b[1][3]*a[3][0];
    c[1][1] = b[1][0]*a[0][1] + b[1][1]*a[1][1] + b[1][2]*a[2][1] + b[1][3]*a[3][1];
    c[1][2] = b[1][0]*a[0][2] + b[1][1]*a[1][2] + b[1][2]*a[2][2] + b[1][3]*a[3][2];
    c[1][3] = b[1][0]*a[0][3] + b[1][1]*a[1][3] + b[1][2]*a[2][3] + b[1][3]*a[3][3];

    c[2][0] = b[2][0]*a[0][0] + b[2][1]*a[1][0] + b[2][2]*a[2][0] + b[2][3]*a[3][0];
    c[2][1] = b[2][0]*a[0][1] + b[2][1]*a[1][1] + b[2][2]*a[2][1] + b[2][3]*a[3][1];
    c[2][2] = b[2][0]*a[0][2] + b[2][1]*a[1][2] + b[2][2]*a[2][2] + b[2][3]*a[3][2];
    c[2][3] = b[2][0]*a[0][3] + b[2][1]*a[1][3] + b[2][2]*a[2][3] + b[2][3]*a[3][3];

    c[3][0] = b[3][0]*a[0][0] + b[3][1]*a[1][0] + b[3][2]*a[2][0] + b[3][3]*a[3][0];
    c[3][1] = b[3][0]*a[0][1] + b[3][1]*a[1][1] + b[3][2]*a[2][1] + b[3][3]*a[3][1];
    c[3][2] = b[3][0]*a[0][2] + b[3][1]*a[1][2] + b[3][2]*a[2][2] + b[3][3]*a[3][2];
    c[3][3] = b[3][0]*a[0][3] + b[3][1]*a[1][3] + b[3][2]*a[2][3] + b[3][3]*a[3][3];
#endif
    return c;
}

template<typename T>constexpr _inline T determinant(const ml::mat4<T> &m){
   return
   m[0][3]*m[1][2]*m[2][1]*m[3][0] - m[0][2]*m[1][3]*m[2][1]*m[3][0] - m[0][3]*m[1][1]*m[2][2]*m[3][0] + m[0][1]*m[1][3]*m[2][2]*m[3][0]+
   m[0][2]*m[1][1]*m[2][3]*m[3][0] - m[0][1]*m[1][2]*m[2][3]*m[3][0] - m[0][3]*m[1][2]*m[2][0]*m[3][1] + m[0][2]*m[1][3]*m[2][0]*m[3][1]+
   m[0][3]*m[1][0]*m[2][2]*m[3][1] - m[0][0]*m[1][3]*m[2][2]*m[3][1] - m[0][2]*m[1][0]*m[2][3]*m[3][1] + m[0][0]*m[1][2]*m[2][3]*m[3][1]+
   m[0][3]*m[1][1]*m[2][0]*m[3][2] - m[0][1]*m[1][3]*m[2][0]*m[3][2] - m[0][3]*m[1][0]*m[2][1]*m[3][2] + m[0][0]*m[1][3]*m[2][1]*m[3][2]+
   m[0][1]*m[1][0]*m[2][3]*m[3][2] - m[0][0]*m[1][1]*m[2][3]*m[3][2] - m[0][2]*m[1][1]*m[2][0]*m[3][3] + m[0][1]*m[1][2]*m[2][0]*m[3][3]+
   m[0][2]*m[1][0]*m[2][1]*m[3][3] - m[0][0]*m[1][2]*m[2][1]*m[3][3] - m[0][1]*m[1][0]*m[2][2]*m[3][3] + m[0][0]*m[1][1]*m[2][2]*m[3][3];
}

template<typename T>constexpr _inline ml::mat4<T> inverse(const ml::mat4<T> &m){
   ml::mat4<T> inv;

   inv[0][0] =  m[1][1] * m[2][2] * m[3][3] - m[1][1] * m[2][3] * m[3][2] - m[2][1] * m[1][2] * m[3][3] +
                m[2][1] * m[1][3] * m[3][2] + m[3][1] * m[1][2] * m[2][3] - m[3][1] * m[1][3] * m[2][2];

   inv[0][1] = -m[0][1] * m[2][2] * m[3][3] + m[0][1] * m[2][3] * m[3][2] + m[2][1] * m[0][2] * m[3][3] -
                m[2][1] * m[0][3] * m[3][2] - m[3][1] * m[0][2] * m[2][3] + m[3][1] * m[0][3] * m[2][2];

   inv[0][2] =  m[0][1] * m[1][2] * m[3][3] - m[0][1] * m[1][3] * m[3][2] - m[1][1] * m[0][2] * m[3][3] +
                m[1][1] * m[0][3] * m[3][2] + m[3][1] * m[0][2] * m[1][3] - m[3][1] * m[0][3] * m[1][2];

   inv[0][3] = -m[0][1] * m[1][2] * m[2][3] + m[0][1] * m[1][3] * m[2][2] + m[1][1] * m[0][2] * m[2][3] -
                m[1][1] * m[0][3] * m[2][2] - m[2][1] * m[0][2] * m[1][3] + m[2][1] * m[0][3] * m[1][2];

   inv[1][0] = -m[1][0] * m[2][2] * m[3][3] + m[1][0] * m[2][3] * m[3][2] + m[2][0] * m[1][2] * m[3][3] -
                m[2][0] * m[1][3] * m[3][2] - m[3][0] * m[1][2] * m[2][3] + m[3][0] * m[1][3] * m[2][2];

   inv[1][1] =  m[0][0] * m[2][2] * m[3][3] - m[0][0] * m[2][3] * m[3][2] - m[2][0] * m[0][2] * m[3][3] +
                m[2][0] * m[0][3] * m[3][2] + m[3][0] * m[0][2] * m[2][3] - m[3][0] * m[0][3] * m[2][2];

   inv[1][2] = -m[0][0] * m[1][2] * m[3][3] + m[0][0] * m[1][3] * m[3][2] + m[1][0] * m[0][2] * m[3][3] -
                m[1][0] * m[0][3] * m[3][2] - m[3][0] * m[0][2] * m[1][3] + m[3][0] * m[0][3] * m[1][2];

   inv[1][3] =  m[0][0] * m[1][2] * m[2][3] - m[0][0] * m[1][3] * m[2][2] - m[1][0] * m[0][2] * m[2][3] +
                m[1][0] * m[0][3] * m[2][2] + m[2][0] * m[0][2] * m[1][3] - m[2][0] * m[0][3] * m[1][2];

   inv[2][0] =  m[1][0] * m[2][1] * m[3][3] - m[1][0] * m[2][3] * m[3][1] - m[2][0] * m[1][1] * m[3][3] +
                m[2][0] * m[1][3] * m[3][1] + m[3][0] * m[1][1] * m[2][3] - m[3][0] * m[1][3] * m[2][1];

   inv[2][1] = -m[0][0] * m[2][1] * m[3][3] + m[0][0] * m[2][3] * m[3][1] + m[2][0] * m[0][1] * m[3][3] -
                m[2][0] * m[0][3] * m[3][1] - m[3][0] * m[0][1] * m[2][3] + m[3][0] * m[0][3] * m[2][1];

   inv[2][2] =  m[0][0] * m[1][1] * m[3][3] - m[0][0] * m[1][3] * m[3][1] - m[1][0] * m[0][1] * m[3][3] +
                m[1][0] * m[0][3] * m[3][1] + m[3][0] * m[0][1] * m[1][3] - m[3][0] * m[0][3] * m[1][1];

   inv[2][3] = -m[0][0] * m[1][1] * m[2][3] + m[0][0] * m[1][3] * m[2][1] + m[1][0] * m[0][1] * m[2][3] -
                m[1][0] * m[0][3] * m[2][1] - m[2][0] * m[0][1] * m[1][3] + m[2][0] * m[0][3] * m[1][1];

   inv[3][0] = -m[1][0] * m[2][1] * m[3][2] + m[1][0] * m[2][2] * m[3][1] + m[2][0] * m[1][1] * m[3][2] -
                m[2][0] * m[1][2] * m[3][1] - m[3][0] * m[1][1] * m[2][2] + m[3][0] * m[1][2] * m[2][1];

   inv[3][1] =  m[0][0] * m[2][1] * m[3][2] - m[0][0] * m[2][2] * m[3][1] - m[2][0] * m[0][1] * m[3][2] +
                m[2][0] * m[0][2] * m[3][1] + m[3][0] * m[0][1] * m[2][2] - m[3][0] * m[0][2] * m[2][1];

   inv[3][2] = -m[0][0] * m[1][1] * m[3][2] + m[0][0] * m[1][2] * m[3][1] + m[1][0] * m[0][1] * m[3][2] -
                m[1][0] * m[0][2] * m[3][1] - m[3][0] * m[0][1] * m[1][2] + m[3][0] * m[0][2] * m[1][1];

   inv[3][3] =  m[0][0] * m[1][1] * m[2][2] - m[0][0] * m[1][2] * m[2][1] - m[1][0] * m[0][1] * m[2][2] +
                m[1][0] * m[0][2] * m[2][1] + m[2][0] * m[0][1] * m[1][2] - m[2][0] * m[0][2] * m[1][1];

   T det = m[0][0] * inv[0][0] + m[0][1] * inv[1][0] + m[0][2] * inv[2][0] + m[0][3] * inv[3][0];

   return (det == 0) ? ml::mat4<T>(1.f) : inv*(1.f/det);
}

template<typename T>constexpr _inline mat2<T> transpose(const ml::mat2<T> &m){
    return mat2<T>( m[0].x, m[1].x, m[0].y, m[1].y);
}

template<typename T>constexpr _inline ml::mat4<T> transpose(const ml::mat4<T> &m){
    return ml::mat4<T>(ml::vec4<T>(m[0].x, m[1].x, m[2].x, m[3].x),
                    ml::vec4<T>(m[0].y, m[1].y, m[2].y, m[3].y),
                    ml::vec4<T>(m[0].z, m[1].z, m[2].z, m[3].z),
                    ml::vec4<T>(m[0].w, m[1].w, m[2].w, m[3].w));
}

template<typename T>constexpr _inline ml::mat4<T> mat4<T>::operator*=(const ml::mat4<T> &n){
    return (*this = *this*n);
}

template<typename T>constexpr _inline ml::mat4<T> mat4<T>::operator/=(const ml::mat4<T> &n){
    return (*this *= inverse(n));
}

template<typename T>constexpr _inline ml::vec4<T> operator*(const mat4<T> &a, const vec4<T> &b){
    //ROW ORDER
    return ml::vec4<T>(a[0].x*b.x+a[1].x*b.y+a[2].x*b.z+a[3].x*b.w,
                     a[0].y*b.x+a[1].y*b.y+a[2].y*b.z+a[3].y*b.w,
                     a[0].z*b.x+a[1].z*b.y+a[2].z*b.z+a[3].z*b.w,
                     a[0].w*b.x+a[1].w*b.y+a[2].w*b.z+a[3].w*b.w);
    //COLUMN ORDER
    //return ml::vec4<T>(ml::dot(a[0], b), ml::dot(a[1], b), ml::dot(a[2], b), ml::dot(a[3], b));
}

template<typename T>constexpr _inline ml::vec2<T> operator-(vec2<T> const& a){
    return vec2<T>(-a.x,-a.y);
}
template<typename T>constexpr _inline ml::vec2<T> operator+(vec2<T> const& a,vec2<T> const& b){
    return vec2<T>(a.x+b.x,a.y+b.y);
}
template<typename T>constexpr _inline ml::vec2<T> operator-(vec2<T> const& a,vec2<T> const& b){
    return vec2<T>(a.x-b.x,a.y-b.y);
}
template<typename T>constexpr _inline ml::vec2<T> operator*(vec2<T> const& a,vec2<T> const& b){
    return vec2<T>(a.x*b.x,a.y*b.y);
}
template<typename T>constexpr _inline ml::vec2<T> operator/(vec2<T> const& a,vec2<T> const& b){
    return vec2<T>(a.x/b.x,a.y/b.y);
}

template<typename T>_inline ml::vec2<T> operator+(vec2<T> const& a,T const& b){
    return vec2<T>(a.x+b,a.y+b);
}
template<typename T>_inline ml::vec2<T> operator-(vec2<T> const& a,T const& b){
    return vec2<T>(a.x-b,a.y-b);
}
template<typename T>_inline ml::vec2<T> operator*(vec2<T> const& a,T const& b){
    return vec2<T>(a.x*b,a.y*b);
}
template<typename T>_inline ml::vec2<T> operator/(vec2<T> const& a,T const& b){
    return vec2<T>(a.x/b,a.y/b);
}
template<typename T>constexpr ml::vec2<T> cast3(const vec3<T> &v){ return ml::vec2<T>(v.x, v.y); }
template<typename T>constexpr ml::vec3<T> cast4(const vec4<T> &v){ return ml::vec3<T>(v.x, v.y, v.z); }


typedef vec2<int32> vec2i;
typedef vec2<uint32> vec2u;
typedef vec2<float> vec2f;

typedef vec3<int32> vec3i;
typedef vec3<uint32> vec3u;
typedef vec3<float> vec3f;

typedef vec4<int32> vec4i;
typedef vec4<uint32> vec4u;
typedef vec4<float> vec4f;

typedef mat2<int32> mat2i;
typedef mat2<uint32> mat2u;
typedef mat2<float> mat2f;


typedef mat4<int32> mat4i;
typedef mat4<uint32> mat4u;
typedef mat4<float> mat4f;

template<typename T, typename U>constexpr _inline vec2<T> cast(const vec2<U> &a){ return vec2<T>((T)a.x, (T)a.y); }
template<typename T, typename U>constexpr _inline vec3<T> cast(const vec3<U> &a){ return vec3<T>((T)a.x, (T)a.y, (T)a.z); }
template<typename T, typename U>constexpr _inline vec4<T> cast(const vec4<U> &a){ return vec4<T>((T)a.x, (T)a.y, (T)a.z, (T)a.w); }
}


namespace ext{
template<typename T>class num_hash{
public:
    size_t operator()(ml::vec2<T> const& a) const
    {
        std::size_t h1 = std::hash<T>()(a.x);
        std::size_t h2 = std::hash<T>()(a.y);
        return h1 ^ (h2 << 1);
    }
};
}

#endif // MLTYPES_H
