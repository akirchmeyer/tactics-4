#ifdef ENABLE_GL
#ifndef INPUT_H
#define INPUT_H

#include <bitset>
#include <unordered_map>
#include <GLFW/glfw3.h>

#include "ml/types.h"
#include "ul/common.h"

#define GLFW_NMB 8

struct baction{
    constexpr baction() noexcept : button(0), action(0), mods(0) {}
    constexpr baction(int32 const b, int32 const a, int32 const m) noexcept : button(b), action(a), mods(m) {}
    int32 button, action, mods;
};

struct kaction{
    constexpr kaction() noexcept : key(0), scancode(0), action(0), mods(0) {}
    constexpr kaction(int32 const k, int32 const sc, int32 const a, int32 const m) noexcept : key(k), scancode(sc), action(a), mods(m) {}
    int32 key, scancode, action, mods;
};

class Input{
public:
    Input() noexcept : win(nullptr), mouse(0.f), scroll(0.f), size(0){reset();}
    ~Input() noexcept{
        reset();
        win = nullptr;
        io::get("this")->debug("deleted input\n");
    }

    const ml::vec2f getMousePos() const{
        assert(win);
        double x, y;
        glfwGetCursorPos(win, &x, &y);
        return ml::vec2f(x, y);
    }
    const ml::vec2i getWinPos() const{
        assert(win);
        int x, y;
        glfwGetWindowPos(win, &x, &y);
        return ml::vec2i(x, y);
    }
    const ml::vec2i getWinSize() const{
        assert(win);
        int w, h;
        glfwGetWindowSize(win, &w, &h);
        return ml::vec2i(w, h);
    }

    static void MouseButton(GLFWwindow *window, int32 const b, int32 const a, int32 const mods){
        assert(window);
        Input *inp = (Input*)glfwGetWindowUserPointer(window);
        assert(inp);
        inp->activate(MOUSE_BUTTON); inp->activate((a != GLFW_RELEASE) ? MOUSE_PRESS : MOUSE_RELEASE);
        inp->buttons[b] = baction(b, a, mods);
    }

    static void MouseMove(GLFWwindow *window, double const xpos, double const ypos){
        assert(window);
        Input *inp = (Input*)glfwGetWindowUserPointer(window);
        assert(inp);
        inp->activate(MOUSE_MOVE);
        inp->mouse += ml::vec2f(xpos,ypos);
    }

    static void MouseScroll(GLFWwindow *window, double const xoffset, double const yoffset){
        assert(window);
        Input *inp = (Input*)glfwGetWindowUserPointer(window);
        assert(inp);
        inp->activate(MOUSE_SCROLL);
        inp->scroll += ml::vec2f(xoffset, yoffset);
    }

    static void KeyButton(GLFWwindow* window, int32 const k, int32 const scancode, int32 const a, int32 const mods){
        assert(window);
        Input *inp = (Input*)glfwGetWindowUserPointer(window);
        assert(inp);
        inp->activate(KEY_BUTTON); inp->activate((a != GLFW_RELEASE) ? KEY_PRESS : KEY_RELEASE);
        inp->keys[k] = kaction(k, scancode, a, mods);
    }

    static void Resize(GLFWwindow *window, int const width, int const height){
        assert(window);
        Input *inp = (Input*)glfwGetWindowUserPointer(window);
        assert(inp);
        inp->activate(WIN_RESIZE);
        inp->size = ml::vec2i(width, height);
    }

    static void Close(GLFWwindow *window){
        assert(window);
        Input *inp = (Input*)glfwGetWindowUserPointer(window);
        assert(inp);
        inp->activate(WIN_CLOSE);
    }

    void bind(GLFWwindow *w){
        assert(w);
        glfwSetInputMode(w, GLFW_STICKY_KEYS, 1);
        glfwSetWindowUserPointer(w, this);
        win = w;

        glfwSetMouseButtonCallback(win, Input::MouseButton);
        glfwSetScrollCallback(win, Input::MouseScroll);
        glfwSetCursorPosCallback(win, Input::MouseMove);
        glfwSetKeyCallback(win, Input::KeyButton);

        glfwSetWindowCloseCallback(win, Input::Close);
        glfwSetWindowSizeCallback(win, Input::Resize);
    }

    ///KEYS
    _inline bool kactive(const int32 k) const noexcept{
        return (keys.find(k) != keys.end());
    }
    bool key(const int32 k, kaction* const r) const noexcept{
        auto i = keys.find(k);
        if(i != keys.end()){
            *r = (*i).second; return true;
        }
        return false;
    }
    _inline bool keydown(const int32 k) const noexcept{
        return (glfwGetKey(win, k) != GLFW_RELEASE);
    }
    /*_inline bool keyup(const int32 k){
        return (glfwGetKey(win, k) == GLFW_RELEASE);
    }*/
    _inline bool keyup(const int32 k) const noexcept{
        kaction r;
        return (key(k, &r)) ? r.action == GLFW_RELEASE : false;
    }

    void reset() noexcept{
        event.reset(); buttons.clear(); keys.clear(); mouse = ml::vec2f(0.f); scroll = ml::vec2f(0.f); size = ml::vec2i(0);
    }
    _inline bool active(const int32 e) const noexcept{
        return event.test(e);
    }

    ///BUTTONS
    _inline bool bactive(const int32 b) const noexcept{
        return (buttons.find(b) != buttons.end());
    }
    bool button(const int32 b, baction* const r) const noexcept{
        auto i = buttons.find(b);
        if(i != buttons.end()){
            *r = (*i).second; return true;
        }
        return false;
    }
    _inline bool mousedown(const int32 m) const noexcept{
        return (glfwGetMouseButton(win, m) != GLFW_RELEASE);
    }
    _inline bool mouseup(const int32 m) const noexcept{
        baction r;
        return (button(m, &r)) ? r.action == GLFW_RELEASE : false;
    }

    void activate(int32 const e) noexcept{
        event.set(e, true);
    }

    enum { // keyCodes
        MOUSE_BUTTON = 0,
        MOUSE_MOVE = 1,
        MOUSE_SCROLL = 2,
        KEY_BUTTON = 3,
        WIN_RESIZE = 4,
        WIN_CLOSE  = 5,
        MOUSE_PRESS = 6,
        MOUSE_RELEASE = 7,
        KEY_PRESS = 8,
        KEY_RELEASE = 9,
        SIZE = 10
    };

    GLFWwindow *win;
    ml::vec2f mouse, scroll;
    ml::vec2i size;

private:
    std::bitset<SIZE> event;
    std::unordered_map<int32, baction> buttons;
    std::unordered_map<int32, kaction> keys;
};

#endif // INPUT_H
#endif
