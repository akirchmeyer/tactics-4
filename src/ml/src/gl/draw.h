#ifdef ENABLE_GL
#ifndef DRAW_H
#define DRAW_H

#include <GL/glew.h>
#include "ml/types.h"
#include "ul/common.h"

namespace gl{
_inline void Blend(const bool on){
    if(on){
        glEnable(GL_BLEND);     // Turn Blending On
        glDepthMask( GL_FALSE );//glDisable(GL_DEPTH_TEST);   // Turn Depth Testing Off
    }
    else{
        glDisable(GL_BLEND);        // Turn Blending Off
        glDepthMask( GL_TRUE );//glEnable(GL_DEPTH_TEST);    // Turn Depth Testing On
    }
    //glColor4ub(255, 255, 255, 0);
}

_inline void Translate(ml::vec3f const &v){
    glTranslatef(v.x, v.y, v.z);
}

_inline void Translate(ml::vec2f const &v){
    glTranslatef(v.x, v.y, 0.f);
}

_inline void Color(ml::vec3f const &v){
    glColor3f(v.x, v.y, v.z);
}

_inline void Color(ml::vec4f const &v){
    glColor4f(v.x, v.y, v.z, v.w);
}

_inline void Vertex(ml::vec2f const &v){
    glVertex2f(v.x, v.y);
}

_inline void TexCoord(ml::vec2f const &v){
    glTexCoord2f(v.x, v.y);
}


_inline void Scale(ml::vec3f const &v){
    glScalef(v.x, v.y, v.z);
}
_inline void Rotate(float const a, ml::vec3f const &v){
    glRotatef(a, v.x, v.y, v.z);
}

_inline void DrawLine(ml::vec2f const &p0, ml::vec2f const &p1){
    glBegin(GL_LINES);
        glVertex2f(p0.x, p0.y);
        glVertex2f(p1.x, p1.y);
    glEnd();
}

_inline void DrawAABB(ml::vec2f const &p1, ml::vec2f const &p2){
    glBegin(GL_LINES);
        glVertex2f(p1.x, p1.y);//->
        glVertex2f(p2.x, p1.y);

        glVertex2f(p2.x, p1.y);//|
        glVertex2f(p2.x, p2.y);//v

        glVertex2f(p2.x, p2.y);//<-
        glVertex2f(p1.x, p2.y);

        glVertex2f(p1.x, p2.y);//^
        glVertex2f(p1.x, p1.y);//|
    glEnd();
}

_inline void DrawQuad(ml::vec2f const &p0, ml::vec2f const &p1){
    glBegin(GL_TRIANGLE_STRIP);
        glTexCoord2f(0.f, 0.f); glVertex2f(p0.x, p0.y);
        glTexCoord2f(0.f, 1.f); glVertex2f(p0.x, p1.y);
        glTexCoord2f(1.f, 0.f); glVertex2f(p1.x, p0.y);
        glTexCoord2f(1.f, 1.f); glVertex2f(p1.x, p1.y);
    glEnd();
}

_inline void DrawQuad(ml::vec2f const &s){
    glBegin(GL_TRIANGLE_STRIP);
        glTexCoord2f(0.0, 0.0f); glVertex2f(0, 0);
        glTexCoord2f(0.0, 1.0f); glVertex2f(0, s.y);
        glTexCoord2f(1.0, 0.0f); glVertex2f(s.x, 0);
        glTexCoord2f(1.0, 1.0f); glVertex2f(s.x, s.y);
    glEnd();
}

_inline void Init(){
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
}

#endif // DRAW_H
#endif
