#ifdef ENABLE_GL
#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <ul/io.h>
#include "ml/types.h"
#include "ul/common.h"

#include "draw.h"


class camera : public del
{
public:
    camera(ml::vec2f const &p, ml::vec2f const &w, const float zoom = 1.f) noexcept : pos(p), hws(w*0.5f), z(zoom){}
    virtual ~camera() noexcept{
        io::get("this")->debug("deleted camera\n");
    }

    void resize(ml::vec2f const &w) noexcept{
        hws = w*0.5f;
        ortho();
    }
    void move(ml::vec2f const &p) noexcept{
        pos += p*z;
        look();
    }
    void mulvs(float const _z) noexcept{
        z *= (_z > 0.f) ? 2.f : 0.5f;
        ortho();
        look();
    }

    void ortho() const noexcept{
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        ml::vec2f v = hws*z;
        glOrtho(-v.x, v.x, v.y, -v.y, -10.f, 10.f);
    }

    void look() const noexcept{
        glMatrixMode (GL_MODELVIEW);
        glLoadIdentity();
        gl::Translate(-pos);
    }

    ml::vec2f const project(ml::vec2f const &c) const noexcept{
        return (c-hws)*z + pos;
    }

private:
    ml::vec2f pos, hws;//hws = 1/2*wsize
    float z;
};

#endif // CAMERA_H
#endif
