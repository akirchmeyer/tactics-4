#ifdef ENABLE_GL
#ifndef TEXTURE_H
#define TEXTURE_H

#include <ml/ml.h>
#include <GL/glew.h>

#include <stb/stb_image.h>
#include "ul/common.h"

#include "draw.h"

class Texture{
public:
    constexpr Texture() noexcept : data(nullptr), w(0), h(0), tid(0), comp(0) {}


     ~Texture() noexcept { reset(); io::get("this")->debug("deleted texture\n");}

    void load(const char* filename){
        int x, y, _comp;
        data = stbi_load(filename, &x, &y, &_comp, 0);
        if(!data) io::get("this")->critical("Error: unable to open file {}\n",filename);
        assert(data);

        w = x; h = y; comp = _comp;

        assert(w > 0);
        assert(h > 0);
    }

    static uptr<Texture> create(std::string filename, int filter = GL_LINEAR, int wrap = GL_CLAMP){
        uptr<Texture> tex = make_uptr<Texture>();//(Texture*)gc::create(new Texture());
        tex->load(filename.c_str());
        tex->init(filter, wrap);
        return tex;
    }

    void free(){
        if(data) stbi_image_free(data);
    }

    void bind() const{
        glBindTexture(GL_TEXTURE_2D, tid);
    }
    void unbind() const{
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void reset(){
        free();
        if(tid) glDeleteTextures(1, &tid);
    }

    void init(int filter = GL_LINEAR, int wrap = GL_CLAMP){
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &tid);
        glBindTexture(GL_TEXTURE_2D, tid);

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,filter);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, wrap);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, wrap);

        GLint format = GL_RGBA;
        if(comp == 1) format = GL_LUMINANCE;
        else if(comp == 3) format = GL_RGB;
        //else if(comp == 4) format = GL_RGBA;

        if(filter!=GL_LINEAR_MIPMAP_LINEAR)
            glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, GL_UNSIGNED_BYTE, data);
        else
            gluBuild2DMipmaps( GL_TEXTURE_2D, format, w, h, format, GL_UNSIGNED_BYTE, data);
    }



private:
    uint8* data;
    uint32 w, h;
    GLuint tid;
    uint8 comp;
};


#endif // TEXTURE_H
#endif
