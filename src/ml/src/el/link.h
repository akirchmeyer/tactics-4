#ifndef LINK_H
#define LINK_H

#include <functional>
#include <vector>
#include <tuple>
#include "el.h"

namespace el{

template<typename... Args> class event
{
public:
    typedef std::function<void(Args...)> func;

    event() noexcept : m_args(){}
    event(Args&&... a) noexcept : m_args(std::forward<Args>(a)...){}
    ~event() noexcept{
        close();
    }

    void open(Args&&... a) noexcept{
        m_args = std::make_tuple<Args...>(std::forward<Args>(a)...);
    }

    void connect(func const &b) noexcept{
        m_listeners.emplace_back(b);
    }
    /*void disconnect(func const &b){
        ext::erase(m_listeners, b);
    }*/
    void close() noexcept{
        m_listeners.clear();
    }

    void dispatch() const noexcept{
        std::for_each(m_listeners.begin(), m_listeners.end(), [this](func const &b){
            b(std::get<Args>(m_args)...);
        });
    }
    std::tuple<Args...> m_args;

private:
    std::vector<func> m_listeners;
};

}

#endif // LINK_H
