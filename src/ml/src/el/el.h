#ifndef EL_H
#define EL_H

#include <ml/common.h>
#include <vector>
#include <algorithm>
#include <functional>
#include <tbb/concurrent_vector.h>
/*#include <boost/range/iterator_range.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/iterator.hpp>*/

#define make_uptr std::make_unique
#define make_uvec ext::make_unique_vector
namespace ext{
template<typename T>class ptr_hash {
public:
    size_t operator()(const T* val) const {
        static const size_t shift = (size_t)std::log2(1 + sizeof(T));
        return (size_t)(val) >> shift;
    }
};

template <typename T, typename ... Args>
std::vector<std::unique_ptr<T>> make_unique_vector(Args&&... a)
{
    std::unique_ptr<T> init[] = {std::forward<Args>(a)...};
    return std::vector<std::unique_ptr<T>> { std::make_move_iterator(std::begin(init)), std::make_move_iterator(std::end(init))};
}

template<typename T>void erase(std::vector<T> &a, T b){
    a.erase(std::remove(a.begin(), a.end(), b), a.end());
}

template<typename T>void erase_if(std::vector<T> &a, std::function<bool(T&)> const &b){
    a.erase(std::remove_if(a.begin(), a.end(), b), a.end());
}

template<typename T>void erase_if(tbb::concurrent_vector<T> &a, std::function<bool(T&)> const &b){
    a.resize(std::distance(std::remove_if(a.begin(), a.end(), b), a.begin()));
}

template<typename T>void erase_if(std::vector<const T*> &a, std::function<bool(const T*)> const &b){
    a.resize(std::distance(std::remove_if(a.begin(), a.end(), b), a.begin()));
}
template<typename T>void erase_if(tbb::concurrent_vector<const T*> &a, std::function<bool(const T*)> const &b){
    a.resize(std::distance(std::remove_if(a.begin(), a.end(), b), a.begin()));
}

template<typename T> bool add_if(std::vector<T> &c, T const &b){
    if(std::none_of(c.begin(), c.end(), [&b](T const &a) { return (a == b); })){
        c.push_back(b);
        return true;
    }
    return false;
}

template<typename T> bool add_if(tbb::concurrent_vector<T> &c, T const &b){
    if(std::none_of(c.begin(), c.end(), [&b](T const &a) { return (a == b); })){
        c.push_back(b);
        return true;
    }
    return false;
}

//http://stackoverflow.com/questions/6953128/need-iterator-when-using-ranged-based-for-loops

template< typename iter > class range_iter : std::reference_wrapper<iter> {
public:
    constexpr range_iter( iter &in ) : std::reference_wrapper<iter>( in ) {}

    constexpr decltype( * std::declval<iter>() ) operator*() { return *this->get(); }

    constexpr iter &operator++(){ return ++this->get(); }
    constexpr friend bool operator!= ( range_iter const &l, range_iter const &r ) {  return l.get() != r.get();  }
};

template< typename iter > class range_proxy {
public:
    constexpr range_proxy( iter &in_first, iter const &in_last ) : first( in_first ), last( in_last ) {}

    constexpr range_iter<iter> begin() const { return first; }
    range_iter<iter> end() {  return last;  }

    iter &first;
    iter last;
};


template< typename iter > class const_range_proxy {
public:
    constexpr const_range_proxy( iter const &in_first, iter const &in_last ) : first( in_first ), last( in_last ) {}

    range_iter<iter> begin() { return first; }
    range_iter<iter> end()   {  return last; }

    iter first;
    iter last;
};

template<typename iter>constexpr const_range_proxy<iter> range( iter const &in_first, const iter &in_last )  {
    return const_range_proxy<iter>( in_first, in_last );
}

template<typename iter>constexpr range_proxy<iter> range( iter &in_first, const iter &in_last )  {
    return range_proxy<iter>( in_first, in_last );
}

template<typename T>std::vector<T> irange(T const from, T const to){
    return { from, to };
}
}
#endif // EL_H
