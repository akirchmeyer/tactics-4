TEMPLATE = lib
CONFIG += staticlib

CONFIG += BUILD_GLFW BUILD_STBI BUILD_GLEW BUILD_BOOST

include(../../deps.pri)

TARGET = $$NAME_ML

INCLUDEPATH += "./src"

HEADERS += \
    src/ml/math.h \
    src/ml/ml.h \
    src/ml/types.h \
    src/ml/common.h \
    src/ml/timer.h \
    src/ml/functions.h \
    src/ul/common.h \
    src/ul/console.h \
    src/gl/camera.h \
    src/gl/draw.h \
    src/gl/input.h \
    src/gl/texture.h \
    src/el/el.h \
    src/el/link.h \
    src/ul/io.h

SOURCES += \
    src/ul/common.cpp \
    src/el/el.cpp \
    src/ul/io.cpp
