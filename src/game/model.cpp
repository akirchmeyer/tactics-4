#include "model.h"
#include "dnx/mesh.h"
#include "entity.h"
#include <string>
#include <gl/draw.h>
#include <dnx/world.h>
#include "map.h"
#include "game_inst.h"

void mesh::free(){
    data::free();
    t.clear();
    if(glIsList(id))
        glDeleteLists(id, 1);
}


void mesh::draw(){
    glCallList(id);
}

void mesh::load(std::vector<ml::vec2f> _v, std::vector<uint16> _i, std::vector<ml::vec2f> _t){
    t.clear(); t = std::move(_t);
    data::load(_v, _i);
}

void mesh::init(){
    data::init();

    id = glGenLists(1);
    glNewList(id, GL_COMPILE);

    glBegin(GL_TRIANGLES);
    for(auto &j : i){
        gl::TexCoord(t[j]);
        gl::Vertex(m_verts[j].v);
    }
    glEnd();
    glEndList();
}

void Model::bind() const{
    m_tex->bind();
}

void Model::unbind() const{
    m_tex->unbind();
}

void Model::render(const entity *e) const{
    /*ROTATE + TRANSLATE*/
    glPushMatrix();
    gl::Translate(e->getPos());
    gl::Rotate(e->getRot().angle()*(180.f/ml::pi), ml::vec3f(1.f,0.f,1.f));
    m_mesh->draw();
    glPopMatrix();
}

entity* e_factory::construct(const dnx::transform &_t, const player *p, const factory *pt) const{
    assert(pt);

    uptr<mind> m((m_mind) ? m_mind->construct() : nullptr);
    uptr<hull> h((m_hull) ? m_hull->construct() : nullptr);
    uptr<engine> e((m_engine) ? m_engine->construct() : nullptr);
    uptr<vision> v((m_vision) ? m_vision->construct() : nullptr);

    uptr<entity> res = make_uptr<entity>(m_world, _t, m_props.get(), m_shape->duplicate(), pt, p, std::move(m), std::move(h), std::move(e), std::move(v));

    std::for_each(m_weapons.begin(), m_weapons.end(), [&res](const uptr<weapon> &w){
        res->addWeapon(w->construct());
    });
    return m_world->add(std::move(res));
}

factory::factory(GameInstance *w, const std::string &id, std::unique_ptr<Model> _gm, std::unique_ptr<mind> _mind, std::unique_ptr<hull> _hull, std::unique_ptr<engine> _engine, std::unique_ptr<vision> _vision, std::vector<std::unique_ptr<weapon> > &&_w, uptr<dnx::props> p)
    : m_gmod(std::move(_gm)), m_emod(nullptr), m_id(id){
    m_emod = std::make_unique<e_factory>(w, std::move(p), make_uptr<dnx::convex>(m_gmod->acqData()), std::move(_mind), std::move(_hull), std::move(_engine), std::move(_vision));
    assert(m_emod);
    assert(m_gmod);
    m_emod->setWeapons(std::move(_w));
}

factory::~factory() noexcept{
    io::get("this")->debug("deleted factory\n");
}

void factory::create(const ml::vec2f &pos, const player *p) const{
    create(dnx::transform(pos, ml::rot()), p);
}

void factory::create(const dnx::transform &t, const player *p) const{
    //dbg::push("create", _FILE, _LINE, _FUNC, code::ver);

    assert(m_emod);

    //uptr<entity> body;
    //sim::acqMap()->Add(ml::gen_id(), m_emod->construct(world, t, p, this));

    m_emod->construct(t, p, this);

    /*body->setUpdateCallback(UpdateCB);
    body->setData(m_emod->duplicate(body, p, this));*/

    //dbg::pop();
}
