TEMPLATE = app
CONFIG += console

CONFIG += BUILD_DNX BUILD_ML BUILD_GLFW BUILD_STBI BUILD_GLEW BUILD_TBB BUILD_BOOST

include(../../deps.pri)

TARGET = $$NAME_GAME

DEFINES += ENABLE_GL

SOURCES += main.cpp \
    window.cpp \
    user.cpp \
    entity.cpp \
    model.cpp \
    components.cpp \
    map.cpp \
    cd/hgrid.cpp \
    cd/cell.cpp \
    game_inst.cpp

HEADERS += \
    window.h \
    player.h \
    common.h \
    filters.h \
    user.h \
    entity.h \
    model.h \
    components.h \
    map.h \
    cd/hgrid.h \
    cd/cell.h \
    game_inst.h
