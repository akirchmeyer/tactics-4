#ifndef ENTITY_H
#define ENTITY_H

#include <dnx/body.h>
#include <el/link.h>

#include <functional>
#include "player.h"
#include "components.h"

class factory;
class entity;
class Model;

class ebase
{
public:
    ebase() noexcept : m_mind(nullptr), m_hull(nullptr), m_engine(nullptr), m_vision(nullptr), m_weapons(){}
    ebase(uptr<mind> _mind, uptr<hull> _hull, uptr<engine> _engine, uptr<vision> _vision) noexcept
        : m_mind(std::move(_mind)), m_hull(std::move(_hull)), m_engine(std::move(_engine)), m_vision(std::move(_vision)), m_weapons(){}
    virtual ~ebase() noexcept{
        m_weapons.clear();
    }

    DEFINE_COPY2(ebase, delete)
    DEFINE_MOVE2(ebase, default)


    virtual void addWeapon(uptr<weapon> w){
        m_weapons.push_back(std::move(w));
    }

    void setWeapons(std::vector<uptr<weapon>> &&w){
        m_weapons.clear();
        m_weapons = std::move(w);
    }

    _inline uptr<mind>   const& getMind()    const   { return m_mind; }
    _inline uptr<engine> const& getEngine()  const   { return m_engine; }
    _inline uptr<hull>   const& getHull()    const   { return m_hull; }
    _inline uptr<vision> const& getVision()  const   { return m_vision; }
    _inline uptr<weapon> const& getWeapon(uint32 i) const { return m_weapons[i]; }

    _inline uptr<hull> &acqHull()                  { return m_hull; }

    _inline size_t getNWeapon() const { return m_weapons.size(); }
    _inline bool isWEmpty()     const { return m_weapons.empty(); }

protected:
    uptr<mind> m_mind;
    uptr<hull> m_hull;
    uptr<engine> m_engine;
    uptr<vision> m_vision;
    std::vector<uptr<weapon>> m_weapons;
};

class entity final : public ebase{
public:
    entity(const GameInstance *w, const dnx::transform &_t, const dnx::props *props, dnx::shape *s, const factory *pt, const player *p, uptr<mind> _mind, uptr<hull> _hull, uptr<engine> _engine, uptr<vision> _vision) noexcept;
    virtual ~entity() noexcept;

    DEFINE_COPY2(entity, delete)
    DEFINE_MOVE2(entity, default)

    void addWeapon(uptr<weapon> w) override{
        w->setEntity(this);
        w->start();
        m_weapons.push_back(std::move(w));
    }

    /*void setWeapons(std::vector<uptr<weapon>> &_w){
        m_weapons.clear();
        m_weapons = std::move(_w);

        std::for_each(m_weapons.begin(), m_weapons.end(), [this](uptr<weapon> &w){
            w->setEntity(this);
            w->start();
        });
    }*/

    void setProto(const factory *fact){
        m_fact = fact;
    }

    _inline bool hasPlayer() const { return (m_player); }
    _inline const player *getPlayer() const{ return m_player; }

    void update();
    void draw() const;
    void drawhud() const;
    _inline bool getRender() const { return m_render;      }
    _inline void setRender(bool const b)   { m_render = b;      }

    const Model* getModel() const;
    const factory *getProto() const  { assert(m_fact); return m_fact;        }

    //_inline const entity* getBody() const{ return this;        }
    //_inline entity* body(){ assert(m_body); return m_body;        }

    void moveto(ml::vec2f const &v) noexcept{
        if(m_engine) m_engine->add(v);
    }

    bool isShieldActive() const noexcept{
        return (m_hull) ? m_hull->isShieldActive() : false;
    }
    void swapShield() noexcept{
        if(m_hull){
            if(isShieldActive()) m_hull->s_deactivate();
            else m_hull->s_activate();
        }
    }
    void boost() noexcept{
        if(m_engine) m_engine->boost();
    }
    void tscape();

    void listenMove(std::function<void(const entity*)> const& f) noexcept{
        e_move.connect(f);
    }
    void listenDestroy(std::function<void(const entity*)> const& f) noexcept{
        e_move.connect(f);
    }

    dnx::shape *setShape(dnx::shape *s){
        assert(s);
        dnx::shape *old = m_shape.release();
        m_shape = std::unique_ptr<dnx::shape>(s);
        m_shape->setTransform(&t);
        m_shape->calcAABB();
        return old;
    }

    void updateMov(){
        m_shape->calcAABB();
        m_mom = getPos()-m_ppos;
    }

    _inline const dnx::shape *getShape() const { assert(m_shape); return m_shape.get(); }
    _inline dnx::shape *acqShape() { assert(m_shape); return m_shape.get(); }
    //_inline del *data()          { assert(m_data); return m_data.get(); }
    _inline const dnx::props *getProps() const { assert(m_props); return m_props; }
    _inline const GameInstance *getWorld() const { assert(m_world); return m_world; }

    _inline bool isAlive() const { return m_alive; }

    _inline const dnx::aabb &getAABB() const  { return m_shape->getAABB(); }
    _inline const dnx::aabb getBounding() const { return dnx::aabb::fromvelo(m_shape->getAABB(), m_mom); }

    _inline float getMass() const { return m_props->mass(); }
    //_inline ml::vec2f &velo(){ return m_velo; }
    _inline const ml::vec2f getMomentum() const { return m_mom*m_props->mass(); }

    void setDestroy(){
        m_alive = false;
    }

    //_inline ml::rot &rot()    { return t.q; }
    _inline const ml::rot& getRot() const { return t.q; }
    _inline void setRot(const ml::rot &b) { t.q = b; }

    _inline const ml::vec2f& getPos() const { return t.p; }
    _inline void setPos(const ml::vec2f &b) { t.p = b; e_move.dispatch(); updateMov(); }
    //_inline ml::vec2f &acqPos()  { return t.p; }

    _inline uint32 getID() const { return m_id; }

    //_inline bool &active()  { return m_active; }
    _inline bool isActive() const { return m_active; }
    _inline void setActive(bool const b) { m_active = b; }

protected:
    std::unique_ptr<dnx::shape> m_shape;
    dnx::transform t;
    ml::vec2f m_ppos, m_mom/*, m_velo*/, m_acc;

    mutable el::event<const entity*> e_destroy, e_move;
    const factory* m_fact;
    const player* m_player;


    const GameInstance *m_world;
    const dnx::props *m_props;
    const uint32 m_id;


    bool m_alive, m_active,m_render;
};

#if 0
namespace sim{
    const entity *getEntity(const uint32 id);
    entity *acqEntity(const uint32 id);
    void add(const uint32 id, entity *e);
}
#endif
#endif // ENTITY_H
