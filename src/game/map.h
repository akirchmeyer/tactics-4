#ifndef MAP_H
#define MAP_H

#include "cd/hgrid.h"
#include "player.h"
#include "entity.h"
#include <unordered_map>
#include <gl/texture.h>

namespace sim{
class map : public hgrid
{
public:
    map(const dnx::aabb &a);
    virtual ~map(){
        clear();
        m_cells.clear();
    }

    virtual void add(std::unique_ptr<entity> b);
    virtual void rec_attach(entity *b);
    virtual void rec_detach(entity *b);

    virtual void clear();

    virtual void update(sim::pcont &pairs, const float dt);

    void resetMovementListen();

    void Init() const;

    void Draw() const;

protected:
    virtual void broadphase(sim::pcont &pairs);
};
}

#endif // MAP_H
