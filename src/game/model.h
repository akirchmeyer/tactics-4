#ifndef MODEL_H
#define MODEL_H

#include <dnx/body.h>
#include <gl/texture.h>
#include <dnx/mesh.h>
#include "entity.h"

#include "common.h"

///GMODEL

class mesh : public dnx::data{
public:
    mesh() : data(), t(){ }
    mesh(std::string filename) : data(), t(){
        load_cmf(filename.c_str());
    }
    virtual ~mesh(){ free(); }

    virtual void free();
    void load(std::vector<ml::vec2f> _v, std::vector<uint16> _i, std::vector<ml::vec2f> _t);
    void alloc(uint32 nv, uint32 ni, uint32 nt){
        data::alloc(nv, ni);
        t.clear(); t.resize(nt);
    }

    void load_cmf(const char *filename){
        FILE *stream;
        unsigned int n,nv, ni, nt;
        stream = fopen(filename, "r");

        assert(stream);

        fscanf(stream, "%u %u %u", &nv, &ni, &nt);
        alloc(nv, ni, nt);

        for(n = 0;n < nv;++n) fscanf(stream, "%f %f", &m_verts[n].v.x,&m_verts[n].v.y);
        for(n = 0;n < ni;n+=3) fscanf(stream, "%hu %hu %hu",&i[n],  &i[n+1], &i[n+2]);
        for(n = 0;n < nt; ++n) fscanf(stream, "%f %f", &t[n].x,&t[n].y);

        fclose(stream);

        init();
    }

    void draw();

    virtual void init();

    _inline const ml::vec2f &tex(uint16 const j){ return t[j]; }
    _inline std::vector<ml::vec2f> &texs(){ return t; }

protected:
    std::vector<ml::vec2f> t;
    GLuint id;
};

class entity;


class Model : public del{
public:
    Model(uptr<Texture> t, uptr<mesh> m) : m_tex(std::move(t)), m_mesh(std::move(m)){
        assert(m_tex);
        assert(m_mesh);
    }
    virtual ~Model() noexcept{
        io::get("this")->debug("deleted Model\n");
    }

    void render(const entity *e) const;
    void bind() const;
    void unbind() const;

    _inline const Texture*       getTexture()const{ return m_tex.get(); }
    _inline const dnx::data*     getData()   const{ return m_mesh.get(); }

    _inline dnx::data *  acqData()   { return m_mesh.get(); }

    static uptr<Model> create(std::string t, std::string m){
        return make_uptr<Model>(Texture::create(t), make_uptr<mesh>(m));
    }

    static uptr<Model> load_cf(const char *filename){
        FILE *stream;
        uptr<mesh> m = make_uptr<mesh>();

        stream = fopen(filename, "r");
        assert(stream);

        char tex_name[1024];
        fscanf(stream, "%s", tex_name);

        unsigned int n,nv, ni, nt;
        fscanf(stream, "%u %u %u", &nv, &ni, &nt);

        std::vector<ml::vec2f>	v, t;
        std::vector<uint16> i;
        v.resize(nv); i.resize(ni); t.resize(nt);

        for(n = 0;n < nv;++n) fscanf(stream, "%f %f", &v[n].x,&v[n].y);
        for(n = 0;n < ni;n+=3) fscanf(stream, "%hu %hu %hu",&i[n],  &i[n+1], &i[n+2]);
        for(n = 0;n < nt; ++n) fscanf(stream, "%f %f", &t[n].x,&t[n].y);

        fclose(stream);

        m->load(v, i, t);
        m->init();

        return make_uptr<Model>(Texture::create(tex_name), std::move(m));
    }


protected:
    uptr<Texture> m_tex;
    uptr<mesh> m_mesh;
};

class entity;
class player;
class factory;

class e_factory : public ebase{
public:
    e_factory(GameInstance *w, uptr<dnx::props> props, uptr<dnx::shape> s, uptr<mind> _mind, uptr<hull> _hull, uptr<engine> _engine, uptr<vision> _vision)
        : ebase(std::move(_mind), std::move(_hull), std::move(_engine), std::move(_vision)), m_props(std::move(props)), m_shape(std::move(s)), m_world(w) {
        assert(m_world);
        assert(m_props);
        assert(m_shape);

        m_shape->resetTransform();
    }

    virtual ~e_factory() noexcept{
        io::get("this")->debug("deleted e_factory\n");
    }

    entity* construct(const dnx::transform &_t, const player *p, const factory *pt) const;

    _inline const dnx::shape *getShape() const{
        assert(m_shape);
        return m_shape.get();
    }

    _inline const dnx::props *getProps() const{
        assert(m_props);
        return m_props.get();
    }

protected:
    std::unique_ptr<dnx::props> m_props;
    std::unique_ptr<dnx::shape> m_shape;
    GameInstance *m_world;
};

class factory : public del
{
public:
    factory(GameInstance *w, const std::string &id, uptr<Model> _gm, uptr<mind> _mind, uptr<hull> _hull, uptr<engine> _engine, uptr<vision> _vision, std::vector<uptr<weapon>> &&_w, std::unique_ptr<dnx::props> p);
    virtual ~factory() noexcept;

    void create(const ml::vec2f &pos, const player *p) const;
    void create(const dnx::transform &t, const player *p) const;

    _inline const dnx::props* getProps()  const         { return m_emod->getProps();   }
    _inline const Model* getModel()  const           { return m_gmod.get();      }
    _inline const dnx::shape* getShape()  const      { return m_emod->getShape();   }
    _inline const std::string& getID() const          { return m_id; }

private:
    uptr<Model> m_gmod;
    std::unique_ptr<e_factory>  m_emod;

    std::string  m_id;
};

#endif // MODEL_H
