#include <ul/io.h>
#include <fstream>
#include <dnx/world.h>
#include <ctime>
#include <stdexcept>
#include <tbb/parallel_for_each.h>
#include <tbb/parallel_for.h>

#include "window.h"
#include "game_inst.h"

static window *s_win(nullptr);
namespace sim{
const GameInstance &getGameInstance(){
    return s_win->getGameInstance();
}

GameInstance &acqGameInstance(){
    return s_win->acqGameInstance();
}

/*const GameInstance *getWorld(){
    return getGameInstance();
}
GameInstance *acqWorld(){
    return acqGameInstance();
}*/

#if 0

const entity *getEntity(const uint32 id){
    return sim::getMap()->getEntity(id);
}
entity *acqEntity(const uint32 id){
    return sim::acqMap()->acqEntity(id);
}
#endif
const sim::map *getMap(){
    return getGameInstance().getMap();
}
sim::map *acqMap(){
    return acqGameInstance().acqMap();
}

}

_inline void glew_init(){
    io::get("this")->debug("Initializing GLEW\n");
    //dbg::push("glew init", _FILE, _LINE, _FUNC, code::prog, 0);
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if(err != GLEW_OK){
        io::get("this")->critical() << "GLEW Error: " << glewGetErrorString(err) << '\n';
    }
    assert(err == GLEW_OK);
    //dbg::pop();
}

_inline void glfw_init(GLFWerrorfun Error){
    io::get("this")->debug("Initializing GLFW\n");
    //dbg::push("glfw init", _FILE, _LINE, _FUNC, code::prog, 0);
    assert(glfwInit() == GL_TRUE);
    glfwSetErrorCallback(Error);
    //dbg::pop();
}

_inline void Error(int error, const char* description){
    io::get("this")->critical() << "GLFW Error " << error << ": " << description << '\n';
     //dbg::log(description, _FILE, _LINE, _FUNC, code::fat, error);
}

void new_error(){
    io::get("this")->critical() << "Error: Failed to allocate memory\n";
}

int main(){
    std::set_terminate(__gnu_cxx::__verbose_terminate_handler);
    std::set_terminate(std::abort);
    std::set_new_handler(new_error);

    try{
        srand(time(0));

        /*log::add_file_log
        (
            log::keywords::file_name = "log.txt",
            log::keywords::format = "[%TimeStamp%]: %Message%"
        );*/

        size_t q_size = 1048576; //queue size must be power of 2
        io::set_async_mode(q_size);
        io::set_pattern("[%Y:%m:%d] [%H:%M:%S]: %v");
        io::set_level(io::level::debug);

        std::ofstream stream("log.txt");
        stream.close();
        auto logger = io::create<io::sinks::stdout_sink_mt>("this"/*, "log.txt", true*/);
        auto out = io::create<io::sinks::stdout_sink_mt>("out");

        io::get("out")->info() << "Tactics r3\n";
        io::get("this")->info() << LOC << "{Initialization}\n";
        //log::core::get()->set_filter ( log::trivial::severity >= log::trivial::info );
        //dbg::open("log.txt", code::deb);

        //dbg::push("Phase I: Init", _FILE, _LINE, _FUNC, code::gen, 0);
        glfw_init(Error);
        ml::vec2f s = ml::vec2f(1024.f);
        std::unique_ptr<window> win = make_uptr<window>(glfwCreateWindow(s.x, s.y, "Tactics", nullptr, nullptr), s);

        s_win = win.get();

        win->init();
        glew_init();
        gl::Init();
        //dbg::pop();

        //dbg::left();
        //dbg::push("Phase II: Run", _FILE, _LINE, _FUNC, code::gen, 0);
        io::get("this")->info("{Execution}\n");
        win->run();
        io::get("out")->info("Releasing ressources\n");

        //dbg::pop();
    }
    catch (const io::spdlog_ex& ex){
        std::cout << "Error: log: " << ex.what() << std::endl;
    }
    catch(std::exception const &e){
        io::get("this")->critical() << "Exception catched:\n" << e.what() << '\n';
    }
    s_win = nullptr;
    io::get("this")->info("{Exiting}\n");
    io::drop_all();

    return 0;
}
