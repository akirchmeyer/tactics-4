#include <gl/draw.h>
#include <dnx/world.h>
#include <cstdlib>
#include "game_inst.h"
#include "filters.h"
#include <el/el.h>

#if 0
void collision2Ds(const float m1, const float m2, const float R, const ml::vec2f &p0, const ml::vec2f &p1, ml::vec2f &v0, ml::vec2f &v1)     {

    float  m21,dvx2,a;
    ml::vec2f p2, v2, vcm;

    m21= (m1 != 0.f) ? m2/m1 : m2;

    p2=p1-p0;
    v2=v1-v0;

    vcm = (m1*v0+m2*v1)/(m1+m2);

    //     *** return old velocities if balls are not approaching ***
    //if ( ml::dot(v2,p2) >= 0.f) return;

    //     *** I have inserted the following statements to avoid a zero divide;
    //         (for single precision calculations,
    //          1.0E-12 should be replaced by a larger value). **************

    float fy21=1e-12f*ml::abs(p2.y);
    if ( ml::abs(p2.x)<fy21) {
        p2.x=fy21*ml::sgn(p2.x);
    }

    //     ***  update velocities ***
    a=p2.y/p2.x;
    dvx2= (-2.f)*(v2.x+a*v2.y)/((1.f+a*a)*(1.f+m21));

    v0 -= ml::vec2f(1.f, a)*(m21*dvx2);
    v1 += ml::vec2f(1.f, a)*dvx2;

    //     ***  velocity correction for inelastic collisions ***
    v0=(v0-vcm)*R + vcm;
    v1=(v1-vcm)*R + vcm;
}

void ResolveCollision( entity * A, entity *B, const ml::vec2f &normal, const float pend)
{
  // Calculate relative velocity
  ml::vec2f rv = B->mom() - A->mom();

  // Calculate relative velocity in terms of the normal direction
  // Do not resolve if velocities are separating
  float velAlongNormal = ml::dot( rv, normal );
  if( velAlongNormal > 0.f) return;


  // Calculate restitution
  float e = ml::min( A->props()->bounce(), B->props()->bounce());

  // Calculate impulse scalar
  float j = -(1 + e) * velAlongNormal;
  j /= (1.f / A->mass()) + (1.f / B->mass());

  // Apply impulse
  ml::vec2f impulse = j * normal;
  A->pos() -= (1.f / A->mass()) * impulse;
  B->pos() += (1.f / B->mass()) * impulse;

  A->updateMom(); B->updateMom();
}
#endif


void solve(const sim::bpair &p, const float dt, const float pend, const ml::vec2f &con){
#if 1
    ml::vec2f f = ml::normalize(p.first->getPos()-p.second->getPos());
    float j;

    if(FilterStatic(p.first)){
        j = ml::max( - ( 1.f+p.first->getProps()->bounce() ) * ml::dot(p.first->getMomentum(), f), 0.f);
        if(j > 0.f){
            entity *b = const_cast<entity*>(p.first);
            b->setPos(b->getPos() + f*(j/p.first->getMass()));
        }
    }

    f = -f;

    if(FilterStatic(p.second)){
        j = ml::max( - ( 1.f+p.second->getProps()->bounce() ) * ml::dot(p.second->getMomentum(), f), 0.f);
        if(j > 0.f){
            entity *b = const_cast<entity*>(p.second);
            b->setPos(b->getPos() + f*(j/p.second->getMass()));
        }
    }
#endif

#if 0
    ResolveCollision(p.first, p.second, con*ml::sgn(ml::dot(p.second->getPos(), p.first->getPos())), pend);
#endif
#if 0
    ml::vec2f v0 = p.first->mom(), v1 = p.second->mom();
    collision2Ds(p.first->mass(), p.second->mass(), 1.f, p.first->getPos(), p.second->getPos(), v0, v1);
    if(p.first->mass() != 0.f) p.first->pos() += v0-p.first->mom();
    if(p.second->mass() != 0.f) p.second->pos() += v1-p.second->mom();
#endif
#if 0
    //ml::vec2f d = ml::normalize(p.first->getPos() - p.second->getPos());
    p.first->pos()  -= ml::normalize(ml::perp(con))*pend*0.5f;
    p.second->pos() -= ml::normalize(ml::perp(con))*pend*0.5f;
    /*gl::Color(ml::vec3f(1.f, 1.f, 1.f));
    gl::DrawQuad(dnx::aabb(p.first->getPos()+con, ml::vec2f(0.01f)));*/
#endif
}

GameInstance::GameInstance(user *u, const ml::vec2f &size)
    : m_broad(new sim::map(dnx::aabb(ml::vec2f(0.f), ml::vec2f(256.f))))/*, m_narrow(new dnx::naiven(solve))*/, m_fact(), m_players(), m_user(u), m_size(size), m_dt(ml::now())
{
    assert(m_broad);
    //assert(m_narrow);
}

GameInstance::~GameInstance(){
    clear();
    m_user->clear();

    gc::releaseAll();

    m_players.clear();
    m_fact.clear();
}


void GameInstance::draw(){
    m_user->bind();

    /*gl::Color(ml::vec3f(0.f,1.f,0.f));
    gl::DrawQuad(-m_size*0.5f, m_size*0.5f);
    gl::Color(ml::vec3f(1.f));*/

    gl::Blend(true);
    update();
    gl::Blend(false);
    getMap()->Draw();

    m_user->draw();
}

void GameInstance::update(){
    ///PLAYERS
    std::for_each(m_players.begin(), m_players.end(), [](uptr<player> &p){
        p->update();
    });

    ///WORLD

    physics_update(/*ml::dur<float, ml::tsec>(ml::now(), m_dt)*/0.016f);

    //io::get("out")->info() << "\r" << ml::dur<float, ml::tms>(ml::now(), m_dt) << "ms";
}

void GameInstance::init(){
    io::get("this")->info() << "Initializing ressources\n";

    std::string dir = "./data/";

    set_pt(make_uvec<factory>(
        make_uptr<factory>(this, "tree",Model::create(dir+"tree.png", dir+"tree.txt"),
                           nullptr, nullptr, nullptr, nullptr, std::vector<uptr<weapon>>(),
                           make_uptr<dnx::props>(0.f, 0.5f, 0.5f)),

        make_uptr<factory>(this, "soldier", Model::create(dir+"turret.png",dir+"soldier2.txt"),
                           make_uptr<mind>(),
                           make_uptr<hull>(health(3.f, 0.2f, 1.f), health(7.5f, 0.75f, 1.f)),
                           make_uptr<engine>(loader2(5.f,10.f)),
                           make_uptr<vision>(5.f),
                           make_uvec<weapon>(make_uptr<weapon>(apoint(1.f), std::chrono::milliseconds(750), weapon::NORMAL)),
                           make_uptr<dnx::props>(50.f, 1.f, 0.5f)),

        make_uptr<factory>(this, "tank", Model::create(dir+"tank2.png",dir+"tank.txt"),
                           make_uptr<mind>(),
                           make_uptr<hull>(health(50.f, 0.5f, 1.f), health(30.f, 3.f, 1.f)),
                           make_uptr<engine>(loader2(2.f,6.f)),
                           make_uptr<vision>(5.f),
                           make_uvec<weapon>(make_uptr<weapon>(apoint(5.f), std::chrono::milliseconds(6000), weapon::SPLASH)),
                           make_uptr<dnx::props>(100.f, 1.f, 0.5f)),

        make_uptr<factory>(this, "base", Model::create(dir+"base.png",dir+"base.txt"),
                           make_uptr<mind>(),
                           make_uptr<hull>(health(150.f, 1.f, 1.f), health(100.f, 5.f, 1.f)),
                           uptr<engine>(nullptr),
                           make_uptr<vision>(5.f),
                           make_uvec<weapon>(make_uptr<weapon>(apoint(0.75f), std::chrono::milliseconds(500)),
                                             make_uptr<weapon>(apoint(0.75f), std::chrono::milliseconds(500)),
                                             make_uptr<weapon>(apoint(0.75f), std::chrono::milliseconds(500)),
                                             make_uptr<weapon>(apoint(0.75f), std::chrono::milliseconds(500)),
                                             make_uptr<weapon>(apoint(0.75f), std::chrono::milliseconds(500))),
                           make_uptr<dnx::props>(0.f, 1.f, 0.f))));
    //add_pt(new factory("wall",    new Model("wall", Texture::create(dir+"wall.png")), env,         new dnx::b_factory(w, new dnx::props(0.f, 0.5f, 0.f), new dnx::rect(ml::vec2f(1.f)))));

    add_player(new player(ml::vec3f(1.f,0.f,0.f)));
    add_player(new player(ml::vec3f(0.f,1.f,0.f)));
    add_player(new player(ml::vec3f(0.f,0.f,1.f)));
    add_player(new player(ml::vec3f(1.f,1.f,0.f)));
    add_player(new player(ml::vec3f(1.f,0.f,1.f)));

    acqMap()->Init();
}

_inline float randf(){
    return(float)(rand())/(float)(RAND_MAX);
}

player *cp = nullptr;

_inline void addTroopers(const ml::vec2f &v, const factory *fact, player *p){
    ml::vec2f _v = v;

    const dnx::aabb s = fact->getShape()->getAABB();
    const ml::vec2f e = s.he*2.f;

    const uint32 nx = (uint32)ml::floor(4.f/e.x), ny = (uint32)ml::floor(4.f/e.y);

    for(uint32 _x = 0; _x < nx; ++_x, _v.x += e.x){
        _v.y = v.y;
        for(uint32 _y = 0; _y < ny; ++_y, _v.y += e.y){
            fact->create(dnx::transform(_v, ml::rot()), p);
        }
    }
}

void GameInstance::handle(const Input *i){
    m_user->handle(i);

    if(i->active(Input::KEY_BUTTON)){
        if(i->keydown(GLFW_KEY_Q)){
            factory* fact = Get("tree");
            const uint32 ntree = rand()%100;
            const float w = m_size.x*0.5f, h = m_size.y*0.5f;
            for(uint32 _i = 0; _i < ntree; ++_i){
                fact->create(ml::vec2f(w-(randf()*2.f*w),h-(randf()*2.f*h)), nullptr);
            }
        }
        if(i->keydown(GLFW_KEY_W)){
            const ml::vec2f v = m_user->getCamera()->project(i->getMousePos());
            addTroopers(v, Get("soldier"), cp);
        }
        if(i->keydown(GLFW_KEY_E)){
            const ml::vec2f v = m_user->getCamera()->project(i->getMousePos());
            Get("tank")->create(v, cp);//addTroopers(v, factory::Get("tank"), cp, m_world.get());
        }
        if(i->keydown(GLFW_KEY_R)){
            const ml::vec2f v = m_user->getCamera()->project(i->getMousePos());
            Get("base")->create(v, cp);
        }/*
        else if(i->keydown(GLFW_KEY_T)){
            ml::vec2f v = m_user->cam()->project(i->getMousePos());
            factory::Get("wall")->create(m_world.get(), v, cp);
        }*/
        if(i->keyup(GLFW_KEY_Z)){
            GameInstance::clear();
            m_user->clear();
        }
        if(i->keyup(GLFW_KEY_A))
            cp = m_players[0].get();
        else if(i->keyup(GLFW_KEY_S))
            cp = m_players[1].get();
        else if(i->keyup(GLFW_KEY_D))
            cp = m_players[2].get();
        else if(i->keyup(GLFW_KEY_F))
            cp = m_players[3].get();
        else if(i->keyup(GLFW_KEY_G))
            cp = m_players[4].get();
        else if(i->keyup(GLFW_KEY_H))
            cp = nullptr;
    }
}
