#ifndef PLAYER_H
#define PLAYER_H

#include <gl/camera.h>
#include <ml/types.h>
#include "common.h"

class player
{
public:
    player(ml::vec3f const &col) noexcept : m_energy(0.f, 10.f), m_color(col){ }

    ~player() noexcept{ }

    void update(){
        m_energy.update();
    }

    const ml::vec3f& getColor() const{ return m_color; }

private:
    loader<float> m_energy;
    const ml::vec3f m_color;
};

#endif // PLAYER_H
