#include "user.h"
#include "player.h"
#include "filters.h"
#include <el/el.h>
#include <dnx/world.h>
#include "map.h"

user::user(camera *c) : m_cam(c), draw_box(false){
}

user::~user(){
    pck.clear();
}

void user::bind(){
    m_cam->ortho();
    m_cam->look();
}

const float m = 10.f;
bool mov = false;
ml::vec2f mvec(0.f);

void user::draw(){
    if(draw_box){
        gl::Color(ml::vec3f(1.f));
        gl::DrawAABB(m_cam->project(ml::min(m0, m1)), m_cam->project(ml::max(m0, m1)));

        /* QUAD BOX
         * ml::vec2f p0 = ml::vec2f(x, y), p1 = ml::max(p0, m0)-ml::min(p0,m0);
        gl::DrawAABB(cam->project(m0 - p1), cam->project(m0 + p1));*/
    }

    std::for_each(pck.begin(), pck.end(), [this](const entity *b){
        assert(b);
        gl::Color(ml::vec3f(1.f));
        /*entity *e = (entity*)((*i)->data());

        glPolygonMode(GL_FRONT, GL_LINE);
        e->gm()->render(*i);
        glPolygonMode(GL_FRONT, GL_FILL);*/
        gl::DrawAABB(b->getAABB());
    });
}

void user::clear(){
    pck.clear();
}


void user::handle(const Input *i){
    if(i->active(Input::WIN_RESIZE)){
        m_cam->resize(ml::cast<float>(i->getWinSize()));
    }
    if(i->active(Input::MOUSE_SCROLL)){
        m_cam->mulvs(-i->scroll.y);
    }

    if(i->active(Input::MOUSE_BUTTON)){
        if(i->bactive(GLFW_MOUSE_BUTTON_MIDDLE)){
            mov = i->mousedown(GLFW_MOUSE_BUTTON_MIDDLE);
            mvec = i->getMousePos();
        }

        if(i->mousedown(GLFW_MOUSE_BUTTON_LEFT)){
            m0 = i->getMousePos();
            m1 = m0;
            draw_box = true;
        }
        else if(i->mouseup(GLFW_MOUSE_BUTTON_LEFT)){
            /* QUAD BOX PICK
             * ml::vec2f p0 = ml::vec2f(x, y), p1 = ml::max(p0, m0) - ml::min(p0,m0);
            sim::Convex2D(m0 - p1,  m0 + p1, ml::vec2f(w, h), sim::FilterNone, sim::CastAdd, &sim::pck);*/
            m1 = i->getMousePos();

            /*sim::getMap()->CastAABB(dnx::aabb::fromMinMax(m_cam->project(ml::min(m0, m1)), m_cam->project(ml::max(m0, m1))), [&](const entity *b, const float *d){
                assert(b->isAlive());
                if(ext::add_if(pck, b)){
                   b->listenDestroy([this](const entity *_b){
                       ext::erase(pck, (entity*)_b);
                   });
               }
            }, FilterNeutral);*/

            draw_box = false;
        }
        if(i->mouseup(GLFW_MOUSE_BUTTON_RIGHT)){
            ml::vec2f v = m_cam->project(i->getMousePos());
            std::for_each(pck.begin(), pck.end(), [&v](entity *e){
                if(FilterStatic(e)){
                    e->moveto(v);
                }
            });
        }
    }
    if(i->active(Input::MOUSE_MOVE)){
        if(draw_box){
            m1 = i->getMousePos();
        }
        if(mov){
            m_cam->move((mvec-i->getMousePos())*1.5f);
            mvec = i->getMousePos();
        }
    }
    if(i->active(Input::KEY_BUTTON)){
        if(i->keydown(GLFW_KEY_UP)){
            m_cam->move(ml::vec2f(0.f,-m));
        }
        if(i->keydown(GLFW_KEY_DOWN)){
            m_cam->move(ml::vec2f(0.f,m));
        }
        if(i->keydown(GLFW_KEY_LEFT)){
            m_cam->move(ml::vec2f(-m,0.f));
        }
        if(i->keydown(GLFW_KEY_RIGHT)){
            m_cam->move(ml::vec2f(m,0.f));
        }
        if(i->keyup(GLFW_KEY_X)){
            pck.clear();
        }
        if(i->keyup(GLFW_KEY_T)){
            std::for_each(pck.begin(), pck.end(), [](entity *e){
                if(e) e->swapShield();
            });
        }
        if(i->keyup(GLFW_KEY_Y)){
            std::for_each(pck.begin(), pck.end(), [](entity *e){
                if(e) e->tscape();
            });
        }
    }
}

