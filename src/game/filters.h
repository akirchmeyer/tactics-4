#ifndef FILTERS
#define FILTERS

#include <dnx/body.h>
#include "player.h"
#include "entity.h"

_inline bool FilterNone(const entity *b){
    return (b->isAlive() && b->isActive());
}
_inline bool FilterStatic(const entity *b){
    return FilterNone(b) && (b->getMass() != 0.f);
}
_inline bool FilterMobile(const entity *b){
    return FilterNone(b) && (b->getMass() == 0.f);
}

_inline bool FilterMoving(const entity *b){
    return FilterNone(b) && (ml::lengthSq(b->getMomentum()) == 0.f);
}

_inline bool FilterNonMoving(const entity *b){
    return FilterNone(b) && (ml::lengthSq(b->getMomentum()) != 0.f);
}

_inline bool FilterNeutral(const entity *b){
    if(!FilterNone(b)) return false;
    return (b) ? b->hasPlayer() : false;
}

_inline bool FilterTeams(const entity *b){
    if(!FilterNone(b)) return false;
    return (b) ? !b->hasPlayer() : false;
}

#endif // FILTERS

