#ifndef COMPONENTS_H
#define COMPONENTS_H


#include <vector>
#include <ml/types.h>
#include <dnx/body.h>
#include <ml/timer.h>
#include "common.h"
#include <mutex>
#include <thread>
#include <condition_variable>
#include <unordered_map>
#include <tbb/concurrent_hash_map.h>

typedef std::vector<ml::vec2f> v2c;

class entity;

class mind;
class hull;
class engine;
class weapon;


class event{
public:
    event() noexcept : lock(), check(), notif(false), m_des(false){}
    ~event() noexcept { notif = false; m_des = true; }

    DEFINE_COPY2(event, delete)
    DEFINE_MOVE2(event, default)

    void notify(){
        std::unique_lock<std::mutex> mutex(lock);
        notif = true;
        check.notify_all();
    }
    void denotify(){
        notif = false;
    }

    bool isNotified() const { return notif; }
    bool isDestroyed() const{ return m_des; }

    void destroy() noexcept{
        std::unique_lock<std::mutex> mutex(lock);
        m_des = true;
        check.notify_all();
    }


    void wait(){
        std::unique_lock<std::mutex> mutex(lock);
        check.wait(mutex, [this]() -> bool { return m_des || notif; });
    }
    void wait_for(const ml::tdur &d){
        std::unique_lock<std::mutex> mutex(lock);
        check.wait_for(mutex,d, [this]() -> bool { return m_des; });
    }

private:
    std::mutex              lock;
    std::condition_variable check;
    bool                    notif, m_des;
};

class vision{
public:
    vision(const float r) noexcept : m_ent(nullptr), m_ents(), m_range(r), m_atk(){}
    ~vision() noexcept = default;

    DEFINE_COPY2(vision, delete)
    DEFINE_MOVE2(vision, default)

    void destroy() noexcept{
        m_atk.destroy();
    }

    void update();
    void setEntity(entity *e);

    uptr<vision> construct() const noexcept{
        return make_uptr<vision>(m_range);
    }

    void addTracked(const entity *b);
    auto &getTracked() { return m_ents; }

    /*_inline auto begin() const { return m_ents.begin(); }
    _inline auto end()   const { return m_ents.end();   }*/

    _inline event& evt(){ return m_atk; }

private:
    entity *m_ent;
    std::vector<entity*> m_ents;
    float m_range;
    event m_atk;
};

class weapon
{
public:
    constexpr weapon(const apoint &a, const std::chrono::milliseconds rate, const uint8 caps = weapon::NORMAL)
        : m_ent(nullptr), m_dmg(a), m_rate(rate), m_caps(caps){}
    ~weapon() noexcept {}

    DEFINE_COPY(weapon, delete)
    DEFINE_MOVE(weapon, default)

    void setEntity(entity *e){
        assert(e);
        m_ent = e;
    }

    void start();

    void operator>>(hull &h);

    _inline bool check(uint8 state) const{
        return (m_caps&state);
    }

    _inline uptr<weapon> construct() const noexcept{
        return make_uptr<weapon>(m_dmg, m_rate, m_caps);
    }

    entity *m_ent;

    apoint m_dmg;
    std::chrono::milliseconds m_rate;

    uint8 m_caps;

    enum{
        NORMAL = 0,
        SPLASH = 0x01
    };
};

class hull{
public:
    hull(const health &life, health const &shield) noexcept: m_ent(nullptr), m_life(life), m_shield(shield), s_act(false){
        m_shield.setValue(0.f);
        m_tp = ml::now();
    }    
    ~hull() noexcept = default;

    DEFINE_COPY2(hull, delete)
    DEFINE_MOVE2(hull, default)


    void setEntity(entity *e);

    void regen();

    uptr<hull> construct() const noexcept{
        return make_uptr<hull>(m_life.construct(), m_shield.construct());
    }

    void destroy() noexcept{
        m_life.destroy();
        s_act = false;
    }

    void s_activate();
    void s_deactivate();

    bool isAlive() const{
        return (m_life.isAlive());
    }

    const health &getLife() const{
        return m_life;
    }
    const health &getShield() const{
        return m_shield;
    }

    const bool &isShieldActive() const{
        return s_act;
    }

    void update();

    void operator-=(const apoint &b);

    const entity *getEntity(){
        assert(m_ent);
        return m_ent;
    }

private:
    entity *m_ent;
    health m_life, m_shield;
    bool s_act;
    ml::tpoint m_tp;
};

class mind{
public:
    constexpr mind() noexcept : m_ent(nullptr){
    }
    ~mind() noexcept = default;
    DEFINE_COPY(mind, delete)
    DEFINE_MOVE(mind, default)

    uptr<mind> construct() const noexcept{
        return make_uptr<mind>();
    }

    _inline void setEntity(entity *e){
        assert(e);
        m_ent = e;
    }

    void update() const{
        assert(m_ent);
    }

private:
    entity *m_ent;
};

class engine{
public:
    engine(loader2 &&fuel) noexcept : m_force(0.f), m_body(nullptr), m_path(), m_fuel(std::move(fuel)){}
    ~engine() noexcept{
        m_path.clear();
        //io::log("engine\n");
    }
    DEFINE_COPY2(engine, delete)
    DEFINE_MOVE2(engine, default)

    uptr<engine> construct() const noexcept{
        return make_uptr<engine>(m_fuel.construct());
    }

    _inline void setBody(entity *b){
        assert(b);
        m_body = b;
    }

    _inline const ml::vec2f& getCurpath() const{
        return m_path.front();
    }

    void update();
    void destroy() noexcept{
        m_path.clear(); m_force = ml::vec2f(0.f); m_fuel.destroy();
    }

    bool isMoving() const{
        return (!m_path.empty() && m_fuel.isRunning());
    }
    void add(const ml::vec2f &p){
        m_path.push_back(p);
    }
    void reset(){
        m_path.clear();
        m_force = ml::vec2f(0.f);
    }

    void boost(){
        if(!m_fuel.isBoosted()) m_fuel.boost();
        else m_fuel.unboost();
    }

    void operator+=(const ml::vec2f &v);
    ml::vec2f move();

    const ml::vec2f &getForce() const noexcept{
        return m_force;
    }
    const loader2& getFuel() const noexcept{
        return m_fuel;
    }

private:
    ml::vec2f m_force;
    entity *m_body;
    v2c m_path;
    loader2 m_fuel;
};


#endif // COMPONENTS_H
