#ifndef COMMON
#define COMMON

#include <ml/timer.h>
#include <ml/types.h>
#include <ul/common.h>
#include <bitset>

class GameInstance;
namespace dnx{
class world;
}

namespace sim{
    class map;

    /*const GameInstance *getWorld();
    GameInstance *acqWorld();*/

    const GameInstance &getGameInstance();
    GameInstance &acqGameInstance();

    const sim::map *getMap();    
    sim::map *acqMap();

    constexpr ml::vec2f seek(const ml::vec2f &target, const ml::vec2f &pos){
        return target - pos;
    }
}

class apoint{
public:
    constexpr apoint(float const _v, float const _i = 1.f) noexcept : v(_v), i(_i){}
    ~apoint() noexcept = default;

    DEFINE_COPY(apoint, default)

    constexpr bool isAlive() const{ return (v > 0.f); }
    constexpr float getValue() const { return v; }
    constexpr float getInten() const { return i; }
    constexpr float getBrute() const { return v*i; }

    void setValue(const float b) { v = b; }
    void setInten(const float b) { i = b; }

    apoint& operator-=(apoint& b){
        float d = getBrute()-b.getBrute();
        setValue(ml::max(d, 0.f)/i);
        b.setValue(ml::max(-d, 0.f)/b.i);

        return *this;
    }
    apoint& operator-=(apoint const& b){
        float d = getBrute()-b.getBrute();
        setValue(ml::max(d, 0.f)/i);

        return *this;
    }

private:
    float v, i;
};

constexpr apoint operator*(apoint const &a, float const& b){
    return apoint(a.getValue()*b, a.getInten());
}
/*constexpr _inline float operator-(apoint const &a, apoint const& b){
    return a.getBrute()-b.getBrute();
}*/

class reload{
public:
    reload(std::chrono::milliseconds const &r) noexcept : m_rate(r), m_tp(ml::now()) {}
    ~reload() noexcept = default;

    DEFINE_COPY(reload, delete)
    DEFINE_MOVE(reload, default)

    bool check() const { return ((ml::now()-m_tp) >= m_rate); }
    void update()       { m_tp = ml::now(); }

private:
    const std::chrono::milliseconds m_rate;
    ml::tpoint m_tp;
};

template<typename T> class loader{
public:
    constexpr loader(const T &v,const T &r) noexcept : m_tp(ml::now()), m_rate(r), m_value(v){ }
    ~loader() noexcept = default;

    DEFINE_COPY(loader, delete)
    DEFINE_MOVE(loader, default)

    void update(){
        m_value += ml::dur<T, ml::tsec>(ml::now(), m_tp)*m_rate;
        m_tp = ml::now();
    }

private:
    ml::tpoint m_tp;
    const T m_rate;
    T m_value;
};

class loader2{
public:
    loader2(float const _r, float const _m) noexcept : m_tp(ml::now()), m(_m), v(0.f), r(_r), i(1.f){ }
    ~loader2() noexcept = default;

    DEFINE_COPY(loader2, delete)
    DEFINE_MOVE(loader2, default)

    void destroy() { v = 0.f; i = 1.f; }

    loader2 construct() const noexcept{
        return loader2(r, m);
    }

    void regen(){
        v = ml::min(v+getdt()*r, m);
    }
    void update(){
        m_tp = ml::now();
        if(!isRunning()) unboost();
    }

    void setValue(const float _v) { v = _v; }

    void boost()   { i += 0.5f; }
    void unboost() { i = 1.f; }

    bool isRunning()  const { return (v > 0.f); }
    bool isBoosted()  const { return (i > 1.f); }

    float getValue() const  {return v; }
    float getInten() const  { return i; }
    float getValueScaled() const { return v/m;}
    float getRate()   const  { return r*i; }
    float getdt()     const  { return ml::dur<float, ml::tsec>(ml::now(), m_tp); }

private:
    ml::tpoint m_tp;
    float m, v, r, i;
};

class health{
public:
    /*health(float v, float regen, float armor, float max) : m_value(v), m_regen(regen), m_armor(armor), m_max(max){
        m_tp = ml::now();
    }*/
    health(float const max, float const r, float const armor) noexcept : m_value(max, armor), m_regen(r), m_max(max), m_tp(ml::now()){}

    health construct() const noexcept{
        return health(m_max, m_regen, m_value.getInten());
    }
    void destroy() noexcept{
        m_value.setValue(0.f);
    }
    void regen(){
        m_value.setValue(ml::min(m_value.getValue()+m_regen*ml::dur<float, ml::tsec>(ml::now(), m_tp), m_max));
        m_tp = ml::now();
    }

    void setValue(float const v){ m_value.setValue(v); }

    bool isAlive() const{ return m_value.isAlive(); }
    float getValue() const{ return m_value.getValue(); }
    float getRegen() const{ return m_regen; }
    float getValueScaled() const{ return getValue()/m_max; }

    health& operator-=(apoint& b){
        if(m_value.isAlive() && b.isAlive())
            m_value -= b;
        return *this;
    }
    health& operator-=(apoint const& b){
        if(m_value.isAlive() && b.isAlive())
            m_value -= b;
        return *this;
    }

private:
    apoint m_value;
    const float m_regen, m_max;
    ml::tpoint m_tp;
};

#endif // COMMON

