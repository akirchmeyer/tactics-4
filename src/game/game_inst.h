#ifndef INTERFACE_H
#define INTERFACE_H

#include <vector>



#include "common.h"
#include "player.h"
#include "user.h"
#include "model.h"
#include "map.h"

#include <gl/input.h>
#include <dnx/world.h>

class GameInstance
{
public:
    GameInstance(user *u, const ml::vec2f &size);
    virtual ~GameInstance();

    void add_pt(factory* p){      m_fact.push_back(uptr<factory>(p)); }
    void add_player(player *p){     m_players.push_back(uptr<player>(p)); }

    void set_players(std::vector<uptr<player>> &&p){
        m_players.clear();
        m_players = std::move(p);
    }
    void set_pt(std::vector<std::unique_ptr<factory> > &&p){
        m_fact.clear();
        m_fact = std::move(p);
    }

    void init();
    void draw();
    void update();
    void handle(const Input *i);


    user *acqUser(){
        return m_user.get();
    }
    const user *getUser() const{
        return m_user.get();
    }

    const sim::map *getMap() const{
        return static_cast<const sim::map*>(m_broad.get());
    }
    sim::map *acqMap(){
        return static_cast<sim::map*>(m_broad.get());
    }


    factory* Get(const std::string &id){
        for(auto &b : m_fact){
            if(id == b->getID()) return b.get();
        }
        io::get("this")->critical("Error: out of bound: id !c factories\n");
        assert(false);
        return nullptr;    clear();
    }

    void physics_update(const float dt){
        assert(m_broad);
        //assert(m_narrow);

        sim::pcont pairs;
        m_broad->update(pairs, dt);
        //m_narrow->narrowphase(dt, pairs);
    }

    void clear(){
        assert(m_broad);
        m_broad->clear();
    }

    entity* add(uptr<entity> e){
        m_broad->add(std::move(e));
        return m_broad->m_bodies.back().get();
    }


protected:
    uptr<sim::map> m_broad;
    //uptr<dnx::narrow> m_narrow;

    std::vector<uptr<factory>> m_fact;
    std::vector<uptr<player>> m_players;

    uptr<user> m_user;


    ml::vec2f m_size;
    ml::tpoint m_dt;
};

#endif // INTERFACE_H
