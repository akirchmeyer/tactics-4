#include "window.h"
#include <ml/timer.h>

#include <gl/input.h>
#include <gl/camera.h>
#include "game_inst.h"
#include "user.h"

window::window(GLFWwindow *w, const ml::vec2f &s) : m_win(w), m_inp(new Input()), m_width(0), m_height(0),
                                m_int(new GameInstance(new user(new camera(ml::vec2f(0.f), s, 1.f)), s))
{
    assert(m_inp);
    assert(m_int);
    assert(m_win);

    glfwGetWindowSize(w, &m_width, &m_height);
    assert(m_width > 0);
    assert(m_height > 0);

    io::get("this")->info("Constructed window");//dbg::log("window created", _FILE, _LINE, _FUNC, code::ver, 0);
}

window::~window(){
    io::get("this")->info("{Termination}\n");
    io::get("this")->debug("deleted window\n");
}
void window::draw() const{
    assert(m_int);
    m_int->draw();
}

void window::update(){
    assert(m_int);
    m_int->update();
}


void window::run(){
    m_int->init();

    io::get("this")->info() << "Starting execution\n";

    while(!glfwWindowShouldClose(m_win.get())){
        assert(m_win);
        glfwPollEvents();
        handle();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //update();
        draw();
        glfwSwapBuffers(m_win.get());
    }
}

void window::handle(){
    assert(m_inp);
    if(m_inp->active(Input::KEY_BUTTON)){
        if(m_inp->keydown(GLFW_KEY_ESCAPE))
            glfwSetWindowShouldClose(m_win.get(), true);
    }
    else if(m_inp->active(Input::WIN_RESIZE)){
        m_width = m_inp->size.x; m_height = m_inp->size.y;
        //dbg::log("win resized", _FILE, _LINE, _FUNC, code::ver, 0);
    }

    m_int->handle(m_inp.get());

    m_inp->reset();
}

void window::init(){
    //dbg::push(ml::exception("win init", _FILE, _LINE, _FUNC, code::prog, 0));

    GLFWwindow *win = m_win.get();

    assert(win);
    assert(m_inp);

    glfwMakeContextCurrent(win);
    glfwSetFramebufferSizeCallback(win, ResizeFB);

    m_inp->bind(win);

    //dbg::log("win callbacks set", _FILE, _LINE, _FUNC, code::ver, 0);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);

    glfwSwapInterval(1);

    io::get("this")->info("Window Initialized\n");
    //dbg::log("win hints set", _FILE, _LINE, _FUNC, code::ver, 0);
    //dbg::pop();
}


///CALLBACKS

void window::ResizeFB(GLFWwindow* window, const int width, const int height){
    assert(window);
    glViewport(0, 0, width, height);
    //dbg::log("framebuffer resized", _FILE, _LINE, _FUNC, code::ver, 0);
}
