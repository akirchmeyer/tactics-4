#include "map.h"
#include <tbb/parallel_for_each.h>
#include <tbb/blocked_range.h>
#include <el/el.h>
#include "model.h"

namespace sim{

uptr<Texture> t_shield(nullptr);

void map::Init() const{
    std::string dir = "./data/";
    t_shield = uptr<Texture>(Texture::create(dir+"shield.png"));
}
map::map(const dnx::aabb &a) : hgrid(a, std::vector<ml::vec2f>{ml::vec2f(2.f)}) {
}

void map::clear(){
    /*std::for_each(m_ent.begin(), m_ent.end(), [](auto &e){
        e.second->setDestroy();
    });*/
    hgrid::clear();

    //m_ent.clear();
}

void map::update(sim::pcont &pairs, const float dt){
    for(auto &a : m_cells) a->clear();

    entity *e = nullptr;

    ext::erase_if<uptr<entity>>(m_bodies, [this, &e](uptr<entity> &b) -> bool{
        assert(b);
        if(!b->isAlive()){
            return true;
        }

        e = b.get();
        ///PUT RENDER CODE IN CLIENT - use
        e->setRender(true);

        if(!b->isActive()) return false;

        ///compute velocities
        b->setPos(ml::clamp(b->getPos(), m_aabb.min(), m_aabb.max()));
        b->updateMov();
        b->update();
        rec_attach(b.get());

        return false;
    });

#if 0

    ///UPDATE ENTITIES
    std::for_each(m_ent.begin(), m_ent.end(), [&e](auto &b){
        assert(b.second);
        e = b.second.get();
        if(e->isAlive()) e->update();
    });
#endif

    ///broadphase
    broadphase(pairs);
}

void map::Draw() const{
    //dbg::log("map - draw", _FILE, _LINE, _FUNC, code::deb);

    for(auto &c:m_cells) c->Draw();
    const Model *last = nullptr;
    entity *ent;

    gl::Blend(true);

    for(auto &b : m_bodies){
        auto &a = b;
        assert(a);

        if(!a->getRender()) return;

        if(a->getModel() != last){
            a->getModel()->bind();
            last = a->getModel();
        }
        a->draw();
    }

    assert(t_shield);
    t_shield->bind();
    gl::Color(ml::vec4f(1.f));
    std::for_each(m_bodies.begin(), m_bodies.end(), [&ent](auto &b){
        auto &a = b;
        assert(a);
        if(a->getRender() && a->isShieldActive()) gl::DrawQuad(dnx::aabb(a->getPos(), a->getAABB().he*1.25f));
    });
    t_shield->unbind();

    gl::Color(ml::vec3f(1.f));

    std::for_each(m_bodies.begin(), m_bodies.end(), [&ent](auto &b){
        auto &a = b;
        assert(a);
        a->drawhud();
    });
    gl::Blend(false);

    gl::Color(ml::vec3f(1.f));
}

#if 0
void map::Add(const uint32 id, uptr<entity> e){
    assert(!exists(id));
    assert(e);

    e->listenDestroy([this](const entity *_b){
        if(exists(_b->getID())){
            Remove(_b->getID());
        }
    });

    /*e->listenMove([this](const entity *_b){
        rec_detach(_b);
        rec_attach(_b);
    });*/

    m_ent.emplace(std::make_pair(id, std::move(e)));
    assert(exists(id));
}

void map::Remove(const uint32 id){
    assert(exists(id));
    m_ent.erase(id);
    assert(!exists(id));
}
#endif

void map::broadphase(sim::pcont &pairs){
    hgrid::broadphase(pairs);
}

void map::add(std::unique_ptr<entity> b){
    hgrid::add(std::move(b));
}

void map::rec_attach(entity *b){
    hgrid::rec_attach(b);
}

void map::rec_detach(entity *b){
    hgrid::rec_detach(b);
}

void map::resetMovementListen(){
    assert(false); //needs fixing
    /*std::for_each(m_cells[0].begin(), m_cells[0].end(), [this](const dnx::cell *c){
        std::for_each(c->bbegin(), c->bend(), [&c, this](const entity *_a){
            entity *a = acqEntity(_a->getID());
            std::for_each(c->bbegin(), c->bend(), [&a, this](const entity *_b){
                entity *b = acqEntity(_b->getID());
                vision &v = *b->getVision();
                a->listenMove([this, &v](const entity *_d){
                    v.addTracked(acqEntity(_d->getID()));
                });
            });
        });
    });*/
}
}
