#include "entity.h"
#include <dnx/world.h>
#include "model.h"

const Model*     entity::getModel() const { assert(m_fact);return m_fact->getModel();  }

entity::entity(const GameInstance *w, const dnx::transform &_t, const dnx::props *props, dnx::shape *s, const factory *pt, const player *p, uptr<mind> _mind, uptr<hull> _hull, uptr<engine> _engine, uptr<vision> _vision) noexcept
    : ebase(std::move(_mind), std::move(_hull), std::move(_engine), std::move(_vision)),
      m_shape(s), t(_t), m_ppos(_t.p)/*, m_velo(0.f)*/, m_mom(0.f), m_acc(0.f), e_destroy(this), e_move(this), m_fact(pt), m_player(p), m_world(w), m_props(props), m_id(ml::gen_id()),m_alive(true), m_active(true){
    if(m_hull) m_hull->setEntity(this);
    if(m_engine) m_engine->setBody(this);
    if(m_mind) m_mind->setEntity(this);
    if(m_vision) m_vision->setEntity(this);
}

void entity::update(){
    if(!isAlive()) return;

    if(m_vision) m_vision->update();
    if(m_hull){
        m_hull->update();
    }
    if(m_mind) m_mind->update();

    if(m_engine) m_engine->update();

    if(!isActive()) return;
    updateMov();
    m_ppos = getPos();
}

void entity::tscape(){
    std::thread _t([this](){
        setActive(false);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        setActive(true);
    });
    _t.detach();
}

void entity::draw() const {
    assert(getModel());
    gl::Color((m_player) ? m_player->getColor() : ml::vec3f(1.f));
    getModel()->render(this);
}

void entity::drawhud() const {
    if(!m_render) return;

    const ml::vec2f p(0.05f, 0.f);
    const ml::vec2f s(0.2f, 0.05f);
    const ml::vec2f p0(getPos()+ml::vec2f(-s.x*2.f, s.y*0.5f)-p);
    const ml::vec2f p1(getPos()+ml::vec2f(-s.x*2.f,-s.y*0.5f)-p);
    const ml::vec2f p2(getPos()+ml::vec2f(-s.x*2.f,-s.y*1.5f)-p);

    if(m_hull){
        const health l = m_hull->getLife(), sh = m_hull->getShield();
        gl::Color(ml::vec3f(0.f, 0.f, 0.f));
        gl::DrawQuad(dnx::aabb::fromMinExt(p0, s));
        gl::DrawQuad(dnx::aabb::fromMinExt(p1, s));
        gl::Color(ml::vec3f(0.f, 1.f, 0.f));
        gl::DrawQuad(dnx::aabb::fromMinExt(p0, ml::vec2f(l.getValueScaled(), 1.f)*s));
        gl::Color(ml::vec3f(0.f, 0.f, 1.f));
        gl::DrawQuad(dnx::aabb::fromMinExt(p1, ml::vec2f(sh.getValueScaled(), 1.f)*s));
    }
    if(m_engine){
        const loader2 &e = m_engine->getFuel();
        gl::Color(ml::vec3f(0.f, 0.f, 0.f));
        gl::DrawQuad(dnx::aabb::fromMinExt(p2, s));
        gl::Color(ml::vec3f(0.5f));
        gl::DrawQuad(dnx::aabb::fromMinExt(p2, ml::vec2f(e.getValueScaled(), 1.f)*s));
        gl::Color((m_player) ? m_player->getColor() : ml::vec3f(1.f));
        gl::DrawQuad(dnx::aabb(getPos()+m_engine->getForce(), ml::vec2f(0.1f)));
    }

    gl::Color(ml::vec4f(1.f));
}

entity::~entity() noexcept{
    m_alive = false;
    m_active = false;
    e_destroy.dispatch();

    if(m_vision) m_vision->destroy();
    if(m_hull) m_hull->destroy();
    if(m_engine) m_engine->destroy();

    m_weapons.clear();
}
