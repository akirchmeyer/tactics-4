#ifndef USER_H
#define USER_H

#include <gl/camera.h>
#include <gl/input.h>

#include "common.h"
#include "entity.h"
#include "model.h"

class player;

class user
{
public:
    user(camera *c);
    ~user();

    void check(player *p);

    void bind();
    void draw();

    void handle(const Input *i);
    ml::vec2f project(const ml::vec2f &v) const{
        return m_cam->project(v);
    }

    const camera* getCamera() const{
        return m_cam.get();
    }

    void clear();

private:

    uptr<camera> m_cam;
    std::vector<entity*> pck;

    bool draw_box;
    ml::vec2f m0, m1;
};

#endif // USER_H
