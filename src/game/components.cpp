#include "components.h"
#include "entity.h"
#include "filters.h"
#include <ml/functions.h>
#include <future>
#include <dnx/dbg.h>
#include <dnx/world.h>
#include "map.h"

void vision::setEntity(entity *e) {
    m_ent = e;
    /*m_ent->getBody()->listenDestroy([this](const entity *b){
        m_atk.destroy();
        m_ents.clear();
    });*/
}

void vision::update(){
    assert(m_ent);

    if(!m_ent->isAlive()) destroy();

    dnx::aabb a(m_ent->getPos(), m_ent->getAABB().he+ml::vec2f(m_range));

    m_ents.clear();

    sim::getMap()->CastAABB(a, [this](const entity *b, const float *d){addTracked(b);}, [this](const entity *e){
        if(FilterNone(e)){
            if(e->isAlive()){
                if(e->hasPlayer()){
                    if(m_ent->getPlayer() != e->getPlayer()) return true;
                }
            }
        }
        return false;
    });
    if(!m_ents.empty()) m_atk.notify();
    else m_atk.denotify();
}

void vision::addTracked(const entity *b){
    m_ents.emplace_back((entity*)b);
    /*b->listenDestroy([this](const entity *b){
       auto i = m_ents.equal_range(b->getID());
       m_ents.unsafe_erase(i.first, i.second);
    });*/
}

void hull::update(){
    assert(m_ent);
    if(!isAlive()){ return; }
    regen();

    /*gl::Color(ml::vec3f(0.f, m_life.value()/m_life.m_max, 0.f));
    gl::DrawQuad(dnx::aabb(m_ent->body()->getPos()-ml::vec2f(0.1f), ml::vec2f(0.1f)));
    gl::Color(ml::vec3f(0.f, 0.f, m_shield.value()/m_shield.m_max));
    gl::DrawQuad(dnx::aabb(m_ent->body()->getPos()+ml::vec2f(0.1f), ml::vec2f(0.1f)));
    gl::Color(ml::vec4f(1.f));*/
}

ml::vec2f engine::move(){
    ml::vec2f v = sim::seek(m_path[0], m_body->getPos());

    if(ml::length(v) < m_fuel.getRate()){
        if((m_path.size() >= 2)){
            m_path.erase(m_path.begin());
            return move();
        }
    }
    return v;
}

void engine::update(){
    assert(m_body);

    m_fuel.regen();
    if(isMoving()){
        float g = m_fuel.getRate()*m_fuel.getdt();
        m_force = move();
        if(ml::lengthSq(m_force) > 0.f){
            float _v    = ml::min(m_fuel.getValue(), g);
            ml::vec2f v = ml::truncater(m_force, _v);
            m_fuel.setValue(m_fuel.getValue() - _v*m_fuel.getInten());
            m_force -=  v;

            *this += v;
        }
    }
    m_fuel.update();
}

void engine::operator+=(const ml::vec2f &v){
    assert(m_body);
    m_body->setPos(m_body->getPos()+v);
}

void weapon::start(){
    std::thread thread([this](){
        assert(m_ent);
        assert(m_ent->getVision());
        vision &v = const_cast<vision&>(*m_ent->getVision());

        ml::vec3f const col = m_ent->hasPlayer() ? m_ent->getPlayer()->getColor() : ml::vec3f(1.f);
        entity *e = nullptr;

        auto& t = v.getTracked();
        auto& evt = v.evt();

        while(!evt.isDestroyed()){
            for(auto i = t.begin(); i != t.end(); ++i){
                e = *i;
                assert(e);
                if(!e->isAlive()) continue;
                else{
                    *this>>(*e->acqHull());
                    gl::Color(col);
                    gl::DrawLine(m_ent->getPos(), e->getPos());
                    evt.wait_for(m_rate);
                    break;
                }
            }
            if(!evt.isNotified()) evt.wait();
        }
    });
    thread.detach();
}

void weapon::operator>>(hull &h){
    assert(m_ent);
    if(!h.isAlive()) return;

    if(!check(SPLASH)) h -= m_dmg;
    else{
        const ml::vec2f &a = h.getEntity()->getPos();
        sim::getMap()->CastAABB(dnx::aabb(a, ml::vec2f(m_dmg.getValue()*0.5f)), [&](entity *e, const float *d){
            if(e){
                float _d = 1.f/ml::max(ml::distance(a, e->getPos()), 1.f);
                *e->acqHull()-= (m_dmg*_d);
            }
        } , FilterNeutral);
    }
}

void hull::operator-=(const apoint &b){
    assert(m_ent);

    if(!isAlive()){
        m_ent->setDestroy();
        return;
    }

    regen();

    apoint d(b);
    if(isShieldActive()) m_shield -= d;

    m_life -= d;
    if(!isAlive()) m_ent->setDestroy();
}

void hull::regen(){
    m_life.regen();
    m_shield.regen();
    if(isShieldActive()){
        float d = ml::dur<float, ml::tsec>(ml::now(), m_tp)*3.f;
        m_tp = ml::now();
        m_shield.setValue(m_shield.getValue() - (m_shield.getRegen()*d));
        if(!m_shield.isAlive()){
            s_deactivate();
        }
    }
}

void hull::setEntity(entity *e){
    assert(e);
    m_ent = e;
}

void hull::s_activate(){
    if(m_shield.getValue() > 0.f){
        s_act = true;
        m_tp = ml::now();
    }
}
void hull::s_deactivate(){
    s_act = false;
}
