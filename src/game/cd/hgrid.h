#ifndef HGRID_H
#define HGRID_H

#include "cell.h"

namespace sim{

class broad{
public:
    broad() : m_bodies(){}
    virtual ~broad(){
        clear();
    }

    virtual void add(uptr<entity> b){
        /*for(auto &a : m_bodies){
            assert(a.get() != b);
        }*/
        assert(b);
        m_bodies.push_back(std::move(b));
    }

    virtual void clear(){
        const uint32 s = m_bodies.end()-m_bodies.begin();
        m_bodies.clear();
        io::get("out")->info() << "destroyed " << s << " bodies\n";
    }

    _inline auto bbegin() const{
        return m_bodies.begin();
    }
    _inline auto bend() const{
        return m_bodies.end();
    }

    virtual void update(sim::pcont &pairs, const float dt) = 0;

    virtual void CastRay(const dnx::ray &a, sim::cast_cb const &call, sim::cast_filter const &fil) const = 0;
    virtual void CastAABB(const dnx::aabb &a, sim::cast_cb const &call, sim::cast_filter const &fil) const = 0;

    std::vector<uptr<entity>> m_bodies;

private:
    virtual void broadphase(sim::pcont &pairs) = 0;
};
}

class hgrid : public sim::broad{
public:
    hgrid(const dnx::aabb &a, std::vector<ml::vec2f> const &v) : broad(), m_aabb(a){
        for(auto &b : v){
            m_cells.push_back(uptr<cmap>(new cmap(a, b)));
        }
    }
    virtual ~hgrid(){
        clear();
        /*for(auto &b : m_cells){
            delete b;
        }*/
        m_cells.clear();
    }

    const dnx::aabb& getAABB() const{ return m_aabb; }

    virtual void add(uptr<entity> b){
        assert(b);
        rec_attach(b.get());
        broad::add(std::move(b));
    }

    virtual void clear();

    virtual void CastRay(const dnx::ray &a, const sim::cast_cb &call, const sim::cast_filter &fil) const;
    virtual void CastAABB(const dnx::aabb &a, sim::cast_cb const &call, sim::cast_filter const &fil) const;

protected:
    virtual void broadphase(sim::pcont &pairs);
    virtual void rec_attach(entity *b);
    virtual void rec_detach(entity *b);

    std::vector<uptr<cmap>> m_cells;
    const dnx::aabb m_aabb;
};

#endif // HGRID_H
