#ifndef CELL_H
#define CELL_H

#include <dnx/cd/broad.h>
#include "entity.h"

namespace sim{
typedef std::pair<const entity*,const entity*> bpair;
typedef std::unordered_map<ml::vec2u, bpair, ext::num_hash<uint32>> pcont;

typedef std::function<void(entity*, const float *)> cast_cb;
typedef std::function<bool(entity*)> cast_filter;

_inline void castAABB(const dnx::aabb &a, entity *b, cast_cb const& call, cast_filter const &fil){
    if(fil(b)){
        if(dnx::shape::AABBvsAABB(a, b->getAABB())){
            call(b, nullptr);
        }
    }
}
_inline void castRay(const dnx::ray& a, entity *b, cast_cb const &call, cast_filter const &fil){
    if(fil(b)){
        float d(0.f);
        if(dnx::shape::RayvsAABB(a, b->getAABB(), &d))call(b, &d);
    }
}

_inline bool testAABB(const dnx::aabb &a, entity *b, cast_filter const &fil){
    if(fil(b)){
        return (dnx::shape::AABBvsAABB(a, b->getAABB()));
    }
    return false;
}
_inline bool testRay(const dnx::ray& a, entity *b, cast_filter const &fil){
    if(fil(b)){
        return (dnx::shape::RayvsAABB(a, b->getAABB(), nullptr));
    }
    return false;
}

}

class cell{
public:
    cell() : m_aabb(), m_bodies(){}
    cell(const dnx::aabb &a) : m_aabb(a), m_bodies(){}
    ~cell(){ m_bodies.clear(); }

    void attach(entity *b){
        /*for(auto &a : m_bodies){
            if(a==b) return;
        }*/
        m_bodies.emplace_back(b);
    }
    void detach(entity *b){
        /*auto i = m_bodies.begin();
        for(const entity *a : ext::range(i, m_bodies.end())){
            if(a == b){
                m_bodies.erase(i);
                return;
            }
        }*/
        ext::erase<entity*>(m_bodies, b);
    }


    void test(sim::pcont &pairs) const;

    void castRay(const dnx::ray &a, sim::cast_cb const &call, sim::cast_filter const &fil) const;
    void castAABB(const dnx::aabb &a, sim::cast_cb const &call, sim::cast_filter const &fil) const;

    bool testRay(const dnx::ray &a, sim::cast_filter const &fil) const;
    bool testAABB(const dnx::aabb &a, sim::cast_filter const &fil) const;

    void clear() { m_bodies.clear(); }

    _inline const dnx::aabb& getAABB() const { return m_aabb; }
    _inline void setAABB(dnx::aabb const& b) { m_aabb = b; }

    const auto bbegin() const{ return m_bodies.begin(); }
    const auto bend() const{ return m_bodies.end(); }

private:
    dnx::aabb m_aabb;
    std::vector<entity*> m_bodies;
};

class cmap{
public:
    cmap(dnx::aabb const &a, const ml::vec2f &cs) : m_cells(), m_aabb(a), m_cs(cs){
    }
    ~cmap(){
        m_cells.clear();
    }

    void attach(ml::vec2i h, entity *b){
        if(!b->isActive()) return;
        if(m_cells.find(h) == m_cells.end()){
            m_cells.emplace(std::make_pair(h, cell(zone(h))));
        }
        m_cells[h].attach(b);
        gl::Color(ml::vec4f(1.f));
        gl::DrawAABB(m_cells[h].getAABB());
        gl::Color(ml::vec4f(1.f));
    }

    void attach(entity *b);
    void detach(entity *b);

    void clear(){
        m_cells.clear();
    }
    _inline ml::vec2i hashf(ml::vec2f const &v) const{
        return ml::floor<int32>((v-m_aabb.min())/m_cs);
    }
    _inline ml::vec2i hashc(ml::vec2f const &v) const{
        return ml::ceil<int32>((v-m_aabb.min())/m_cs);
    }
    _inline ml::vec2f unhash(ml::vec2i const &h) const{
        return (ml::cast<float>(h)*m_cs)+m_aabb.min();
    }
    _inline dnx::aabb zone(ml::vec2i const h) const{
        return dnx::aabb(unhash(h)+m_cs*0.5f, m_cs*0.5f);
    }

    void broadphase(sim::pcont &pairs) const;

    std::vector<dnx::aabb> broadphaseRec(std::vector<dnx::aabb> const &a) const;

    void broadphaseRange(std::vector<dnx::aabb> const &a, sim::pcont &pairs) const;
    void castRay(const dnx::ray &a, sim::cast_cb const &call, sim::cast_filter const &fil) const;
    void castAABB(const dnx::aabb &a, sim::cast_cb const &call, sim::cast_filter const &fil) const;

    void computeRegions(const std::vector<dnx::aabb> &a, std::vector<const cell *> &b) const;
    void computeRegions(const std::vector<dnx::aabb> &a, std::vector<cell *> &b);
    std::vector<dnx::aabb> castRayRec(std::vector<dnx::aabb> const &r, const dnx::ray &a, sim::cast_filter const &fil) const;
    std::vector<dnx::aabb> castAABBRec(std::vector<dnx::aabb> const &r, const dnx::aabb &a, sim::cast_filter const &fil) const;

    void castRayRange(std::vector<dnx::aabb> const &r, const dnx::ray &a, sim::cast_cb const &call, sim::cast_filter const &fil) const;
    void castAABBRange(std::vector<dnx::aabb> const &r, const dnx::aabb &a, sim::cast_cb const &call, sim::cast_filter const &fil) const;

    void Draw() const{
        gl::Color(ml::vec3f(1.f,0.f,0.f));
        gl::DrawAABB(m_aabb);
        gl::Color(ml::vec3f(1.f));
    }

    const dnx::aabb& getAABB() const{
        return m_aabb;
    }

    const auto cbegin() const{ return m_cells.begin(); }
    const auto cend() const{ return m_cells.end(); }

protected:
    std::unordered_map<ml::vec2i, cell, ext::num_hash<int32>> m_cells;
    dnx::aabb m_aabb;

    const ml::vec2f m_cs;
};

#if 0
class grid : public broad{
public:
    grid(const dnx::aabb &a, ml::vec2f const &cs) : broad(), m_cells(new cmap(a, cs)){
    }
    virtual ~grid(){ clear(); }

    virtual void add(entity *b);
    virtual void clear();

    virtual void CastRay(const dnx::ray &a, const sim::cast_cb &call, const sim::cast_filter &fil) const;
    virtual void CastAABB(const dnx::aabb &a, cast_cb const &call, sim::cast_filter const &fil) const;

protected:
    virtual void broadphase(pcont &pairs);

    uptr<cmap> m_cells;
};
#endif

#endif // CELL_H
