#include "cell.h"
#include <tbb/parallel_for_each.h>
#include <tbb/parallel_do.h>
#include <tbb/pipeline.h>
#include <tbb/parallel_for.h>
#include <utility>

#define paral std::for_each//tbb::parallel_for_each

using namespace dnx;

template<typename Value>

class cancelable_range {

    tbb::blocked_range<Value> m_range;

    volatile bool& m_stop;

public:

    // Constructor for client code

    /** Range becomes empty if stop==true. */

    cancelable_range( Value b, Value e, volatile bool& stop, int grainsize = 1) :

        m_range(b,e,grainsize),

        m_stop(stop)

    {}

    //! Splitting constructor used by parallel_for

    cancelable_range( cancelable_range& r, tbb::split ) :

        m_range(r.m_range,tbb::split()),

        m_stop(r.m_stop)

    {}

    //! Cancel the range.

    void cancel() const {m_stop=true;}

    //! True if range is empty.

    /** Range is empty if there is request to cancel the range. */

    bool empty() const {return m_stop || m_range.empty();}

    //! True if range is divisible

    /** Range becomes indivisible if there is request to cancel the range. */

    bool is_divisible() const {return !m_stop && m_range.is_divisible();}

    //! Initial value in range.

    Value begin() const {return m_range.begin();}

    //! One past last value in range

    /** Note that end()==begin() if there is request to cancel the range.

        The value of end() may change asynchronously if another thread cancels the range. **/

    Value end() const {return m_stop ? m_range.begin() : m_range.end();}

};

#define DATA(a) (a)//.second

///CELL
void cell::test(sim::pcont &pairs) const{
    paral(m_bodies.begin(), m_bodies.end(), [](auto & b){
        const_cast<entity*>(DATA(b))->updateMov();
    });

    for(auto i = m_bodies.begin(), e = m_bodies.end(); i != e; ++i){
        auto j = i;
        for(++j; j != e; ++j){
            pairs[ml::vec2u(DATA(*i)->getID(),DATA(*j)->getID())] = sim::bpair(DATA(*i), DATA(*j));
        }
    }

    /*    tbb::parallel_for(tbb::blocked_range<size_t>(0, m_bodies.end()-m_bodies.begin()), [this](const tbb::blocked_range<size_t>& r){
        for( size_t i=r.begin(); i!=r.end(); ++i )
            const_cast<entity*>(m_bodies[i])->updateMom();
    });

    auto i = m_bodies.begin();

    tbb::parallel_for(tbb::blocked_range<size_t>(0, m_bodies.end()-m_bodies.begin()), [this](const tbb::blocked_range<size_t>& r){
        for( size_t i=r.begin(); i!=r.end(); ++i ){
            for(auto &b : ext::range(i+1, m_bodies.end())){
                pairs[ml::vec2u(a->getID(),b->getID())] = bpair(a, b);
            }
        }
    });
    */
}

void cell::castRay(const dnx::ray &a, sim::cast_cb const &call, sim::cast_filter const &fil) const{
    if(shape::RayvsAABB(a, m_aabb, nullptr)){
        paral(m_bodies.begin(), m_bodies.end(), [&a, &call, &fil](auto& b){
            sim::castRay(a, DATA(b), call, fil);
        });
    }
}

void cell::castAABB(const dnx::aabb &a, sim::cast_cb const &call, sim::cast_filter const &fil) const{
    if(shape::AABBvsAABB(a, m_aabb)){
        paral(m_bodies.begin(), m_bodies.end(), [&a, &call, &fil](auto& b){
            sim::castAABB(a, DATA(b), call, fil);
        });
    }
}

bool cell::testRay(const dnx::ray &a, sim::cast_filter const &fil) const{
    /*bool stop = false;
    if(shape::RayvsAABB(a, m_aabb, nullptr)){
        paral(cancelable_range<int>(0, m_bodies.size(), stop),[&](const cancelable_range<int>& r){//(m_bodies.begin(), m_bodies.end(), [&a, &fil](const entity *b){
            for(int i = r.begin(); i<r.end(); ++i ) {
                if(fil(m_bodies[i])){
                    if(shape::RayvsAABB(a, m_bodies[i]->getAABB(), nullptr)) r.cancel();
                }
            }
        });
    }
    return stop;*/
    if(dnx::shape::RayvsAABB(a, m_aabb, nullptr)){
        for(auto b = m_bodies.begin(), e = m_bodies.end(); b != e; ++b){
            if(fil(DATA(*b))){
                if(shape::RayvsAABB(a, DATA(*b)->getAABB(), nullptr)) return true;
            }
        }
    }
    return false;
}

bool cell::testAABB(const dnx::aabb &a, sim::cast_filter const &fil) const{
    if(dnx::shape::AABBvsAABB(a, m_aabb)){
        for(auto b = m_bodies.begin(), e = m_bodies.end(); b != e; ++b){
            if(fil(DATA(*b))){
                if(dnx::shape::AABBvsAABB(a, DATA(*b)->getAABB())) return true;
            }
        }
    }
    return false;
    /*bool stop = false;
    if(shape::AABBvsAABB(a, m_aabb)){
        paral(cancelable_range<int>(0, m_bodies.size(), stop),[&](const cancelable_range<int>& r){//(m_bodies.begin(), m_bodies.end(), [&a, &fil](const entity *b){
            for(int i = r.begin(); i<r.end(); ++i ) {
                if(fil(m_bodies[i])){
                    if(shape::AABBvsAABB(a, m_bodies[i]->getAABB())) r.cancel();
                }
            }
        });
    }
    return stop;*/
}

///BROADPHASE CLASSES

///CMAP
void cmap::attach(entity *b){
    const dnx::aabb a = b->getBounding();

    const auto imin = hashf(a.min()), imax = hashc(a.max());

    if(imin!=imax){
        for(auto v(imin); v.x < imax.x; ++v.x){
            for(v.y = imin.y; v.y < imax.y; ++v.y)
                attach(v, b);
        }
    }
    else attach(imin, b);
}

void cmap::detach(entity *b){
    const dnx::aabb a = b->getBounding();

    std::vector<cell*> c;
    computeRegions({a}, c);
    for(auto *d : c){
        d->detach(b);
    }
}

void cmap::broadphase(sim::pcont &pairs) const{
    std::for_each(m_cells.begin(), m_cells.end(), [&](std::pair<auto, cell> a){
        a.second.test(pairs);
    });
}

void cmap::computeRegions(std::vector<dnx::aabb> const &a, std::vector<const cell*> &b) const{
    for(auto &d : a){
        for(auto &c : m_cells){
            if(dnx::shape::AABBvsAABB(d, c.second.getAABB()))
                b.push_back(&c.second);
        }
    }
}

void cmap::computeRegions(std::vector<dnx::aabb> const &a, std::vector<cell*> &b){
    for(auto &d : a){
        for(auto &c : m_cells){
            if(dnx::shape::AABBvsAABB(d, c.second.getAABB()))
                b.push_back(&c.second);
        }
    }
}

std::vector<dnx::aabb> cmap::castRayRec(std::vector<dnx::aabb> const &r, const dnx::ray &a, sim::cast_filter const &fil) const{
    std::vector<dnx::aabb> res;
    std::vector<const cell*> cel;

    computeRegions(r, cel);
    paral(cel.begin(), cel.end(), [&](const cell* b){
        paral(r.begin(), r.end(), [&](dnx::aabb c){
            if(dnx::shape::AABBvsAABB(c, b->getAABB())){
                if(b->testRay(a, fil)) res.push_back(b->getAABB());
            }
        });
    });

    return res;
}

std::vector<dnx::aabb> cmap::castAABBRec(std::vector<dnx::aabb> const &r, const dnx::aabb &a, sim::cast_filter const &fil) const{
    std::vector<dnx::aabb> res;
    std::vector<const cell*> cel;

    computeRegions(r, cel);
    paral(cel.begin(), cel.end(), [&](const cell* b){
        paral(r.begin(), r.end(), [&](dnx::aabb c){
            if(dnx::shape::AABBvsAABB(c, b->getAABB())){
                if(b->testAABB(a, fil)) res.push_back(b->getAABB());
            }
        });
    });

    return res;
}

std::vector<dnx::aabb> cmap::broadphaseRec(std::vector<dnx::aabb> const &a) const{
    std::vector<dnx::aabb> res;
    assert(false); //broadphase rec not implemented yet
    /*std::vector<const cell*> cel;

    computeRegions(a, cel);
    for(auto &c : a){
        std::for_each(cel.begin(), cel.end(), [&](const cell *b){
            if(dnx::shape::AABBvsAABB(c, b->getAABB())){
                if(b->testAABB(a, fil)) res.push_back(b->getAABB());
            }
        });
    }*/

    return res;
}
void cmap::castRayRange(std::vector<dnx::aabb> const &r, const dnx::ray &a, sim::cast_cb const &call, sim::cast_filter const &fil) const{
    std::vector<const cell*> c;
    computeRegions(r, c);
    paral(c.begin(), c.end(), [&](const cell* b){
        b->castRay(a, call, fil);
    });
}

void cmap::castAABBRange(std::vector<dnx::aabb> const &r, const dnx::aabb &a, sim::cast_cb const &call, sim::cast_filter const &fil) const{
    std::vector<const cell*> c;
    computeRegions(r, c);
    paral(c.begin(), c.end(), [&](const cell* b){
        b->castAABB(a, call, fil);
    });
}

void cmap::broadphaseRange(std::vector<dnx::aabb> const &a, sim::pcont &pairs) const{
    std::vector<const cell*> c;
    computeRegions(a, c);
    paral(c.begin(), c.end(), [&pairs](const cell* b){
        b->test(pairs);
    });
}

void cmap::castRay(const dnx::ray &a, sim::cast_cb const &call, sim::cast_filter const &fil) const{
    paral(m_cells.begin(), m_cells.end(), [&a, &call, &fil](std::pair<auto, cell> b){
        b.second.castRay(a, call, fil);
    });
}

void cmap::castAABB(const dnx::aabb &a, sim::cast_cb const &call, sim::cast_filter const &fil) const{
    castAABBRange({m_aabb}, a, call, fil);
}
#if 0
void grid::add(entity *b){
    broad::add(b);
    m_cells->attach(m_bodies.back().get());
}

void grid::clear(){
    m_cells->clear();
    broad::clear();
}

void grid::broadphase(sim::pcont &pairs){
    m_cells->broadphase(pairs);
}

void grid::CastRay(const ray &a, cast_cb const &call, cast_filter const &fil) const{
    m_cells->castRay(a, call, fil);
}

void grid::CastAABB(const dnx::aabb &a, const cast_cb &call, const cast_filter &fil) const{
    m_cells->castAABB(a, call, fil);
}


#if 0
void grid::update(sim::pcont &pairs, const float dt){
    m_cells->clear();

    ext::erase_if<uptr<entity>>(m_bodies.end, [this](uptr<entity> &b){
        if(!dnx::alive(b.get())) return true;
        m_cells->attach(b.get());
        return false;
    });

    ///compute velocities
    std::for_each(m_bodies.begin(), m_bodies.end(), [](uptr<entity> &b){
        b->computeVelo();
    });

    ///broadphase
    broadphase(pairs);

    ///update position
    std::for_each(m_bodies.begin(), m_bodies.end(), [](uptr<entity> &b){
       b->update();
    });
}
#endif
#endif
