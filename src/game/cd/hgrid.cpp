#include "hgrid.h"
#include <tbb/parallel_for_each.h>
#include <tbb/blocked_range.h>

using namespace dnx;
void hgrid::rec_attach(entity *b){
    std::for_each(m_cells.begin(), m_cells.end(), [&b](uptr<auto> &a){
        a->attach(b);
    });
}

void hgrid::rec_detach(entity *b){
    std::for_each(m_cells.begin(), m_cells.end(), [&b](uptr<auto> &a){
        a->detach(b);
    });
}

void hgrid::clear(){
    std::for_each(m_cells.begin(), m_cells.end(), [](uptr<auto> &a){
        a->clear();
    });
    broad::clear();
}

void hgrid::CastRay(const dnx::ray &a, sim::cast_cb const &call, sim::cast_filter const &fil) const{
    assert(!m_cells.empty());
    std::vector<dnx::aabb> r({m_aabb});
    for(const auto &b : ext::range(m_cells.begin(),m_cells.end()-1)){
        r = b->castRayRec(r, a, fil);
        if(r.empty()) return;
    }
    m_cells.back()->castRayRange(r, a, call, fil);
}

void hgrid::CastAABB(const dnx::aabb &a, const sim::cast_cb &call, const sim::cast_filter &fil) const{
    assert(!m_cells.empty());
    std::vector<dnx::aabb> r({a});
    for(const auto &b : ext::range(m_cells.begin(),m_cells.end()-1)){
        r = b->castAABBRec(r, a, fil);
        if(r.empty()) return;
    }

    m_cells.back()->castAABBRange(r, a, call, fil);
}

void hgrid::broadphase(sim::pcont &pairs){
    assert(!m_cells.empty());
    /*std::vector<dnx::aabb> r({m_aabb});
    for(const auto &b : ext::range(m_cells.begin(),m_cells.end()-1)){
        r = b->broadphaseRange(r);
        if(r.empty()) return;
    }*/
    m_cells.back()->broadphase/*Range(r, */(pairs);
}
