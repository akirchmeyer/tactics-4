#ifndef WINDOW_H
#define WINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <ml/types.h>
#include "common.h"
#include <ul/io.h>

class GameInstance;
class Input;

class _DestroyWindow
{
public:
    _inline void operator()(GLFWwindow* win) const{
        glfwDestroyWindow(win);
        io::get("this")->debug("deleted GLFWwindow\n");

        io::get("this")->info("Terminating GLFW\n");
        glfwTerminate();
        //dbg::log("window destroyed", _FILE, _LINE, _FUNC, code::act, 0);
    }
};

class window
{
public:
    window(GLFWwindow *w, const ml::vec2f &s);
    ~window();

    void init();
    void run();

    void handle();
    void update();
    void draw() const;

    static void ResizeFB(GLFWwindow* window, const int width, const int height);

    const GameInstance &getGameInstance() const{
        return *m_int;
    }

    GameInstance &acqGameInstance(){
        return *m_int;
    }

private:
    std::unique_ptr<GLFWwindow, _DestroyWindow> m_win;
    std::unique_ptr<Input> m_inp;
    int32 m_width, m_height;

    uptr<GameInstance> m_int;
};

#endif // WINDOW_H
