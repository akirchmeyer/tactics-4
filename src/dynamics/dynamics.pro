TEMPLATE = lib
CONFIG += staticlib

CONFIG += BUILD_ML BUILD_GLFW BUILD_STBI BUILD_GLEW BUILD_TBB BUILD_BOOST

include(../../deps.pri)

TARGET = $$NAME_DNX

DEFINES += ENABLE_GL

INCLUDEPATH += ./src/dnx

SOURCES += \
    src/dnx/body.cpp \
    src/dnx/world.cpp \
    src/dnx/shape.cpp \
    src/dnx/props.cpp \
    src/dnx/cd/narrow.cpp \
    src/dnx/mesh.cpp

HEADERS += \
    src/dnx/body.h \
    src/dnx/common.h \
    src/dnx/world.h \
    src/dnx/shape.h \
    src/dnx/dnx.h \
    src/dnx/props.h \
    src/dnx/dbg.h \
    src/dnx/cd/cast.h \
    src/dnx/cd/broad.h \
    src/dnx/cd/narrow.h \
    src/dnx/mesh.h

