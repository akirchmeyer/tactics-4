#ifdef ENABLE_GL

#ifndef DBG
#define DBG

#include <gl/draw.h>
#include "shape.h"

namespace gl{
_inline void DrawAABB(const dnx::aabb &b){
    gl::DrawAABB(b.cen-b.he, b.cen+b.he);
}

_inline void DrawQuad(const dnx::aabb &b){
    gl::DrawQuad(b.min(), b.max());
}

_inline void DrawRay(const dnx::ray &r){
    gl::DrawLine(r.p0, r.p1);
}
}

#endif // DBG

#endif
