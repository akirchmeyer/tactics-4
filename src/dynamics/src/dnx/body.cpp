#include "body.h"
#include "common.h"
#include "world.h"

namespace ml{
uint32 icount = 0;
uint32 gen_id(){
    return icount++;
}

void reset_id(){
    icount = 0;
}
}

namespace dnx{
body::body(const dnx::world *w, const transform &_t, const dnx::props *props, dnx::shape *s)
    : m_shape(s), t(_t), m_ppos(_t.p)/*, m_velo(0.f)*/, m_mom(0.f), m_acc(0.f), e_destroy(this), e_move(this), m_alive(true), m_active(true), m_world(w), m_props(props), m_id(ml::gen_id())
{
    assert(m_world);
    assert(m_props);
    assert(m_shape);

    m_shape->setTransform(&t);
    m_shape->calcAABB();
}

dnx::shape *body::setShape(dnx::shape *s){
    assert(s);
    dnx::shape *old = m_shape.release();
    m_shape = std::unique_ptr<dnx::shape>(s);
    m_shape->setTransform(&t);
    m_shape->calcAABB();
    return old;
}

void body::update(){
    if(!m_active) return;
    //m_velo += m_acc * dt;


    //pos() += m_mom;
    updateMom();

    m_acc = ml::vec2f(0.f);

    assert(m_shape);
    m_shape->calcAABB();
}

void body::setDestroy(){
    m_alive = false; m_active = false;
}

void body::computeVelo(){
    if(!m_active) return;

    updateMom();
    m_ppos = getPos();
}
}

