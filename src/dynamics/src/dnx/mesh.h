#ifndef MESH_H
#define MESH_H

#include "common.h"
#include "shape.h"
#include <el/el.h>

namespace dnx{

struct vert{
    constexpr vert() : v(0.f), n(0.f) {}
    constexpr vert(ml::vec2f const &_v, ml::vec2f const &_n) : v(_v), n(_n){}
    ml::vec2f v, n;
};

class data : public del{
public:
    data() : m_verts(), i(){ }
    virtual ~data(){ free(); }

    virtual void free();

    void load(std::vector<ml::vec2f> _v, std::vector<uint16> _i);
    virtual void init();

    static dnx::data* create(std::vector<ml::vec2f> _v, std::vector<uint16> _i);

    _inline void alloc(uint32 const nv, uint32 const ni){
        free();
        m_verts.resize(nv); i.resize(ni);
    }

    _inline const ml::vec2f &vert(uint16 const j) const { return m_verts[j].v; }
    _inline const ml::vec2f &norm(uint16 const j) const { return m_verts[j].n; }

    _inline std::vector<dnx::vert> &verts(){ return m_verts; }
    //_inline std::vector<ml::vec2f> &norms(){ return n; }
    _inline std::vector<uint16> &idx()  { return i; }

    _inline const ml::vec2f& extent() const{ return m_ext; }

protected:
    std::vector<dnx::vert>	m_verts;
    std::vector<uint16> i;

    ml::vec2f m_ext;

    void compute_normals();
};

class convex : public shape{
public:
    convex(dnx::data *d) : shape(), m_data(d){ ///CW
        assert(m_data);
        assert(ml::lengthSq(m_data->norm(0)) != 0);
    }
    virtual ~convex(){
        m_data = nullptr;
    }

    static convex *create_rect(const ml::vec2f &ext);

    _inline void reset(dnx::data *d){
        m_data = d;
    }

    _inline dnx::data *data(){ return m_data; }

    virtual void calcAABB(){
        assert(m_data);
        assert(t);
        ml::vec2f min(INFINITY), max(-INFINITY);

        for(const auto &i : ext::irange(uint32(0), nverts())){
            min = ml::min(min, wsv(i));
            max = ml::max(max, wsv(i));
        }
        _aabb = aabb(t->p, (max-min)*0.5f);
    }

    _inline virtual const ml::vec2f& lsv(uint16 const id) const{
        assert(m_data);
        assert(t);
        return m_data->vert(id);
    }
    _inline virtual const ml::vec2f& lsn(uint16 const id) const{
        assert(m_data);
        assert(t);
        return m_data->norm(id);
    }
    _inline virtual ml::vec2f wsv(uint16 const id) const{
        assert(m_data);
        assert(t);
        return ((*t) * m_data->vert(id));
    }
    _inline virtual ml::vec2f wsn(uint16 const id) const{
        assert(m_data);
        assert(t);
        return (*t).q * m_data->norm(id);
    }

    _inline auto ibegin(){
        assert(m_data);
        return m_data->idx().begin();
    }
    _inline auto iend(){
        assert(m_data);
        return m_data->idx().end();
    }
    _inline auto inext(std::vector<uint16>::iterator const &i){
        auto j = i+1;
        return (j != iend()) ? j : ibegin();
    }
    _inline auto vbegin(){
        assert(m_data);
        return m_data->verts().begin();
    }
    _inline auto vend(){
        assert(m_data);
        return m_data->verts().end();
    }
    _inline auto vnext(std::vector<dnx::vert>::iterator const &i){
        auto j = i+1;
        return (j != vend()) ? j : vbegin();
    }

    uint32 nverts() const{
        assert(m_data);
        return m_data->verts().size();
    }

    _inline virtual convex* duplicate() const{
        return new convex(m_data);
    }

    virtual ml::vec2f support(const ml::vec2f &dir) const{
        std::vector<uint16> i = m_data->idx();
        std::pair<float, uint32> res(-INFINITY, NAN);
        float d = 0.f;

        for (auto j = i.begin(), e = i.end(); j != e; ++j) {
            d = ml::dot(dir, wsv(*j));
            if (d > res.first) res = std::make_pair(d, *j);
        }
        return wsv(res.second);
    }
    virtual void support(const ml::vec2f &dir, std::vector<ml::vec2f> &r) const{
        std::vector<uint16> a = m_data->idx();
        float res = -INFINITY;
        float d = 0.f;

        for (auto j = a.begin(), e = a.end(); j != e; ++j) {
            d = ml::dot(dir, wsv(*j));
            if (d > res) {
                res = d;
                r.clear(); r.push_back(wsv(*j));
            }
            else if(d == res){
                r.push_back(wsv(*j));
            }
        }
    }

protected:
    dnx::data *m_data;
};

#if 0
class circle;

class circle : public shape{
public:
    circle(float rad) : shape(), m_rad(rad){
    }

    virtual void calcAABB(){
        _aabb = aabb((*m_pos), ml::vec2f(m_rad));
    }

    virtual shape *duplicate(){
        return new circle(m_rad);
    }

    virtual ml::vec2f support(ml::vec2f dir) {
        assert(m_pos);
        return (*m_pos)+dir*m_rad;
    }

    virtual const ml::vec2f vert(uint16 id){
        return ml::vec2f(0.f);
    }
    virtual const ml::vec2f norm(uint16 id){
        return ml::vec2f(0.f);//ml::normalize((*m_pos));
    }

    float m_rad;
};
 #endif
}

#endif // MESH_H
