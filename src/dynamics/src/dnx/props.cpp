#include "props.h"
#include <ul/io.h>

namespace dnx{

props::props(const float m, const float f, const float b) : m_mass(m), m_frict(f), m_bounce(b)
{
}

props::~props(){
    io::get("this")->debug("deleted props\n");
}
}
