#include "mesh.h"
#include <ml/functions.h>
#include <fstream>
#include <ul/common.h>

namespace dnx{
convex *convex::create_rect(const ml::vec2f &ext){
    dnx::data *d = data::create(std::vector<ml::vec2f>() = {ml::vec2f(0.f), ml::vec2f(ext.x, 0.f), ml::vec2f(0.f, ext.y), ext},
                                                    std::vector<uint16>() = {0, 1, 2, 1, 2, 3});
    return new convex((dnx::data*)gc::create(d));
}

void data::free(){
    m_verts.clear(); i.clear();
}

data *data::create(std::vector<ml::vec2f> _v, std::vector<uint16> _i){
    dnx::data *d = new data();
    d->load(_v, _i);
    d->init();
    return d;
}

void data::load(std::vector<ml::vec2f> _v, std::vector<uint16> _i){
    free();
    m_verts.resize(_v.size());
    for(uint32 a = 0, e = _v.size(); a != e; ++a){
        m_verts[a].v = _v[a];
    }
    i = std::move(_i);
}

void data::init(){
    if(m_verts.empty()){
        m_ext = ml::vec2f(0.f);
        return;
    }

    ml::vec2f min(INFINITY), max(-INFINITY);
    std::for_each(m_verts.begin(), m_verts.end(), [&min, &max](dnx::vert &a){
        min = ml::min(min, a.v);
        max = ml::max(max, a.v);
    });
    m_ext = max-min;
    ml::vec2f cen = min+m_ext*0.5f;

    std::for_each(m_verts.begin(), m_verts.end(), [&cen](dnx::vert &a){
        a.v -= cen;
    });

    compute_normals();
}

void data::compute_normals(){
    std::for_each(m_verts.begin(), m_verts.end(), [](dnx::vert &a){
        a.n = ml::normalize(ml::perp(a.v));
    });
}
}
