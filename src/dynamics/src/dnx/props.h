#ifndef PROPS_H
#define PROPS_H

#include "common.h"

namespace dnx{
class props : public del
{
public:
    virtual ~props();

    _inline const float &mass() const{
        return m_mass;
    }

    _inline const float &bounce() const{
        return m_bounce;
    }

    _inline const float &friction() const{
        return m_frict;
    }

    props(float const m, float const f, float const b);

    float m_mass, m_frict, m_bounce;
};
}

#endif // PROPS_H
