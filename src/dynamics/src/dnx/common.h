#ifndef DNX_COMMON_H
#define DNX_COMMON_H

#include <ul/common.h>
#include <ml/functions.h>

namespace ml{
struct rot
{
        constexpr rot() : s(0.f), c(1.f){}
        constexpr rot(const float a) : s(ml::sin(a)), c(ml::cos(a)){}
        constexpr rot(float const _s, float const _c) : s(_s), c(_c){}

        /// Get the angle in radians
        constexpr _inline float angle() const { return ml::atan2(s, c); }
        /// Get the x-axis
        constexpr _inline const ml::vec2f x() const { return ml::vec2f(c, s); }
        /// Get the y-axis
        constexpr _inline const ml::vec2f y() const { return ml::vec2f(-s, c); }

        /// Sine and cosine
        float s, c;


        constexpr rot(const rot &b) : s(b.s), c(b.c) {}
        _inline rot operator=(rot b){
            swap(b);
            return *this;
        }

        void swap(rot &b){
            std::swap(s, b.s);
            std::swap(c, b.c);
        }
};

constexpr _inline rot operator*(const rot& q, const rot& r){
    return rot(q.s * r.c + q.c * r.s, q.c * r.c - q.s * r.s);
}
constexpr _inline rot operator/(const rot& q, const rot& r){
    return rot(q.c * r.s - q.s * r.c, q.c * r.c + q.s * r.s);
}

constexpr _inline vec2f operator*(const rot& q, const vec2f& v){
    return vec2f(q.c * v.x - q.s * v.y, q.s * v.x + q.c * v.y);
}
constexpr _inline vec2f operator/(const rot& q, const vec2f& v)
{
        return vec2f(q.c * v.x + q.s * v.y, -q.s * v.x + q.c * v.y);
}

}

namespace dnx{
struct transform
{
        constexpr transform(): q(), p(0.f){}

        /// Initialize using a position vector and a rotation.
        constexpr transform(const ml::vec2f &_p, const ml::rot &r) : q(r), p(_p) {}

        ml::rot q;
        ml::vec2f p;

        constexpr transform(const transform &s) : q(s.q), p(s.p) {}
        _inline transform operator=(transform b){
            swap(b);
            return *this;
        }

        void swap(transform &s){
            std::swap(q, s.q);
            std::swap(p, s.p);
        }
};

constexpr _inline ml::vec2f operator*(const transform& T, const ml::vec2f& v){
    return (T.q*v)+T.p;
}

constexpr _inline ml::vec2f operator/(const transform& T, const ml::vec2f& v){
    return T.q/(v-T.p);
}

constexpr _inline transform operator*(const transform& A, const transform& B){
        return transform(A.q*B.p + A.p, A.q*B.q);
}
constexpr _inline transform operator/(const transform& A, const transform& B){
        return transform(A.q/(B.p-A.p), A.q/B.q);
}
}

#endif // COMMON_H
