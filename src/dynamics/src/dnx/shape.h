#ifndef SHAPE_H
#define SHAPE_H

#include "common.h"
#include <ul/console.h>

namespace dnx{

class body;

class aabb{
public:
    constexpr aabb() : cen(0.f), he(0.f) {}
    constexpr aabb(const ml::vec2f &_cen, const ml::vec2f &half_extent) : cen(_cen), he(half_extent){}
    constexpr aabb(const aabb &a, const ml::vec2f &p) : cen(a.cen+p), he(a.he){}
    constexpr aabb(const aabb& a) : cen(a.cen), he(a.he){}

    _inline aabb& operator=(aabb a){
        swap(a);
        return *this;
    }

    static aabb fromMinMax(const ml::vec2f &min, const ml::vec2f &max){
        return aabb((max+min)*0.5f,(max-min)*0.5f);
    }
    static aabb fromMinExt(const ml::vec2f &min, const ml::vec2f &ext){
        return aabb(min+(ext*0.5f), ext*0.5f);
    }
    static aabb fromvelo(const aabb &a, const ml::vec2f &v){
        ml::vec2f m = a.cen+v;
        return fromMinMax(ml::min(a.min(), m), ml::max(a.max(), m));
    }

    constexpr ml::vec2f min() const{
        return cen-he;
    }
    constexpr ml::vec2f max() const{
        return cen+he;
    }
    constexpr ml::vec2f ext() const{
        return he+he;
    }

    constexpr _inline bool test(const aabb &b) const{
        return (ml::abs(cen.x - b.cen.x) < ml::abs(he.x + b.he.x)) ? (ml::abs(cen.y - b.cen.y) < ml::abs(he.y + b.he.y)) : false;
    }

    constexpr _inline bool test(const ml::vec2f &b) const{
        return (ml::abs(cen.x - b.x) < he.x) ? (ml::abs(cen.y - b.y) < he.y) : false;
    }

    constexpr bool test(ml::vec2f const &b, const float r) const{
        return ml::distanceSq(ml::clamp(b, min(), max()), b) < (r*r);
    }
    bool test(ml::vec2f const &b, float const r, float &d) const{
        return (d = ml::distance(ml::clamp(b, min(), max()), b)) < r;
    }

    void swap(dnx::aabb& b){
        std::swap(cen, b.cen);
        std::swap(he, b.he);
    }

    ml::vec2f cen, he;//center // half extent
};

/*constexpr std::ostream& operator<<(std::ostream& a, aabb const& b){
    return a << "min: " << b.min() << "\nmax: " << b.max() << '\n';
}*/

constexpr aabb operator+ (const aabb &a, const ml::vec2f &b){
    return aabb(a, b);
}
constexpr aabb operator- (const aabb &a, const ml::vec2f &b){
    return aabb(a, -b);
}

class ray{
public:
    ray(const ml::vec2f &origin,const ml::vec2f &end): p0(origin), p1(end), idir(0.f){
        norm();
    }

    _inline void norm(){
        idir = ml::normalize(p1-p0);
    }

    ml::vec2f p0, p1, idir;
};

class shape
{
public:
    constexpr shape() : t(nullptr){    }
    virtual ~shape(){}

    virtual void calcAABB() = 0;

    void setTransform(transform *_t){
        assert(_t);
        t = _t;
    }
    void resetTransform(){
        transform _t;
        t = &_t;
        calcAABB();
        t = nullptr;
    }

    _inline const aabb &getAABB() const{ return _aabb; }
    _inline const ml::vec2f& getPos() const{ assert(t); return t->p; }
    _inline const transform& getTrans() const{ assert(t); return *t; }

    virtual const ml::vec2f& lsv(const uint16 id) const= 0;
    virtual const ml::vec2f& lsn(const uint16 id) const = 0;
    virtual ml::vec2f support(const ml::vec2f &dir) const = 0;

    virtual shape *duplicate() const = 0;

    static bool AABBvsAABB(const aabb &a, const aabb &b);
    static bool RayvsAABB(const ray &a, aabb const &b, float *dist);
    static bool SweepAABBvsAABB(aabb const &a, aabb const &b, ml::vec2f const &va, ml::vec2f const &vb, float &u0, float &u1); //normalized time of first + second collision

    transform *t;
    aabb _aabb;
};
}


#endif // SHAPE_H
