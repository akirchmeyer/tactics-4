#ifndef WORLD_H
#define WORLD_H

#include "cd/broad.h"
#include "cd/narrow.h"

namespace dnx{

class world : public del
{
public:
    world(broad *b, narrow *n);
    virtual ~world();

    void add(uptr<body> b){
        assert(m_broad);
        assert(b);
        m_broad->add(std::move(b));
    }
    void update(const float dt);
    void clear();

    /*_inline auto begin(){
        return m_broad->begin();
    }
    _inline auto end(){
        return m_broad->end();
    }*/

    _inline const broad* getBroad(){ return m_broad.get(); }
    _inline broad* acqBroad(){ return m_broad.get(); }

protected:
    uptr<broad> m_broad;
    uptr<narrow> m_narrow;
};
}

#endif // WORLD_H
