#ifndef NARROW_H
#define NARROW_H

#include "../body.h"
#include "../mesh.h"

namespace dnx{
typedef void (*Solver)(const bpair &p, const float dt, const float pend, const ml::vec2f &con);

class narrow
{
public:
    narrow(const Solver &sol) : m_solver(sol){
    }

    virtual ~narrow(){
    }

    virtual void narrowphase(const float dt, pcont &pairs) = 0;

    Solver m_solver;
};

class naiven : public narrow{
public:
    naiven(const Solver &sol) : narrow(sol){
    }

    virtual ~naiven(){
    }

    void test(const bpair &p, const float dt);

    virtual void narrowphase(const float dt, pcont &pairs){
        std::for_each(pairs.begin(), pairs.end(), [this, &dt](auto &a){
            this->test(a.second, dt);
        });
        pairs.clear();
    }
};
}

#endif // NARROW_H
