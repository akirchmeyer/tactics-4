#ifndef BROAD_H
#define BROAD_H



#include <ul/console.h>
#include <unordered_map>

#include "../body.h"
#include "../dbg.h"
#include "cast.h"
#include <el/el.h>
#include <tbb/concurrent_vector.h>
#include <tbb/concurrent_hash_map.h>

namespace dnx{
class broad{
public:
    broad() : m_bodies(){}
    virtual ~broad(){
        clear();
    }

    virtual void add(uptr<Body> b){
        /*for(auto &a : m_bodies){
            assert(a.get() != b);
        }*/
        assert(b);
        m_bodies.push_back(std::move(b));
    }

    virtual void clear(){
        const uint32 s = m_bodies.end()-m_bodies.begin();
        m_bodies.clear();
        io::get("out")->info() << "destroyed " << s << " bodies\n";
    }

    _inline auto bbegin() const{
        return m_bodies.begin();
    }
    _inline auto bend() const{
        return m_bodies.end();
    }

    virtual void update(pcont &pairs, const float dt) = 0;

    virtual void CastRay(const dnx::ray &a, cast_cb const &call, cast_filter const &fil) const = 0;
    virtual void CastAABB(const dnx::aabb &a, cast_cb const &call, cast_filter const &fil) const = 0;

    std::vector<uptr<Body>> m_bodies;

private:
    virtual void broadphase(pcont &pairs) = 0;
};

#if 0
class naiveb : public broad{
public:
    naiveb() : broad(){ }
    virtual ~naiveb(){
        clear();
    }

    virtual void update(pcont &pairs, const float dt);

    virtual void CastRay(const ray &a, cast_cb call, cast_filter fil) const;
    virtual void CastAABB(const dnx::aabb &a, cast_cb call, cast_filter fil) const;

private:
    virtual void broadphase(pcont &pairs) const;
};

void naiveb::update(pcont &pairs, const float dt){
    ext::erase_if<uptr<dnx::body>>(m_bodies, [](uptr<body> &b){
        return !dnx::alive(b.get());
    });

    std::for_each(m_bodies.begin(),m_bodies.end(), [](uptr<body> &b){
        b->update();
    });

    broadphase(pairs);
}

void naiveb::broadphase(pcont &pairs) const{
    dnx::aabb a;

    for(auto i = m_bodies.begin(), e = m_bodies.end(); i != e; ++i){
        a = (*i)->bounding();
        std::for_each(i+1, e, [&](const uptr<body> &b){
            if(shape::AABBvsAABB(a, b->bounding())) pairs.push_back(bpair((*i).get(), b.get()));
        });
    }
}

void naiveb::CastRay(const dnx::ray &a, cast_cb call, cast_filter fil) const{
    std::for_each(m_bodies.begin(), m_bodies.end(), [&a, &call, &fil](const uptr<body> &j){
        dnx::testRay(a, j.get(), call, fil);
    });
}

void naiveb::CastAABB(const dnx::aabb &a, cast_cb call, cast_filter fil) const{
    std::for_each(m_bodies.begin(), m_bodies.end(), [&a, &call, &fil](const uptr<body> &j){
        dnx::testAABB(a, j.get(), call, fil);
    });
}

#endif

}

#endif // BROAD_H
