#ifndef CAST_H
#define CAST_H

#include "../body.h"
#include "../shape.h"
#include <algorithm>
#include <functional>

typedef dnx::body Body;

namespace dnx{
typedef std::function<void(const Body*, const float *)> cast_cb;
typedef std::function<bool(const Body*)> cast_filter;

typedef std::pair<float, dnx::body*> crp;

_inline void closest(crp const &a, crp &b){
    if(a.first > 0.f && a.first < b.first) b = a;
}

#if 0
class caster{
public:
    caster(){}
    virtual ~caster(){}
    virtual bool operator()(dnx::body *b, float *d) = 0;
};
/*
class caster_ray : public caster{
public:
    caster_ray(dnx::ray ray) : caster(), m_ray(ray){
    }
    void reset(dnx::ray ray){
        m_ray = ray;
    }
    virtual bool operator()(dnx::body *b, float *d){
        return shape::RayvsAABB(m_ray, b->AABB(), d);
    }

protected:
    dnx::ray m_ray;
};

class caster_aabb : public caster{
public:
    caster_aabb(dnx::aabb aabb) : caster(), m_a(aabb){
    }
    void reset(dnx::aabb aabb){
        m_a = aabb;
    }
    virtual bool operator()(dnx::body *b, float *d){
        *d = 0.f;
        return shape::AABBvsAABB(m_a, b->AABB());
    }

protected:
    dnx::aabb m_a;
};

class caster_sweep : public caster_aabb{
public:
    caster_sweep(dnx::aabb aabb, ml::vec2f velo) : caster_aabb(aabb), m_velo(velo){
    }
    virtual bool operator()(dnx::body *b, float *d){
        float u1;
        return shape::SweepAABBvsAABB(m_a, b->AABB(), m_velo, b->mom(), *d, u1);
    }

protected:
    ml::vec2f m_velo;
};*/

class cclosest : public dnx::cast_cb{
public:
    constexpr cclosest() : dnx::cast_cb(), p0(0.f), p1(0.f), dist(INFINITY), body(nullptr){}

    virtual ~cclosest(){
        reset();
    }

    void reset(){
        p0 = ml::vec2f(0.f); p1 = ml::vec2f(0.f);
        dist = INFINITY;
        body = nullptr;
    }

    virtual void operator()(dnx::body *b,const float *d){
        if(d){
            if ((*d) < dist){ // hit is closer than previous, so update
               dist = *d;
               body = b;
            }
        }
    }

    ml::vec2f p0, p1;
    float dist;
    dnx::body *body;
};

class cadd : public dnx::cast_cb{
public:
    constexpr cadd(dnx::bcont &vec) : dnx::cast_cb(), m_vec(vec){
    }

    void reset(dnx::bcont &v){
        m_vec = v;
    }

    dnx::bcont& vec(){
        return m_vec;
    }

    virtual void operator()(dnx::body *b, const float *d){
        for(auto &i : m_vec){
            if(i == b) return;
        }
        m_vec.push_back(b);
    }

private:
    dnx::bcont &m_vec;
};
#endif

_inline void castAABB(const dnx::aabb &a, const dnx::body *b, cast_cb const& call, cast_filter const &fil){
    if(fil(b)){
        if(shape::AABBvsAABB(a, b->getAABB())){
            call(b, nullptr);
        }
    }
}
_inline void castRay(const dnx::ray& a, const dnx::body *b, cast_cb const &call, cast_filter const &fil){
    if(fil(b)){
        float d(0.f);
        if(shape::RayvsAABB(a, b->getAABB(), &d))call(b, &d);
    }
}

_inline bool testAABB(const dnx::aabb &a, const dnx::body *b, cast_filter const &fil){
    if(fil(b)){
        return (shape::AABBvsAABB(a, b->getAABB()));
    }
    return false;
}
_inline bool testRay(const dnx::ray& a, const dnx::body *b, cast_filter const &fil){
    if(fil(b)){
        return (shape::RayvsAABB(a, b->getAABB(), nullptr));
    }
    return false;
}

}

#endif // CAST_H
