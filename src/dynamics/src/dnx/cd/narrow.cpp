#include "narrow.h"
#include "../dbg.h"
#include <ul/console.h>

namespace dnx{
#if 1
_inline ml::vec2f ppoe(ml::vec2f a,ml::vec2f b,ml::vec2f o) {
    ml::vec2f ab(b-a);
    ml::vec2f ao(o-a);

    // time along edge
    float t = ml::clamp(ml::dot(ab, ao) / ml::lengthSq(ab), 0.f, 1.f);
    // form point
    return a + ab * t;
}


bool test(const convex *a,const convex *b, float &pend, ml::vec2f &contact, const float s,const ml::vec2f &origin){
    std::vector<ml::vec2f> sup;

    // face of A, vertices of B
    /*for (auto i = a->ibegin(), e = a->iend(); i != e; ++i){
        ml::vec2f wsN = a->wsn(*i);
        ml::vec2f wV0 = a->wsv(*i), wV1 = a->wsv(*a->inext(i));*/

    for(uint32 i = 0, e = a->nverts(); i != e; ++i){
        ml::vec2f wsN = a->wsn(i);
        ml::vec2f wV0 = a->wsv(i), wV1 = a->wsv((i+1)%e);

        // get supporting vertices of B, most opposite face normal
        b->support(-wsN, sup);
        gl::Color(ml::vec3f(1.f, 0.f, 0.f));
        gl::DrawQuad(dnx::aabb(wV0-a->getPos(), ml::vec2f(0.01f)));
        gl::Color(ml::vec3f(1.f, 0.f, 1.f));
        gl::DrawQuad(dnx::aabb(wV1-a->getPos(), ml::vec2f(0.01f)));

        std::for_each(sup.begin(), sup.end(), [&](ml::vec2f const &j){
            ml::vec2f mfp0 = (j-wV0)*s;
            ml::vec2f mfp1 = (j-wV1)*s;

            float faceDist = ml::dot(mfp0, wsN)*s;
            //if(faceDist > 0.f) return false;

            ml::vec2f p = ppoe(mfp0, mfp1, origin);
            float dist = ml::sgn(faceDist)*ml::length(p);

            /*gl::Color(ml::vec3f(1.f));
            gl::DrawRay(dnx::ray(a->getPos(), a->getPos()+p));*/

            if(dist > 0.f && dist < pend){
                contact = p;
                pend = dist;
            }
            else if(dist>pend){
                contact = p;
                pend = dist;
            }
        });
    }

    return (pend<0.f); //true; //(ml::lengthSq(contact) != 0.f);
}

bool MD(const body *_a, const body *_b, float &pend, ml::vec2f &contact){

    const convex *a = (const dnx::convex*)_a->getShape();
    const convex *b = (const dnx::convex*)_b->getShape();
    contact = ml::vec2f(0.f);

    ml::vec2f ori = ml::vec2f(0.f);//a->getPos()-b->getPos();

    test(a, b, pend, contact, 1.f, ori);
    test(b, a, pend, contact, -1.f, ori);
        /*gl::Color(ml::vec3f(1.f));
        gl::DrawRay(dnx::ray(ml::vec2f(0.f), contact));*/
        /*return true;
    }
    return false;*/return (pend<0.f); //(ml::lengthSq(contact) != 0.f);
}

void naiven::test(const bpair &p, const float dt){
    if(p.first == p.second) return;
    float pend = -INFINITY;

    ml::vec2f con;
    if(MD(p.first, p.second, pend, con))
        m_solver(p, dt, pend, con);
}
#endif
}
