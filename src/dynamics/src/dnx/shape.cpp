#include "shape.h"

namespace dnx{
bool shape::AABBvsAABB(const aabb &a, const aabb &b){
    return a.test(b);
}


_inline bool RaySlabIntersect(const float slabmin, const float slabmax, const float raystart, const float rayend, float& tbenter, float& tbexit)
{
    float raydir = rayend - raystart;

    // ray parallel to the slab
    if (ml::abs(rayend - raystart) < 1.0E-9f){
        return ((slabmin < raystart) && (raystart < slabmax));
    }

    // slab's enter and exit parameters
    float tsenter = (slabmin - raystart) / raydir;
    float tsexit = (slabmax - raystart) / raydir;

    // order the enter / exit values.
    if(tsenter > tsexit){
        std::swap(tsenter, tsexit);
    }

    // make sure the slab interval and the current box intersection interval overlap
    if (tbenter > tsexit || tsenter > tbexit){
        // nope. Ray missed the box.
        return false;
    }
    // yep, the slab and current intersection interval overlap
    else{
        // update the intersection interval
        tbenter = ml::max(tbenter, tsenter);
        tbexit = ml::min(tbexit, tsexit);
        return true;
    }
}

_inline bool SegmentAABBoxIntersect(aabb const &b, ray const &a, float &tinter)
{
    // initialise to the segment's boundaries.
    float tenter = 0.0f, texit = 1.0f;

    // test X slab
    if (!RaySlabIntersect(b.min().x, b.max().x, a.p0.x, a.p1.x,tenter, texit))
    {
        return false;
    }

    // test Y slab

    if (!RaySlabIntersect(b.min().y, b.max().y, a.p0.y, a.p1.y, tenter, texit))
    {
        return false;
    }

    // test Z slab
#if TEST_Z
    if (!RaySlabIntersect(b.min().z, b.max().z, a.p0.z, a.p1.z, tenter, texit))
    {
        return false;
    }
#endif

    // all intersections in the green. Return the first time of intersection, tenter.
    tinter = tenter;
    return  true;
}



bool shape::RayvsAABB(ray const &a, aabb const &b, float *dist){
    float d = 0.f;
    bool r = SegmentAABBoxIntersect(b, a, d);
    if(dist) *dist = d;
    return r;
}

#if 0

bool shape::SweepAABBvsAABB(aabb a, aabb b, ml::vec2f va, ml::vec2f vb, float &u0, float &u1){
    //check if they were overlapping
    // on the previous frame
    if(a.test(b)){
        u0 = u1 = 0.f;
        return true;
    }

    //the problem is solved in A's frame of reference

    ml::vec2f v = (va - vb);
    if(ml::lengthSq(v) > 0.f){
        v = ml::vec2f(1.f)/v;
    }
    else{
        return false;
    }
    //relative velocity (in normalized time)

    ml::vec2f u_0(0.f);
    //first times of overlap along each axis

    ml::vec2f u_1(1.f);
    //last times of overlap along each axis

    //find the possible first and last times
    //of overlap along each axis

    u_0 = (a.max()-b.min())*v;
    u_1 = (b.max()-a.min())*v;

    //possible first time of overlap
    u0 = ml::max(u_0.x, u_0.y);

    //possible last time of overlap
    u1 = ml::min(u_1.x, u_1.y);

    //they could have only collided if
    //the first time of overlap occurred
    //before the last time of overlap
    return (u0 <= u1);
}
#endif

bool shape::SweepAABBvsAABB(aabb const &a, aabb const &b, ml::vec2f const &va, ml::vec2f const &vb, float &u0, float &u1){

    // Return early if a & b are already overlapping
    if(a.test(b)) return true;

    // Treat b as stationary, so invert v to get relative velocity
    ml::vec2f v = vb-va;

    u0 = 0.0f;
    u1 = 1.0f;

    ml::vec2f amax = a.max(), bmax = b.max(),
             amin = a.min(), bmin = b.min();

    // X axis overlap
    if( v.x < 0.f){
        if(     amin.x > bmax.x)        return false;
        else if(amin.x < bmax.x)        u1 = ml::min((amin.x - bmax.x) / v.x, u1 );

        if(     bmin.x > amax.x)        u0 = ml::max((amax.x - bmin.x) / v.x, u0);
    }
    else if( v.x > 0.f){
        if(     bmin.x > amax.x )       return false;
        else if(bmin.x < amax.x)        u1 = ml::min((amax.x - bmin.x) / v.x, u1 );

        if(     amin.x > bmax.x)        u0 = ml::max((amin.x - bmax.x) / v.x, u0);
    }

    if( u0 > u1 ) return false;

    //=================================

    // Y axis overlap
    if( v.y < 0 ){
        if(     amin.y > bmax.y)        return false;
        else if(amin.y < bmax.y)        u1 = ml::min((amin.y - bmax.y) / v.y, u1 );

        if(     bmin.y > amax.y)        u0 = ml::max((amax.y - bmin.y) / v.y, u0);
    }
    else if( v.y > 0 ){
        if(     bmin.y > amax.y )       return false;
        else if(bmin.y < amax.y)        u1 = ml::min((amax.y - bmin.y) / v.y, u1 );

        if(     amin.y > bmax.y)        u0 = ml::max((amin.y - bmax.y) / v.y, u0);
    }

    return (u0 <= u1);
}
}

