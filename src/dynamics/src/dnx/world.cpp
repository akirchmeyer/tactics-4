#include "world.h"
#include <ul/io.h>

namespace dnx{

world::world(broad *b, narrow *n) : m_broad(b), m_narrow(n){
    assert(m_broad);
    assert(m_narrow);
}

world::~world(){
    io::get("this")->debug("deleted world\n");
    clear();
}

void world::update(const float dt){
    assert(m_broad);
    assert(m_narrow);

    pcont pairs;
    m_broad->update(pairs, dt);
    m_narrow->narrowphase(dt, pairs);
}

void world::clear(){
    assert(m_broad);
    m_broad->clear();
}

}
