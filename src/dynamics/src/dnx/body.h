#ifndef BODY_H
#define BODY_H

#include "common.h"
#include "shape.h"
#include "props.h"
#include <unordered_map>
#include <map>
#include <tbb/concurrent_unordered_map.h>
#include <ul/io.h>
#include <el/link.h>

namespace ml{
uint32 gen_id();
void reset_id();
}

namespace  dnx{
class world;

//typedef void (*UpdateCallback)(body *b, float dt);

class body
{
public:
    body(const dnx::world *w, const transform &_t, const dnx::props *props, dnx::shape *s);
    ~body() noexcept{
        m_alive = false; m_active = false;
        e_destroy.dispatch();
    }

    _inline body *duplicate(const transform &_t) const{
        return new body(m_world, _t, m_props, m_shape->duplicate());
    }

    void update();
    void computeVelo();
    _inline void updateMom(){
        m_mom = getPos()-m_ppos;
    }

    dnx::shape *setShape(dnx::shape *s);

    /*void setUpdateCallback(UpdateCallback cb){
        m_cbu = cb;
    }

    void setData(del* d){ assert(d); m_data = uptr<del>(d); }
    void setData(uptr<del> d){ assert(d); m_data = std::move(d); }*/

    _inline const dnx::shape *getShape() const { assert(m_shape); return m_shape.get(); }
    _inline dnx::shape *acqShape() { assert(m_shape); return m_shape.get(); }
    //_inline del *data()          { assert(m_data); return m_data.get(); }
    _inline const dnx::props *getProps() const { assert(m_props); return m_props; }
    _inline const dnx::world *getWorld() const { assert(m_world); return m_world; }

    _inline bool isAlive() const { return m_alive; }

    _inline const aabb &getAABB() const  { return m_shape->getAABB(); }
    _inline const aabb getBounding() const { return dnx::aabb::fromvelo(m_shape->getAABB(), m_mom); }

    _inline float getMass() const { return m_props->mass(); }
    //_inline ml::vec2f &velo(){ return m_velo; }
    _inline const ml::vec2f getMomentum() const { return m_mom*m_props->mass(); }

    void setDestroy();

    //_inline ml::rot &rot()    { return t.q; }
    _inline const ml::rot& getRot() const { return t.q; }
    _inline void setRot(const ml::rot &b) { t.q = b; }

    _inline const ml::vec2f& getPos() const { return t.p; }
    _inline void setPos(const ml::vec2f &b) { t.p = b; e_move.dispatch(); updateMom(); }
    //_inline ml::vec2f &acqPos()  { return t.p; }

    _inline uint32 getID() const { return m_id; }

    //_inline bool &active()  { return m_active; }
    _inline bool isActive() const { return m_active; }
    _inline void setActive(bool const b) { m_active = b; }

    void listenDestroy(std::function<void(const dnx::body*)> const &f) const{
        e_destroy.connect(f);
    }
    void listenMove(std::function<void(const dnx::body*)> const &f) const{
        e_move.connect(f);
    }
    /*void unlistenDestroy(std::function<void(const dnx::body*)> const &f) const{
        e_destroy.disconnect(f);
    }*/

protected:
    std::unique_ptr<dnx::shape> m_shape;
    transform t;
    ml::vec2f m_ppos, m_mom/*, m_velo*/, m_acc;

    mutable el::event<const dnx::body*> e_destroy, e_move;
    bool m_alive, m_active;

    const dnx::world *m_world;
    const dnx::props *m_props;
    const uint32 m_id;
};

typedef std::vector<body*> bcont;

class b_factory : public del
{
public:
    b_factory(const dnx::world *w, uptr<dnx::props> props, uptr<dnx::shape> s) : m_props(std::move(props)), m_shape(std::move(s)), m_world(w) {
        assert(m_world);
        assert(m_props);
        assert(m_shape);

        m_shape->resetTransform();
    }

    virtual ~b_factory() noexcept{
        io::get("this")->debug("deleted b_factory\n");
    }

    _inline uptr<body> construct(const transform &t) const{
        return make_uptr<body>(m_world, t, m_props.get(), m_shape->duplicate());
    }

    _inline const dnx::shape *getShape() const{
        assert(m_shape);
        return m_shape.get();
    }

    _inline const dnx::props *getProps() const{
        assert(m_props);
        return m_props.get();
    }

protected:
    std::unique_ptr<dnx::props> m_props;
    std::unique_ptr<dnx::shape> m_shape;
    const dnx::world *m_world;
};


typedef std::pair<const dnx::body*,const dnx::body*> bpair;
typedef tbb::concurrent_unordered_map<ml::vec2u, bpair, ext::num_hash<uint32>> pcont;

}




#endif // BODY_H
